import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import {ScrollView} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';

class SignupScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      businessName: '',
      address: '',
      zipcode: '',
    };
  }

  handleChange = (value, name) => {
    this.setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  saveUserDetails = () => {
    const {name, businessName, address, zipcode} = this.state;
    const {phone_number} = this.props.route.params;
    if (name.toString().trim() === '') {
      alert('Please enter your name');
    } else if (businessName.toString().trim() === '') {
      alert('Please enter your business name');
    } else if (address.toString().trim() === '') {
      alert('Please enter address');
    } else if (zipcode.toString().trim() === '') {
      alert('Please enter zipcode');
    } else {
      let {actions, dispatch} = this.props;
      actions.showLoader(true);
      let data = {};
      data['buyer_name'] = name;
      data['address'] = address;
      data['pincode'] = zipcode;
      data['firm_name'] = businessName;
      data['mobile_number'] = phone_number;
      console.log('signup reqq : ', data);
      Request.post('buyer_save', data)
        .then((res) => {
          console.log('signup res : ', res);
          actions.hideLoader(false);
          if (res.result.status === 'OK') {
            actions.saveUserData(res.data);
            this.props.navigation.replace('HomeTabFlow');
          } else {
            alert('Server Error');
            console.log('api error : ', res);
          }
        })
        .catch((err) => {
          actions.hideLoader(false);
          MyToast.showToast(err.message());
        });
    }
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar barStyle="light-content" hidden={false} />
        <ScrollView>
          <View style={styles.container}>
            {/* <Toast ref="toast"/> */}

            <HeaderComp
              leftImgHandler={() => this.props.navigation.goBack()}
              imageLeft={constants.images.backArrowBlack}
            />
            <View style={{flex: 1}}>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: moderateScale(16),
                }}>
                <Text style={styles.mobileNuberVerificationText}>
                  {'Enter Account Information'}
                </Text>
                <Text style={styles.enterMobileText}>
                  {'Register your business on aayme'}
                </Text>

                <View style={styles.mobileEditTextView}>
                  <View style={styles.smallMobileImageView}>
                    <Image
                      source={constants.images.user}
                      style={{
                        height: 20,
                        width: 20,
                        tintColor: constants.colors.pink,
                      }}></Image>
                  </View>
                  <TextInput
                    style={styles.mobileEditText}
                    placeholder="Name"
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) =>
                      this.handleChange(text, 'name')
                    }></TextInput>
                </View>
                <View
                  style={[
                    styles.mobileEditTextView,
                    {marginTop: moderateScale(8)},
                  ]}>
                  <View style={styles.smallMobileImageView}>
                    <Image
                      source={constants.images.business}
                      style={{
                        height: 20,
                        width: 20,
                        tintColor: constants.colors.pink,
                      }}></Image>
                  </View>
                  <TextInput
                    style={styles.mobileEditText}
                    placeholder="Business Name"
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) =>
                      this.handleChange(text, 'businessName')
                    }></TextInput>
                </View>

                <View
                  style={[
                    styles.mobileEditTextView,
                    {marginTop: moderateScale(8)},
                  ]}>
                  <View style={styles.smallMobileImageView}>
                    <Image
                      source={constants.images.location}
                      style={{
                        height: 20,
                        width: 20,
                        tintColor: constants.colors.pink,
                      }}></Image>
                  </View>
                  <TextInput
                    style={styles.mobileEditText}
                    placeholder="Zipcode"
                    keyboardType="numeric"
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) => this.handleChange(text, 'zipcode')}
                    maxLength={8}></TextInput>
                </View>

                <View
                  style={[
                    styles.mobileEditTextView,
                    {
                      marginTop: moderateScale(8),
                      alignItems: 'flex-start',
                      height: moderateScale(100),
                    },
                  ]}>
                  <View style={styles.smallMobileImageView}>
                    <Image
                      source={constants.images.location}
                      style={{
                        height: 20,
                        width: 20,
                        tintColor: constants.colors.pink,
                        marginTop: moderateScale(14),
                      }}></Image>
                  </View>
                  <TextInput
                    style={[
                      styles.mobileEditText,
                      {justifyContent: 'flex-start'},
                    ]}
                    placeholder="Address"
                    multiline={true}
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) =>
                      this.handleChange(text, 'address')
                    }></TextInput>
                </View>
              </View>
              <TouchableOpacity onPress={() => this.saveUserDetails()}>
                <View style={styles.verifyView}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 4.5}}
                    colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      right: 0,
                      height: 56,
                      borderRadius: moderateScale(4),
                    }}
                  />
                  <Text style={styles.verifyText}>{'Submit'}</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth}) => ({
  user: auth.userData,
});

export default connect(mapStateToProps, mapDispatchToProps)(SignupScreen);
