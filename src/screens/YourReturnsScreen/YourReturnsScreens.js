import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {textScale} from '../../utilities/responsiveSize';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import {fonts} from '../../assets';
import LinearGradient from 'react-native-linear-gradient';
const {height, width} = Dimensions.get('window');

class YourReturnsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ordersData: [],
    };
  }

  componentDidMount() {
    this.getMyReturns();
  }

  getMyReturns = () => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(`Get_ReturnOrder?BuyerId=${user.Buyer_Id}`, null)
      .then((res) => {
        console.log('your returns res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({ordersData: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  renderEmptyListComponent = () => {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: height / 2 + 100,
        }}>
        <Image
          style={{height: moderateScale(100), width: moderateScale(100)}}
          source={constants.images.return_box}></Image>
        <Text
          style={{
            marginTop: moderateScale(16),
            fontSize: textScale(14),
            fontFamily: fonts.bold,
          }}>
          No Results Found
        </Text>
        <Text
          style={{
            marginTop: moderateScale(4),
            fontSize: textScale(12),
            fontFamily: fonts.regular,
          }}>
          View your delivered shipments
        </Text>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('YourOrdersScreen')}>
          <View style={[styles.verifyView, {marginTop: 16}]}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 4}}
              colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                height: 48,
                borderRadius: moderateScale(4),
              }}
            />
            <Text style={styles.verifyText}>{'Create Return'}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar barStyle="dark-content" hidden={false} />
        <HeaderComp
          leftText={'Your Returns'}
          leftTextStyle={{color: constants.colors.white}}
          imageLeft={constants.images.backArrowBlack}
          leftImageTint={constants.colors.white}
          headerContainerStyle={{backgroundColor: constants.colors.pink}}
          leftImgHandler={() => this.props.navigation.goBack()}
        />
        {/* <View style={styles.searchEditTextView}>
          <View style={styles.smallMobileImageView}>
            <Image
              source={constants.images.search}
              style={{height: 16, width: 16}}></Image>
          </View>
          <TextInput
            style={styles.mobileEditText}
            placeholder="Search for Orders"
            placeholderTextColor={constants.colors.lightBlack}></TextInput>
        </View> */}
        <FlatList
          data={this.state.ordersData}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('OrderDetailsScreen', {
                    orderId: item.Ord_Id,
                  })
                }
                style={{
                  width: width,
                  flexDirection: 'row',
                  padding: moderateScale(16),
                  marginTop: index === 0 ? moderateScale(0) : moderateScale(4),
                  backgroundColor: constants.colors.white,
                }}>
                <Image
                  style={{
                    height: moderateScale(60),
                    width: moderateScale(60),
                  }}
                  source={{
                    uri: item.Product_Image,
                  }}
                  resizeMode="contain"
                />
                <View style={{width: '75%', marginStart: moderateScale(16)}}>
                  <Text
                    style={{
                      width: '95%',
                      fontSize: textScale(12),
                      color: constants.colors.grayLight,
                    }}>{`Ordered From`}</Text>

                  <Text
                    style={{
                      width: '100%',
                      fontWeight: 'bold',
                      fontSize: textScale(16),
                    }}>{`${item.Seller_Firm_Name}`}</Text>
                  <Text
                    style={{
                      color:
                        item.Order_Status === 'Pending'
                          ? constants.colors.red
                          : constants.colors.green,
                      fontWeight: 'bold',
                      marginTop: moderateScale(4),
                    }}>{`${item.Order_Status}`}</Text>
                  <Text
                    style={{
                      fontSize: textScale(16),
                      fontWeight: 'bold',
                      marginTop: moderateScale(8),
                    }}>{`${constants.strings.rupee_symbol}${item.Total_amt}`}</Text>
                  <Text
                    style={{
                      width: '95%',
                      fontSize: textScale(14),
                      marginTop: moderateScale(8),
                      color: constants.colors.grayLight,
                    }}>{`Order ID : ${item.Order_ID}`}</Text>
                  <Text
                    style={{
                      width: '95%',
                      fontSize: textScale(14),
                      color: constants.colors.grayLight,
                    }}>{`Placed On : ${item.Placed_Date}`}</Text>
                  {/* <Text
                    style={{
                      width: '95%',
                      fontSize: textScale(14),
                      color: constants.colors.grayLight,
                    }}>{`Expected Date : ${item.Expected_Date}`}</Text> */}
                </View>
              </TouchableOpacity>
            );
          }}
          ListEmptyComponent={this.renderEmptyListComponent()}></FlatList>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth, category}) => ({
  user: auth.userData,
});

export default connect(mapStateToProps, mapDispatchToProps)(YourReturnsScreen);
