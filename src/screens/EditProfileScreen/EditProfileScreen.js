import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import {ScrollView} from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import LinearGradient from 'react-native-linear-gradient';

class EditProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      businessName: '',
      address: '',
      zipcode: '',
      alternateNum: '',
      billingAddress: '',
      shippingAddress: '',
      imagePath: '',
      base64Image: '',
    };
  }

  componentDidMount() {
    const {user} = this.props;
    this.setState({
      name: user.Buyer_Name,
      businessName: user.Firm_Name,
      imagePath: user.Profile_Pic,
      address: user.Address,
      zipcode: user.Pincode,
      billingAddress: user.BillingAddress,
      shippingAddress: user.DeliveryAddress,
      alternateNum: user.Alternate_Number,
      bankName: user.Bank_Name,
      accountNumber: user.Bank_AC_No,
      ifscCode: user.Bank_Ifsc_Code,
      email: user.Email,
      city: user.City,
      state: user.State,
    });
  }

  handleChange = (value, name) => {
    this.setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  openImagePicker = () => {
    const options = {
      title: 'Select Avatar',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          imagePath: response.uri,
          base64Image: response.data,
        });
      }
    });
  };

  saveUserDetails = () => {
    const {user} = this.props;
    const {
      name,
      businessName,
      address,
      zipcode,
      billingAddress,
      shippingAddress,
      alternateNum,
      base64Image,
      bankName,
      accountNumber,
      ifscCode,
      email,
      city,
      state,
    } = this.state;
    let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (name.toString().trim() === '') {
      alert('Please enter your name');
    } else if (businessName.toString().trim() === '') {
      alert('Please enter your business name');
    } else if (email.toString().trim() === '') {
      alert('Please enter your email address');
    } else if (!regex.test(email.toString().trim())) {
      alert('Please enter valid email address');
    } else if (city.toString().trim() === '') {
      alert('Please enter your city name');
    } else if (state.toString().trim() === '') {
      alert('Please enter your state name');
    } else if (address.toString().trim() === '') {
      alert('Please enter address');
    } else if (zipcode.toString().trim() === '') {
      alert('Please enter zipcode');
    } else if (bankName.toString().trim() === '') {
      alert('Please enter your bank name');
    } else if (accountNumber.toString().trim() === '') {
      alert('Please enter your bank account number');
    } else if (ifscCode.toString().trim() === '') {
      alert('Please enter IFSC code');
    } else if (user.Mobile_Number === alternateNum) {
      alert(
        'Please use another mobile number which is different from your registered mobile number',
      );
    } else {
      console.log('base64 image:', base64Image);
      let {actions} = this.props;
      actions.showLoader(true);
      let data = new FormData();
      data.append('buyer_name', name);
      data.append('address', address);
      data.append('pincode', zipcode);
      data.append('firm_name', businessName);
      data.append('Buyer_Email', email),
        data.append('Buyer_City', city),
        data.append('Buyer_State', state);
      data.append('BuyerId', user.Buyer_Id);
      data.append('Mobile_No', alternateNum);
      data.append('BillingAddress', billingAddress);
      data.append('DeliveryAddress', shippingAddress);
      data.append('Bank_Name', bankName),
        data.append('Bank_AC_No', accountNumber),
        data.append('Bank_Ifsc_Code', ifscCode);
      data.append('ProfilePic', base64Image);
      console.log('upload Image req Req : ', data);
      Request.post('buyer_Update', data)
        .then((res) => {
          console.log('update profile res : ', res);
          actions.hideLoader(false);
          if (res.result.status === 'OK') {
            actions.saveUserData(res.data);
            const {updateData} = this.props.route.params;
            updateData();
            this.props.navigation.goBack();
          } else {
            alert('Server Error');
            console.log('api error : ', res);
          }
        })
        .catch((err) => {
          actions.hideLoader(false);
          MyToast.showToast(err.message());
        });
    }
  };

  gotoPlaceOrder = () => {
    const {user} = this.props;
    const {cartData, totalAmount} = this.props.route.params;
    const {
      shippingAddress,
      zipcode,
      email,
      city,
      state,
      billingAddress,
    } = this.state;
    console.log(shippingAddress, zipcode, email, city, state);
    let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email.toString().trim() === '') {
      alert('Please enter your email address');
    } else if (!regex.test(email.toString().trim())) {
      alert('Please enter valid email address');
    } else if (city.toString().trim() === '') {
      alert('Please enter your city name');
    } else if (state.toString().trim() === '') {
      alert('Please enter your state name');
    } else if (zipcode.toString().trim() === '') {
      alert('Please enter zipcode');
    } else if (shippingAddress.toString().trim() === '') {
      alert('Please enter delivery address');
    } else if (billingAddress.toString().trim() === '') {
      alert('Please enter billing address');
    } else {
      this.props.navigation.navigate('DeliveryAddressScreen', {
        cartData: cartData,
        totalAmount: totalAmount,
        pincode: this.state.zipcode,
        address: this.state.address,
        billingAddress: this.state.billingAddress,
        shippingAddress: this.state.shippingAddress,
        email: this.state.email,
        city: this.state.city,
        state: this.state.state,
      });
    }
  };

  render() {
    const {user} = this.props;
    const {cartData, totalAmount} = this.props.route.params;
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar barStyle="dark-content" hidden={false} />
        <HeaderComp
          leftText={
            cartData === undefined ? 'Edit Profile' : 'Confirm Your Address'
          }
          leftTextStyle={{color: constants.colors.white}}
          imageLeft={constants.images.backArrowBlack}
          leftImageTint={constants.colors.white}
          headerContainerStyle={{backgroundColor: constants.colors.pink}}
          leftImgHandler={() => this.props.navigation.goBack()}
        />
        <ScrollView>
          <View style={styles.container}>
            {/* <Toast ref="toast"/> */}
            <View style={{flex: 1}}>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: moderateScale(16),
                }}>
                {cartData === undefined ? (
                  <TouchableOpacity
                    style={{alignItems: 'center'}}
                    onPress={() => {
                      this.openImagePicker();
                    }}>
                    <Image
                      style={{
                        height: moderateScale(100),
                        width: moderateScale(100),
                        borderRadius: moderateScale(50),
                      }}
                      source={{uri: this.state.imagePath}}
                    />
                  </TouchableOpacity>
                ) : null}
                <View style={styles.mobileEditTextView}>
                  <View style={styles.smallMobileImageView}>
                    <Image
                      source={constants.images.user}
                      style={{
                        height: 20,
                        width: 20,
                        tintColor: constants.colors.pink,
                      }}></Image>
                  </View>
                  <TextInput
                    style={styles.mobileEditText}
                    value={this.state.name}
                    editable={cartData === undefined ? true : false}
                    placeholder="Name"
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) =>
                      this.handleChange(text, 'name')
                    }></TextInput>
                </View>
                <View
                  style={[
                    styles.mobileEditTextView,
                    {marginTop: moderateScale(8)},
                  ]}>
                  <View style={styles.smallMobileImageView}>
                    <Image
                      source={constants.images.business}
                      style={{
                        height: 20,
                        width: 20,
                        tintColor: constants.colors.pink,
                      }}></Image>
                  </View>
                  <TextInput
                    style={styles.mobileEditText}
                    value={this.state.businessName}
                    placeholder="Business Name"
                    editable={
                      cartData === undefined
                        ? user.KYC === false
                          ? true
                          : false
                        : false
                    }
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) =>
                      this.handleChange(text, 'businessName')
                    }></TextInput>
                </View>
                <View
                  style={[
                    styles.mobileEditTextView,
                    {marginTop: moderateScale(8)},
                  ]}>
                  <View style={styles.smallMobileImageView}>
                    <Image
                      source={constants.images.business}
                      style={{
                        height: 20,
                        width: 20,
                        tintColor: constants.colors.pink,
                      }}></Image>
                  </View>
                  <TextInput
                    style={styles.mobileEditText}
                    value={this.state.email}
                    placeholder="Email"
                    // editable={cartData === undefined ? true : false}
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) =>
                      this.handleChange(text, 'email')
                    }></TextInput>
                </View>
                {cartData === undefined ? (
                  <View
                    style={[
                      styles.mobileEditTextView,
                      {marginTop: moderateScale(8)},
                    ]}>
                    <View style={styles.smallMobileImageView}>
                      <Image
                        source={constants.images.smallPhone}
                        style={{
                          height: 20,
                          width: 20,
                          tintColor: constants.colors.pink,
                        }}></Image>
                    </View>
                    <TextInput
                      style={styles.mobileEditText}
                      value={this.state.alternateNum}
                      editable={cartData === undefined ? true : false}
                      placeholder="Alternate Number"
                      keyboardType="numeric"
                      placeholderTextColor={constants.colors.lightBlack}
                      onChangeText={(text) =>
                        this.handleChange(text, 'alternateNum')
                      }
                      maxLength={10}></TextInput>
                  </View>
                ) : null}

                <View
                  style={[
                    styles.mobileEditTextView,
                    {marginTop: moderateScale(8)},
                  ]}>
                  <View style={styles.smallMobileImageView}>
                    <Image
                      source={constants.images.business}
                      style={{
                        height: 20,
                        width: 20,
                        tintColor: constants.colors.pink,
                      }}></Image>
                  </View>
                  <TextInput
                    style={styles.mobileEditText}
                    value={this.state.city}
                    placeholder="City"
                    // editable={cartData === undefined ? true : false}
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) =>
                      this.handleChange(text, 'city')
                    }></TextInput>
                </View>

                <View
                  style={[
                    styles.mobileEditTextView,
                    {marginTop: moderateScale(8)},
                  ]}>
                  <View style={styles.smallMobileImageView}>
                    <Image
                      source={constants.images.business}
                      style={{
                        height: 20,
                        width: 20,
                        tintColor: constants.colors.pink,
                      }}></Image>
                  </View>
                  <TextInput
                    style={styles.mobileEditText}
                    value={this.state.state}
                    placeholder="State"
                    // editable={cartData === undefined ? true : false}
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) =>
                      this.handleChange(text, 'state')
                    }></TextInput>
                </View>

                <View
                  style={[
                    styles.mobileEditTextView,
                    {marginTop: moderateScale(8)},
                  ]}>
                  <View style={styles.smallMobileImageView}>
                    <Image
                      source={constants.images.location}
                      style={{
                        height: 20,
                        width: 20,
                        tintColor: constants.colors.pink,
                      }}></Image>
                  </View>
                  <TextInput
                    style={styles.mobileEditText}
                    value={this.state.zipcode}
                    placeholder="Pincode"
                    keyboardType="numeric"
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) => this.handleChange(text, 'zipcode')}
                    maxLength={8}></TextInput>
                </View>
                {cartData === undefined ? (
                  <View
                    style={[
                      styles.mobileEditTextView,
                      {
                        marginTop: moderateScale(8),
                        alignItems: 'flex-start',
                        height: moderateScale(100),
                      },
                    ]}>
                    <View style={styles.smallMobileImageView}>
                      <Image
                        source={constants.images.location}
                        style={{
                          height: 20,
                          width: 20,
                          tintColor: constants.colors.pink,
                          marginTop: moderateScale(14),
                        }}></Image>
                    </View>
                    <TextInput
                      style={[
                        styles.mobileEditText,
                        {justifyContent: 'flex-start'},
                      ]}
                      value={`${this.state.address}`}
                      placeholder="Address"
                      multiline={true}
                      placeholderTextColor={constants.colors.lightBlack}
                      onChangeText={(text) =>
                        this.handleChange(text, 'address')
                      }></TextInput>
                  </View>
                ) : null}
                <View
                  style={[
                    styles.mobileEditTextView,
                    {
                      marginTop: moderateScale(8),
                      alignItems: 'flex-start',
                      height: moderateScale(100),
                    },
                  ]}>
                  <View style={styles.smallMobileImageView}>
                    <Image
                      source={constants.images.location}
                      style={{
                        height: 20,
                        width: 20,
                        tintColor: constants.colors.pink,
                        marginTop: moderateScale(14),
                      }}></Image>
                  </View>
                  <TextInput
                    style={[
                      styles.mobileEditText,
                      {justifyContent: 'flex-start'},
                    ]}
                    value={`${this.state.billingAddress}`}
                    placeholder="Billing Address"
                    multiline={true}
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) =>
                      this.handleChange(text, 'billingAddress')
                    }></TextInput>
                </View>
                <View
                  style={[
                    styles.mobileEditTextView,
                    {
                      marginTop: moderateScale(8),
                      alignItems: 'flex-start',
                      height: moderateScale(100),
                    },
                  ]}>
                  <View style={styles.smallMobileImageView}>
                    <Image
                      source={constants.images.location}
                      style={{
                        height: 20,
                        width: 20,
                        tintColor: constants.colors.pink,
                        marginTop: moderateScale(14),
                      }}></Image>
                  </View>
                  <TextInput
                    style={[
                      styles.mobileEditText,
                      {justifyContent: 'flex-start'},
                    ]}
                    value={`${this.state.shippingAddress}`}
                    placeholder="Shipping Address"
                    multiline={true}
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) =>
                      this.handleChange(text, 'shippingAddress')
                    }></TextInput>
                </View>
              </View>
              {cartData === undefined &&
              user.Buyer_Bank_Detail_Active === true ? (
                <View
                  style={{
                    ...styles.smallMobileImageView,
                    marginTop: moderateScale(24),
                    marginStart: moderateScale(0),
                  }}>
                  <View style={{width: '70%'}}>
                    <Text
                      style={{
                        ...styles.verifyText,
                        color: constants.colors.black,
                        textAlign: 'left',
                      }}>
                      Bank Details
                    </Text>
                  </View>
                  <View
                    style={[
                      styles.mobileEditTextView,
                      {marginTop: moderateScale(8)},
                    ]}>
                    <View style={styles.smallMobileImageView}>
                      <Image
                        source={constants.images.business}
                        style={{
                          height: 20,
                          width: 20,
                          tintColor: constants.colors.pink,
                        }}></Image>
                    </View>
                    <TextInput
                      style={styles.mobileEditText}
                      value={this.state.bankName}
                      placeholder="Bank Name"
                      editable={cartData === undefined ? true : false}
                      placeholderTextColor={constants.colors.lightBlack}
                      onChangeText={(text) =>
                        this.handleChange(text, 'bankName')
                      }></TextInput>
                  </View>
                  <View
                    style={[
                      styles.mobileEditTextView,
                      {marginTop: moderateScale(8)},
                    ]}>
                    <View style={styles.smallMobileImageView}>
                      <Image
                        source={constants.images.location}
                        style={{
                          height: 20,
                          width: 20,
                          tintColor: constants.colors.pink,
                        }}></Image>
                    </View>
                    <TextInput
                      style={styles.mobileEditText}
                      value={this.state.accountNumber}
                      placeholder="Account Number"
                      keyboardType="numeric"
                      placeholderTextColor={constants.colors.lightBlack}
                      onChangeText={(text) =>
                        this.handleChange(text, 'accountNumber')
                      }
                      maxLength={20}></TextInput>
                  </View>
                  <View
                    style={[
                      styles.mobileEditTextView,
                      {marginTop: moderateScale(8)},
                    ]}>
                    <View style={styles.smallMobileImageView}>
                      <Image
                        source={constants.images.business}
                        style={{
                          height: 20,
                          width: 20,
                          tintColor: constants.colors.pink,
                        }}></Image>
                    </View>
                    <TextInput
                      style={styles.mobileEditText}
                      value={this.state.ifscCode}
                      placeholder="IFSC Code"
                      editable={cartData === undefined ? true : false}
                      placeholderTextColor={constants.colors.lightBlack}
                      onChangeText={(text) =>
                        this.handleChange(text, 'ifscCode')
                      }></TextInput>
                  </View>
                </View>
              ) : null}

              <TouchableOpacity
                onPress={() =>
                  cartData === undefined
                    ? this.saveUserDetails()
                    : this.gotoPlaceOrder()
                }>
                <View style={[styles.verifyView,{backgroundColor:'transparent'}]}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 5}}
                    colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      right: 0,
                      height: 56,
                    }}
                  />
                  <Text style={styles.verifyText}>
                    {cartData === undefined ? 'Save' : 'Continue'}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth}) => ({
  user: auth.userData,
});

const dynamicRandomName = () => {
  let possible =
    'dhsfkhkdshfkhdksjfsdf' + 'mangal' + '_qazwsxedcvfrtgbnhyujmkiolp';
  let textOrder = '';
  for (let i = 0; i < 60; i++) {
    textOrder += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return textOrder.replace(/\s/g, '');
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileScreen);
