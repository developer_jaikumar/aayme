import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
  InteractionManager,
  AppState,
  Alert,
  Linking,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {RFValue} from 'react-native-responsive-fontsize';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {ScrollView, FlatList} from 'react-native-gesture-handler';
import SwiperFlatList from 'react-native-swiper-flatlist';
import {FlatGrid} from 'react-native-super-grid';
import {textScale} from '../../utilities/responsiveSize';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import AsyncStorage from '@react-native-community/async-storage';
import {useFocusEffect} from '@react-navigation/native';
import PTRView from 'react-native-pull-to-refresh';
import {fonts} from '../../assets';
import DeviceInfo from 'react-native-device-info';
import LinearGradient from 'react-native-linear-gradient';

const {height, width} = Dimensions.get('window');

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.reff = null;
    this.isSearchOn = false;
    this.state = {
      imageData: [],
      categoryData: [],
      latestProducts: [],
      featuredProducts: [],
      topSellingProducts: [],
      flagshipBrands: [],
      buyPopularProducts: [],
      bestSellingProducts: [],
      bestSellingMobileProducts: [],
      searchList: [],
      sellingProductsData: [],
      opacity: 1,
      searchText: '',
      adData: {},
      adImage: '',
      isAdVisible: false,
      isLoading: true,
      isAutoFocus: false,
      appState: AppState.currentState,
      appVersion: DeviceInfo.getVersion(),
      responseData: {},
    };
  }

  componentDidMount = () => {
    const {user} = this.props;
    console.log('Deep Link Props : ', this.props);
    Linking.getInitialURL().then((url) => {
      if (url !== undefined && url !== null && url !== '') {
        const id = url.substring(24, url.length);
        this.props.navigation.navigate('ProductDetailsScreen', {
          id: id,
        });
      }
    });

    AppState.addEventListener('change', this._handleAppStateChange);
    this.getSliderImages();
    this.getCategoryData();
    this.getHomeProductsData();
    this.getAdvertisementImage();
    this.getUpdatedBuyerData();
  };

  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
      if (
        this.props.route.params !== undefined &&
        this.props.route.params.id !== ''
      ) {
        this.props.navigation.navigate('ProductDetailsScreen', {
          id: this.props.route.params.id,
        });
      }
    }
    this.setState({appState: nextAppState});
  };

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  componentDidUpdate() {
    // const {index, routes} = this.props.navigation.dangerouslyGetState();
    // const currentRoute = routes[index].name;
    // if (currentRoute === 'SearchScreen') {
    //   this.reff.focus();
    //   this.isSearchOn = true;
    //   // this.focusInputWithKeyboard()
    // } else {
    //   this.isSearchOn = false;
    //   // this.reff.current.focus()
    // }
  }

  focusInputWithKeyboard() {
    InteractionManager.runAfterInteractions(() => {
      this.reff.current.focus();
    });
  }

  renderEmptyListComponent = () => {
    return (
      <View
        style={{
          width: width,
          alignSelf: 'center',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: moderateScale(8),
        }}>
        <Text style={{textAlign: 'center', width: '100%', alignSelf: 'center'}}>
          {this.state.isLoading === false ? 'No Items to Show' : ''}
        </Text>
      </View>
    );
  };

  openMenu = () => {
    this.props.navigation.openDrawer();
  };

  getSliderImages = async () => {
    let {actions, dispatch, user} = this.props;
    actions.showLoader(true);
    let data = {};
    data['user_type'] = 'buyer';
    console.log('get slider images reqq : ', data);
    await Request.get('Buyer_Get_Slider', data)
      .then((res) => {
        console.log('get slider images res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({imageData: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  getCategoryData = () => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(`Get_Categories?buyer_id=${user.Buyer_Id}`, null)
      .then((res) => {
        console.log('categories res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({categoryData: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  getAdvertisementImage = () => {
    let {actions, dispatch} = this.props;
    actions.showLoader(true);
    Request.get('Buyer_Ads?user_type=buyer', null)
      .then((res) => {
        console.log('adv image res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          if (res.data !== undefined) {
            this.setState({
              isAdVisible: true,
              adData: res.data,
              adImage: res.data.img1,
              appVersion: res.data.app_version,
            });
          } else {
            this.setState({
              adData: res.data,
              adImage: res.data.img,
              isAdVisible: false,
            });
          }
          this.checkAppVersioning(
            res.data.app_version,
            res.data.app_message_title,
            res.data.app_message,
          );
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  checkAppVersioning = (appVersion, title, message) => {
    console.log(`version :::::${appVersion}=====${DeviceInfo.getVersion()}`);
    if (parseFloat(appVersion) > parseFloat(DeviceInfo.getVersion())) {
      this.customAlert(title, message);
    }
  };

  customAlert = (title, message) => {
    Alert.alert(
      title,
      message,
      [
        // item !== undefined ? {text: 'Cancel'} : null,

        {
          text: 'UPDATE APP',
          onPress: () => {
            Linking.openURL('market://details?id=com.aayme');
          },
        },
      ],
      {
        cancelable: false,
      },
    );
  };

  getHomeProductsData = () => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(`Get_Home_Products?buyer_id=${user.Buyer_Id}`, null)
      .then((res) => {
        console.log('home products res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({
            responseData: res.data,
            latestProducts: res.data.Latest_Products,
            featuredProducts: res.data.Featured_Products,
            topSellingProducts: res.data.Top_Selling_Products,
            flagshipBrands: res.data.Flagship_Brand,
            buyPopularProducts: res.data.Buy_Popular_Products,
            bestSellingProducts: res.data.Best_Selling_Bluetooth_Products,
            bestSellingMobileProducts: res.data.Best_Selling_Mobile_Products,
            isLoading: false,
          });
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  getUpdatedBuyerData = () => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(`Get_Buyer?buyerid=${user.Buyer_Id}`, null)
      .then((res) => {
        console.log('getBuyerResponse res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          console.log('buyer data success');
          actions.saveUserData(res.data);
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  itemSeprator = () => {
    return (
      <View
        style={{
          height: moderateScale(0.5),
          width: '100%',
          backgroundColor: constants.colors.gray,
        }}></View>
    );
  };

  // emptyListComponent = () => {
  //   return (
  //     <View
  //       style={{
  //         height: moderateScale(40),
  //         width: '100%',
  //         backgroundColor: constants.colors.gray,
  //         alignItems:'center'
  //       }}>
  //         <Text>No Results Found</Text>
  //       </View>
  //   );
  // };

  onSearchItem = (text) => {
    this.setState({opacity: 0.3, searchText: text});
    this.getSearchResults(text);
  };

  _storeData = async (value) => {
    try {
      await AsyncStorage.setItem('update', value);
      console.log('value storrredd');
    } catch (error) {
      console.log('error stored data', error);
      // Error saving data
    }
  };

  _retrieveData = async () => {
    console.log('reterived method called');
    try {
      // console.log('current screen', currentRoute);
      if (value !== null) {
        // We have data!!
        console.log('value retereived : ', value);
        this.setState({opacity: 1, searchList: [], searchText: ''});
        this._storeData('false');
        const value = await AsyncStorage.getItem('update');
        // const {index, routes} = this.props.navigation.dangerouslyGetState();
        // const currentRoute = routes[index].name;
        // if (currentRoute === 'SearchScreen') {
        //   this.setState({isAutoFocus: true});
        //   this.reff.focus();
        //   // this.focusInputWithKeyboard();
        // } else {
        //   this.setState({isAutoFocus: false});
        // }
      }
    } catch (error) {
      console.log('error reterieve data data', error);

      // Error retrieving data
    }
  };

  getSearchResults = (text) => {
    let {actions, user} = this.props;
    // actions.showLoader(true);
    Request.get(`Get_Search?SeachText=${text}&buyer_id=${user.Buyer_Id}`, null)
      .then((res) => {
        console.log('search results res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          if (res.data.length === 0) {
            this.setState({
              opacity: 1,
              searchList: [],
            });
          } else {
            this.setState({
              opacity: 0.7,
              searchList: res.data,
            });
          }
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };
  _refresh = () => {
    return new Promise((resolve) => {
      this.componentDidMount();
      setTimeout(() => {
        resolve();
      }, 2000);
    });
  };

  render() {
    const {categoryClicked, actions, user} = this.props;
    console.log('props data : ', this.props);
    const {
      searchText,
      searchList,
      latestProducts,
      featuredProducts,
      topSellingProducts,
      buyPopularProducts,
      bestSellingProducts,
      bestSellingMobileProducts,
      responseData,
      isAdVisible,
      isAutoFocus,
      flagshipBrands,
      adData,
      adImage,
    } = this.state;
    // const{routeName} = await AsyncStorage.getItem('routeName')
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar barStyle="light-content" hidden={false} />
        <RefreshData onUpdate={this._retrieveData} />

        <View style={styles.container}>
          {/* <Toast ref="toast"/> */}
          <View style={styles.menuHeader}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 3.5}}
              colors={['#E03C3C', '#E03C3C', '#f47518', '#f3711b', '#f3711b']}
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                height: 105,
              }}

              // useAngle = {true}
              // angle = {75}
              // angleCenter = {{x:0.5,y:0.5}}
            />
            <View
              style={[
                // styles.subMenuHeader,
                {marginHorizontal: this.isSearchOn ? 0 : 16, marginTop: 30},
              ]}>
              <View
                style={[
                  styles.searchEditTextView,
                  {
                    width: this.isSearchOn ? '95%' : '80%',
                    marginHorizontal: 0,
                    justifyContent: 'space-between',
                  },
                ]}>
                <View
                  style={[
                    styles.smallMobileImageView,
                    {flexDirection: 'row', marginStart: 8, borderRadius: 2},
                  ]}>
                  <TouchableOpacity onPress={() => this.openMenu()}>
                    <View
                      source={constants.images.menu}
                      style={{
                        width: moderateScale(20),
                        alignItems: 'flex-start',
                        // justifyContent: 'center',
                      }}>
                      <View
                        style={{
                          height: 3,
                          width: 18,
                          borderRadius: 2,
                          backgroundColor: constants.colors.pink,
                        }}
                      />
                      <View
                        style={{
                          height: 3,
                          width: 12,
                          marginTop: 4,
                          borderRadius: 2,
                          backgroundColor: constants.colors.pink,
                        }}
                      />
                      <View
                        style={{
                          height: 3,
                          width: 18,
                          marginTop: 4,
                          borderRadius: 2,
                          backgroundColor: constants.colors.pink,
                        }}
                      />
                    </View>
                  </TouchableOpacity>

                  <TextInput
                    ref={(ref) => (this.reff = ref)}
                    style={[
                      styles.mobileEditText,
                      {
                        width: this.isSearchOn ? '100%' : '70%',
                        paddingEnd: moderateScale(24),

                        marginStart: 4,
                      },
                    ]}
                    value={this.state.searchText}
                    placeholder="Search Products"
                    focusable={true}
                    autoFocus={isAutoFocus}
                    placeholderTextColor={constants.colors.lightBlack}
                    onChangeText={(text) =>
                      text && text.length > 0
                        ? this.onSearchItem(text)
                        : this.setState({
                            opacity: 1,
                            searchList: [],
                            searchText: '',
                          })
                    }></TextInput>
                </View>
                <Image
                  source={constants.images.search}
                  style={{height: 16, width: 16, marginEnd: 16}}></Image>
                {this.state.searchText !== '' && (
                  <TouchableOpacity
                    style={
                      ([styles.smallMobileImageView],
                      {marginStart: -16, paddingEnd: 8})
                    }
                    onPress={() => {
                      this.setState({
                        opacity: 1,
                        searchList: [],
                        searchText: '',
                      });
                    }}>
                    <Image
                      source={constants.images.cross}
                      style={{height: 16, width: 16, marginStart: 0}}></Image>
                  </TouchableOpacity>
                )}
              </View>

              {this.isSearchOn === false ? (
                <>
                  <TouchableOpacity
                    style={{
                      height: moderateScale(24),
                      width: moderateScale(24),
                      position: 'absolute',
                      right: 0,
                      marginEnd: moderateScale(40),
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: moderateScale(8),
                    }}
                    onPress={() => {
                      this.props.navigation.navigate('Notifications');
                    }}>
                    <Image
                      source={constants.images.notification}
                      style={{
                        position: 'absolute',
                        right: 0,
                        tintColor: constants.colors.white,
                      }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      height: moderateScale(24),
                      width: moderateScale(24),
                      position: 'absolute',
                      alignItems: 'center',
                      justifyContent: 'center',

                      marginTop: moderateScale(8),

                      right: 0,
                    }}
                    onPress={() => {
                      user.KYC === true &&
                        this.props.navigation.navigate('YourCartScreen');
                    }}>
                    <Image
                      source={constants.images.cart}
                      style={{
                        marginStart: moderateScale(16),
                        position: 'absolute',
                        right: 0,
                        tintColor: constants.colors.white,
                      }}></Image>
                  </TouchableOpacity>
                </>
              ) : null}
            </View>
            {/* </LinearGradient> */}
          </View>
          <PTRView onRefresh={this._refresh}>
            <ScrollView
              style={{flex: 1, opacity: this.state.opacity}}
              keyboardShouldPersistTaps={'always'}>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: constants.colors.white,
                }}>
                <View style={{height: 190, width: '100%', marginTop: -4}}>
                  {this.state.imageData && this.state.imageData.length > 0 ? (
                    <SwiperFlatList
                      autoplay
                      autoplayDelay={2}
                      autoplayLoop
                      data={this.state.imageData}
                      paginationDefaultColor={'#D5D5D5'}
                      paginationActiveColor={constants.colors.pink}
                      paginationStyleItem={{
                        width: moderateScale(8),
                        height: moderateScale(8),
                        borderRadius: 5,
                        borderColor: 'white',
                        borderWidth: 1,
                        // position:'absolute',
                        // left:0,
                        // marginStart:24
                      }}
                      renderItem={({item}) => (
                        <TouchableOpacity
                          onPress={() => {
                            let data1 = {};
                            data1.Product_Name = 'Search Products';
                            data1.type = 5;
                            data1.prod_ids = item.products;
                            this.props.navigation.navigate(
                              'SearchResultsScreen',
                              {
                                data: data1,
                              },
                            );
                          }}
                          style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            // paddingTop: 6,
                          }}>
                          <ImageBackground
                            style={styles.advImage}
                            source={{uri: item.img}}
                            resizeMode={'stretch'}>
                            <View style={styles.dotsContainer}></View>
                          </ImageBackground>
                        </TouchableOpacity>
                      )}
                      showPagination
                    />
                  ) : null}
                </View>

                {/* <View style={styles.categoriesContainer}> */}
                <FlatList
                  data={this.state.categoryData}
                  // horizontal={true}
                  // style={{
                  //   marginVertical: moderateScale(4),
                  //   marginHorizontal: moderateScale(0),
                  // }}
                  contentContainerStyle={{
                    width: width,
                    alignSelf: 'flex-start',
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    flexWrap: 'wrap',
                    // alignItems:'center',
                    marginVertical: moderateScale(4),
                    marginHorizontal: moderateScale(0),
                  }}
                  showsHorizontalScrollIndicator={false}
                  ListEmptyComponent={this.renderEmptyListComponent()}
                  renderItem={({item, index}) => (
                    <TouchableOpacity
                      onPress={() => {
                        let data = {};
                        data['Product_Name'] = item.cat_name;
                        // data['type'] = 0;
                        data['cat_id'] = item.cat_id;

                        if (item.have_sub_cat === true) {
                          this.props.navigation.navigate(
                            'CategoryExpandScreen',
                            {
                              data: data,
                            },
                          );
                        } else {
                          this.props.navigation.navigate(
                            'SearchResultsScreen',
                            {
                              data: data,
                            },
                          );
                        }
                      }}
                      style={[
                        styles.categoryItemView,
                        {
                          // marginStart:
                          //   index === 0
                          //     ? moderateScale(16)
                          //     : moderateScale(4),
                          paddingBottom: moderateScale(4),
                          // backgroundColor: constants.colors.defaultColor,
                        },
                      ]}>
                      <Image
                        style={styles.categoryGridImage}
                        source={{uri: item.cat_img}}
                        // resizeMode={'stretch'}
                      ></Image>
                      <View
                        style={{
                          // backgroundColor: constants.colors.defaultColor,
                          paddingTop: moderateScale(4),
                          paddingHorizontal: moderateScale(8),

                          // width: '10%',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            fontSize: textScale(12),
                            fontFamily: fonts.bold,
                            // fontWeight: 'bold',
                            alignSelf: 'center',
                            textAlign: 'center',
                            color: constants.colors.black,
                            width: moderateScale(70),
                          }}>
                          {item.cat_name}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )}
                />
                {/* </View> */}

                {isAdVisible === true ? (
                  <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      flexDirection: 'row',
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        let item = {};
                        item.Product_Name = 'Search Products';
                        item.type = 5;
                        item.prod_ids = this.state.adData.products1;
                        this.props.navigation.navigate('SearchResultsScreen', {
                          data: item,
                        });
                      }}>
                      <ImageBackground
                        style={[
                          styles.halfAdvImage,
                          {height: moderateScale(138)},
                        ]}
                        source={{uri: adData.img1}}
                        resizeMode={'stretch'}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        let item = {};
                        item.Product_Name = 'Search Products';
                        item.type = 5;
                        item.prod_ids = this.state.adData.products2;
                        this.props.navigation.navigate('SearchResultsScreen', {
                          data: item,
                        });
                      }}>
                      <ImageBackground
                        style={[
                          styles.halfAdvImage,
                          {height: moderateScale(138), marginStart: 8},
                        ]}
                        source={{uri: adData.img2}}
                        resizeMode={'stretch'}
                      />
                    </TouchableOpacity>
                  </View>
                ) : null}
                {/* <View style={styles.seePriceTextView}>
                <View style={styles.smallMobileImageView}>
                  <Image
                    source={constants.images.mobileVerification}
                    style={{height: 16, width: 16}}></Image>
                </View>
                <Text
                  style={styles.seePriceText}
                  color={constants.colors.white}>
                  {'Only shop owners like you can see wholesale prices'}
                </Text>
              </View> */}
                {/* {user.KYC === false ? (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('KYCStepOneScreen')
                    }
                    style={{
                      marginTop: moderateScale(4),
                      marginHorizontal: moderateScale(16),
                      width: '100%',
                      backgroundColor: constants.colors.white,
                      // borderColor: constants.colors.red,
                      // borderWidth: moderateScale(1),
                      // borderRadius: moderateScale(8),
                      paddingVertical: moderateScale(8),
                    }}>
                    <Text
                      style={styles.uploadKycText}
                      color={constants.colors.white}>
                      {'UPLOAD KYC'}
                    </Text>
                    <Text
                      style={styles.uploadKycDescText}
                      color={constants.colors.white}>
                      {'Submit anyone of the listing documents'}
                    </Text>
                  </TouchableOpacity>
                ) : null} */}

                {user.KYC === false ? (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('KYCStepOneScreen')
                    }
                    style={{
                      marginTop: moderateScale(8),
                      width: '90%',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderColor: constants.colors.pink,
                      borderWidth: moderateScale(1),
                      borderRadius: moderateScale(64),
                      paddingVertical: moderateScale(16),
                      backgroundColor: constants.colors.white,
                      flexDirection: 'row',
                      marginBottom: moderateScale(16),
                      borderStyle: 'dashed',
                    }}>
                    <Text
                      style={[
                        styles.uploadKycDescText,
                        {
                          width: '50%',
                          backgroundColor: null,
                          color: constants.colors.pink,
                          alignSelf: 'center',
                          fontSize: RFValue(14),
                          marginTop: moderateScale(4),
                        },
                      ]}>
                      {'Submit anyone of the listing documents'}
                    </Text>

                    <View style={styles.uploadKycText}>
                      <LinearGradient
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 3.5}}
                        colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                        style={{
                          position: 'absolute',
                          top: 0,
                          left: 0,
                          right: 0,
                          height: 42,
                          borderRadius: moderateScale(16),
                        }}

                        // useAngle = {true}
                        // angle = {75}
                        // angleCenter = {{x:0.5,y:0.5}}
                      />
                      <Text
                        style={{
                          fontFamily: fonts.regular,
                          color: constants.colors.white,
                          alignSelf: 'center',
                          textAlign: 'center',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                        color={constants.colors.white}>
                        {'UPLOAD KYC'}
                      </Text>
                      <Image
                        source={constants.images.backArrowBlack}
                        style={{
                          rotation: 180,
                          height: 24,
                          width: 24,
                          borderRadius: 12,
                          backgroundColor: '#E03C3C8A',
                        }}
                        tintColor={constants.colors.white}></Image>
                    </View>
                  </TouchableOpacity>
                ) : null}

                <View
                  style={{
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                    width: '100%',
                    alignItems: 'center',
                    marginTop: moderateScale(16),
                    // marginEnd: moderateScale(16),
                    paddingBottom: moderateScale(16),
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 3.5}}
                    colors={['#E03C3C', '#FF9403', '#FF9403', '#FF9403']}
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: 250,
                      height: 200,
                      borderBottomRightRadius: moderateScale(200),
                      borderBottomLeftRadius: moderateScale(4),
                      // marginTop:30
                    }}
                  />
                  <View
                    style={{
                      marginTop: moderateScale(16),
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                      paddingHorizontal: moderateScale(16),
                      // paddingTop: moderateScale(16),
                      alignItems: 'center',
                    }}>
                    <Text
                      style={[
                        styles.seePriceText,
                        {
                          fontSize: textScale(18),
                          color: constants.colors.white,
                        },
                      ]}>
                      {responseData.Latest_Product_Title}
                    </Text>

                    <TouchableOpacity
                      onPress={() => {
                        let data = {};
                        data['Product_Name'] =
                          responseData.Latest_Product_Title;
                        data['type'] = 7;
                        data['productType'] = 'BestPicks';
                        this.props.navigation.navigate('SearchResultsScreen', {
                          data: data,
                        });
                      }}
                      style={{
                        alignItems: 'center',
                        borderRadius: moderateScale(40),
                        // backgroundColor: constants.colors.pink,
                        paddingVertical: moderateScale(4),
                        marginEnd: moderateScale(16),
                        width: moderateScale(80),
                        flexDirection: 'row',
                        height: 30,
                      }}>
                      <LinearGradient
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 2.7}}
                        colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                        style={{
                          position: 'absolute',
                          top: 0,
                          left: 0,
                          right: 0,
                          height: 30,
                          borderRadius: moderateScale(40),
                        }}

                        // useAngle = {true}
                        // angle = {75}
                        // angleCenter = {{x:0.5,y:0.5}}
                      />
                      <Text
                        style={[
                          styles.seePriceText,
                          {
                            fontSize: textScale(12),
                            textAlign: 'center',
                            fontFamily: fonts.bold,
                            color: constants.colors.white,
                          },
                        ]}>
                        {'VIEW ALL'}
                      </Text>
                      {/* <Image
                        source={constants.images.dropdown}
                        style={{
                          tintColor: constants.colors.skyBlue,
                          rotation: 270,
                        }}
                      /> */}
                    </TouchableOpacity>
                  </View>
                  <FlatList
                    data={latestProducts.slice(0, 4)}
                    // horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    ListEmptyComponent={this.renderEmptyListComponent()}
                    contentContainerStyle={{
                      width: width - 24,
                      alignSelf: 'flex-start',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      flexWrap: 'wrap',
                      alignItems: 'center',
                      paddingHorizontal: 16,
                      marginTop: 16,
                      paddingBottom: 8,
                    }}
                    renderItem={({item}) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate(
                            'ProductDetailsScreen',
                            {
                              id: item.Product_Id,
                            },
                          );
                        }}
                        style={styles.gridItemView}>
                        <Image
                          style={styles.gridImage}
                          source={{uri: item.Product_Image}}
                          resizeMode={'contain'}></Image>

                        <View
                          style={{
                            height: 2,
                            width: '100%',
                            backgroundColor: '#E5E5E5',
                            marginTop: 8,
                          }}
                        />
                        <View
                          style={{
                            padding: moderateScale(8),
                            width: '100%',
                          }}>
                          <Text
                            style={{
                              fontSize: textScale(13),
                              fontFamily: fonts.regular,
                            }}>
                            {item.Product_Name !== undefined
                              ? item.Product_Name.length > 30
                                ? `${item.Product_Name.substring(0, 30)}...`
                                : item.Product_Name
                              : ''}
                          </Text>
                          {/* {user.KYC === true ? ( */}
                          <Text
                            style={{
                              fontSize: textScale(16),
                              fontFamily: fonts.bold,
                              color: constants.colors.pink,
                              marginTop: 8,
                            }}>
                            {user.KYC === true
                              ? `Price : ${
                                  constants.strings.rupee_symbol
                                } ${parseFloat(item.Selling_Price).toFixed(2)}`
                              : `Price : --/--`}
                          </Text>
                          {/* ) : null} */}
                        </View>
                      </TouchableOpacity>
                    )}
                  />
                </View>

                {isAdVisible === true ? (
                  <TouchableOpacity
                    onPress={() => {
                      let item = {};
                      item.Product_Name = 'Search Products';
                      item.type = 5;
                      item.prod_ids = this.state.adData.products3;
                      this.props.navigation.navigate('SearchResultsScreen', {
                        data: item,
                      });
                    }}
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      width: '95%',
                      marginTop: 16,
                    }}>
                    <ImageBackground
                      style={[styles.advImage, {height: moderateScale(170)}]}
                      source={{uri: adData.img3}}
                      // resizeMode={'stretch'}
                    />
                  </TouchableOpacity>
                ) : null}

                <View
                  style={{
                    marginTop: moderateScale(8),
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    width: '100%',
                    paddingHorizontal: moderateScale(24),
                    paddingTop: moderateScale(16),
                    alignItems: 'center',
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 3.5}}
                    colors={['#E03C3C', '#FF9403', '#FF9403', '#FF9403']}
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: 250,
                      height: 200,
                      borderBottomRightRadius: moderateScale(200),
                      borderBottomLeftRadius: moderateScale(4),
                      // marginTop:30
                    }}
                  />
                  <Text
                    style={[
                      styles.seePriceText,
                      {fontSize: textScale(18), color: constants.colors.white},
                    ]}>
                    {responseData.Flagship_Brands_Title}
                  </Text>

                  <TouchableOpacity
                    onPress={() => {
                      null;
                      // let data = {};
                      // data['Product_Name'] = 'LATEST PRODUCTS';
                      // data['type'] = 1;
                      // this.props.navigation.navigate('SearchResultsScreen', {
                      //   data: data,
                      // });
                    }}
                    style={{
                      alignItems: 'center',
                      borderRadius: moderateScale(40),
                      paddingVertical: moderateScale(4),
                      marginEnd: moderateScale(16),
                      width: moderateScale(80),
                      flexDirection: 'row',
                      height: 30,
                    }}>
                    {/* <LinearGradient
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 2.7}}
                      colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                      style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        height: 30,
                        borderRadius: moderateScale(40),
                      }}
                    />
                    <Text
                      style={[
                        styles.seePriceText,
                        {
                          fontSize: textScale(12),
                          textAlign: 'center',
                          fontFamily: fonts.bold,
                          color: constants.colors.white,
                        },
                      ]}>
                      {'VIEW ALL'}
                    </Text> */}
                  </TouchableOpacity>
                  <View />
                </View>

                <View
                  style={{
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                    width: '95%',
                    alignItems: 'center',
                    marginTop: moderateScale(16),
                    marginStart: moderateScale(16),
                    marginEnd: moderateScale(16),
                    paddingBottom: moderateScale(16),
                  }}>
                  <FlatList
                    data={flagshipBrands.slice(0, 4)}
                    // horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    ListEmptyComponent={this.renderEmptyListComponent()}
                    contentContainerStyle={{
                      width: width - 24,
                      alignSelf: 'flex-start',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      flexWrap: 'wrap',
                      alignItems: 'center',
                      paddingHorizontal: 16,
                      paddingBottom: 8,
                    }}
                    renderItem={({item}) => (
                      <TouchableOpacity
                        onPress={() => {
                          let data1 = {};
                          data1.Product_Name = 'Search Products';
                          data1.type = 5;
                          data1.prod_ids = item.products;
                          this.props.navigation.navigate(
                            'SearchResultsScreen',
                            {
                              data: data1,
                            },
                          );
                        }}
                        style={[styles.gridItemView, {height: 120}]}>
                        <Image
                          style={styles.flagImage}
                          source={{uri: item.img}}
                          resizeMode={'contain'}></Image>
                        {/* <View
                          style={{
                            padding: moderateScale(8),
                            width: '100%',
                            // alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              fontSize: textScale(14),
                              fontFamily: fonts.regular,
                            }}>
                            {item.Product_Name !== undefined
                              ? item.Product_Name.length > 20
                                ? `${item.Product_Name.substring(0, 20)}...`
                                : item.Product_Name
                              : ''}
                          </Text>
                          {user.KYC === true ? (
                            <Text
                              style={{
                                fontSize: textScale(12),
                                fontFamily: fonts.bold,
                              }}>{`Price : ${
                              constants.strings.rupee_symbol
                            } ${parseFloat(item.Selling_Price).toFixed(
                              2,
                            )}`}</Text>
                          ) : null}
                        </View> */}
                      </TouchableOpacity>
                    )}
                  />
                </View>

                {isAdVisible === true ? (
                  <TouchableOpacity
                    onPress={() => {
                      let item = {};
                      item.Product_Name = 'Search Products';
                      item.type = 5;
                      item.prod_ids = this.state.adData.products4;
                      this.props.navigation.navigate('SearchResultsScreen', {
                        data: item,
                      });
                    }}
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      width: '95%',
                      marginTop: 16,
                    }}>
                    <ImageBackground
                      style={[styles.advImage, {height: moderateScale(100)}]}
                      source={{uri: adData.img4}}
                      resizeMode={'stretch'}
                    />
                  </TouchableOpacity>
                ) : null}

                <View
                  style={{
                    marginTop: moderateScale(8),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: '100%',
                    paddingStart: moderateScale(16),
                    paddingTop: moderateScale(16),
                    alignItems: 'center',
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 3.5}}
                    colors={['#E03C3C', '#FF9403', '#FF9403', '#FF9403']}
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: 240,
                      height: 200,
                      borderBottomRightRadius: moderateScale(200),
                      borderBottomLeftRadius: moderateScale(4),
                      // marginTop:30
                    }}
                  />
                  <Text
                    style={[
                      styles.seePriceText,
                      {
                        fontSize: textScale(18),
                        color: constants.colors.white,
                        width: '50%',
                        alignSelf: 'flex-end',
                      },
                    ]}>
                    {responseData.Featured_Product_Title}
                  </Text>

                  <TouchableOpacity
                    onPress={() => {
                      let data = {};
                      data['Product_Name'] =
                        responseData.Featured_Product_Title;
                      data['type'] = 7;
                      data['productType'] = 'BestSellingFeaturePhones';
                      this.props.navigation.navigate('SearchResultsScreen', {
                        data: data,
                      });
                    }}
                    style={{
                      alignItems: 'center',
                      // borderRadius: moderateScale(8),
                      // backgroundColor: constants.colors.pink,
                      paddingVertical: moderateScale(4),
                      marginEnd: moderateScale(16),
                      width: moderateScale(80),
                      flexDirection: 'row',
                      height: 30,
                    }}>
                    <LinearGradient
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 2.7}}
                      colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                      style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        height: 30,
                        borderRadius: moderateScale(40),
                      }}
                    />

                    <Text
                      style={[
                        styles.seePriceText,
                        {
                          fontSize: textScale(12),
                          textAlign: 'center',
                          fontFamily: fonts.bold,
                          color: constants.colors.white,
                        },
                      ]}>
                      {'VIEW ALL'}
                    </Text>
                    {/* <Image
                      source={constants.images.dropdown}
                      style={{
                        tintColor: constants.colors.skyBlue,
                        rotation: 270,
                      }}
                    /> */}
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                    width: '95%',
                    alignItems: 'center',
                    marginStart: moderateScale(16),
                    marginEnd: moderateScale(16),
                    paddingBottom: moderateScale(16),
                  }}>
                  <FlatList
                    data={featuredProducts.slice(0, 4)}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    ListEmptyComponent={this.renderEmptyListComponent()}
                    contentContainerStyle={{
                      width: width - 24,
                      alignSelf: 'flex-start',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      flexWrap: 'wrap',
                      alignItems: 'center',
                      paddingHorizontal: 16,
                      paddingBottom: moderateScale(8),
                    }}
                    renderItem={({item}) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate(
                            'ProductDetailsScreen',
                            {
                              id: item.Product_Id,
                            },
                          );
                        }}
                        style={styles.gridItemView}>
                        <Image
                          style={styles.gridImage}
                          source={{uri: item.Product_Image}}
                          resizeMode={'contain'}></Image>
                        <View
                          style={{
                            padding: moderateScale(8),
                            width: '100%',
                            // alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              fontSize: textScale(14),
                              fontFamily: fonts.regular,
                            }}>
                            {item.Product_Name !== undefined
                              ? item.Product_Name.length > 20
                                ? `${item.Product_Name.substring(0, 20)}...`
                                : item.Product_Name
                              : ''}
                          </Text>
                          {user.KYC === true ? (
                            <Text
                              style={{
                                fontSize: textScale(16),
                                fontFamily: fonts.bold,
                                color: constants.colors.pink,
                                marginTop: 8,
                              }}>{`Price : ${
                              constants.strings.rupee_symbol
                            } ${parseFloat(item.Selling_Price).toFixed(
                              2,
                            )}`}</Text>
                          ) : null}
                        </View>
                      </TouchableOpacity>
                    )}
                  />
                </View>

                <View
                  style={{
                    marginTop: moderateScale(8),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: '100%',
                    paddingStart: moderateScale(16),
                    paddingTop: moderateScale(16),
                    alignItems: 'center',
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 3.5}}
                    colors={['#E03C3C', '#FF9403', '#FF9403', '#FF9403']}
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: 240,
                      height: 200,
                      borderBottomRightRadius: moderateScale(200),
                      borderBottomLeftRadius: moderateScale(4),
                      // marginTop:30
                    }}
                  />
                  <Text
                    style={[
                      styles.seePriceText,
                      {
                        fontSize: textScale(18),
                        color: constants.colors.white,
                        width: '50%',
                        alignSelf: 'flex-end',
                      },
                    ]}>
                    {responseData.Top_Selling_Product_Title}
                  </Text>

                  <TouchableOpacity
                    onPress={() => {
                      let data = {};
                      data['Product_Name'] =
                        responseData.Top_Selling_Product_Title;
                      data['type'] = 7;
                      data['productType'] = 'BestSellingSmartPhones';
                      this.props.navigation.navigate('SearchResultsScreen', {
                        data: data,
                      });
                    }}
                    style={{
                      alignItems: 'center',
                      // borderRadius: moderateScale(8),
                      // backgroundColor: constants.colors.pink,
                      paddingVertical: moderateScale(4),
                      marginEnd: moderateScale(16),
                      width: moderateScale(80),
                      flexDirection: 'row',
                      height: 30,
                    }}>
                    <LinearGradient
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 2.7}}
                      colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                      style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        height: 30,
                        borderRadius: moderateScale(40),
                      }}
                    />
                    <Text
                      style={[
                        styles.seePriceText,
                        {
                          fontSize: textScale(12),
                          textAlign: 'center',
                          fontFamily: fonts.bold,
                          color: constants.colors.white,
                        },
                      ]}>
                      {'VIEW ALL'}
                    </Text>
                    {/* <Image
                      source={constants.images.dropdown}
                      style={{
                        tintColor: constants.colors.skyBlue,
                        rotation: 270,
                      }}
                    /> */}
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                    width: '95%',
                    alignItems: 'center',
                    marginStart: moderateScale(16),
                    marginEnd: moderateScale(16),
                    paddingBottom: moderateScale(16),
                  }}>
                  <FlatList
                    data={topSellingProducts.slice(0, 4)}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    ListEmptyComponent={this.renderEmptyListComponent()}
                    contentContainerStyle={{
                      width: width - 24,
                      alignSelf: 'flex-start',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      flexWrap: 'wrap',
                      alignItems: 'center',
                      paddingHorizontal: 16,
                      paddingBottom: moderateScale(8),
                    }}
                    renderItem={({item}) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate(
                            'ProductDetailsScreen',
                            {
                              id: item.Product_Id,
                            },
                          );
                        }}
                        style={styles.gridItemView}>
                        <Image
                          style={styles.gridImage}
                          source={{uri: item.Product_Image}}
                          resizeMode={'contain'}></Image>
                        <View
                          style={{
                            padding: moderateScale(8),
                            width: '100%',
                            // alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              fontSize: textScale(14),
                              fontFamily: fonts.regular,
                            }}>
                            {item.Product_Name !== undefined
                              ? item.Product_Name.length > 20
                                ? `${item.Product_Name.substring(0, 20)}...`
                                : item.Product_Name
                              : ''}
                          </Text>
                          {user.KYC === true ? (
                            <Text
                              style={{
                                fontSize: textScale(16),
                                fontFamily: fonts.bold,
                                color: constants.colors.pink,
                                marginTop: 8,
                              }}>{`Price : ${
                              constants.strings.rupee_symbol
                            } ${parseFloat(item.Selling_Price).toFixed(
                              2,
                            )}`}</Text>
                          ) : null}
                        </View>
                      </TouchableOpacity>
                    )}
                  />
                </View>

                <View
                  style={{
                    marginTop: moderateScale(8),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: '100%',
                    paddingStart: moderateScale(16),
                    paddingTop: moderateScale(16),
                    alignItems: 'center',
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 3.5}}
                    colors={['#E03C3C', '#FF9403', '#FF9403', '#FF9403']}
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: 240,
                      height: 200,
                      borderBottomRightRadius: moderateScale(200),
                      borderBottomLeftRadius: moderateScale(4),
                      // marginTop:30
                    }}
                  />
                  <Text
                    style={[
                      styles.seePriceText,
                      {
                        fontSize: textScale(18),
                        color: constants.colors.white,
                        width: '60%',
                        alignSelf: 'flex-end',
                      },
                    ]}>
                    {responseData.Buy_Popular_Products_Title}
                  </Text>

                  <TouchableOpacity
                    onPress={() => {
                      let data = {};
                      data['Product_Name'] =
                        responseData.Buy_Popular_Products_Title;
                      data['type'] = 7;
                      data['productType'] = 'BuyPopularHeadphones';
                      this.props.navigation.navigate('SearchResultsScreen', {
                        data: data,
                      });
                    }}
                    style={{
                      // alignItems: 'center',
                      // borderRadius: moderateScale(8),
                      // backgroundColor: constants.colors.pink,
                      paddingVertical: moderateScale(4),
                      marginEnd: moderateScale(16),
                      width: moderateScale(75),
                      flexDirection: 'row',
                      height: 30,
                      alignSelf: 'center',
                      alignItems: 'center',
                    }}>
                    <LinearGradient
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 2.7}}
                      colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                      style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        height: 30,
                        borderRadius: moderateScale(40),
                      }}
                    />
                    <Text
                      style={[
                        styles.seePriceText,
                        {
                          fontSize: textScale(12),
                          textAlign: 'center',
                          fontFamily: fonts.bold,
                          color: constants.colors.white,
                        },
                      ]}>
                      {'VIEW ALL'}
                    </Text>
                    {/* <Image
                      source={constants.images.dropdown}
                      style={{
                        tintColor: constants.colors.skyBlue,
                        rotation: 270,
                      }}
                    /> */}
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                    width: '95%',
                    alignItems: 'center',
                    marginStart: moderateScale(16),
                    marginEnd: moderateScale(16),
                    paddingBottom: moderateScale(16),
                  }}>
                  <FlatList
                    data={buyPopularProducts.slice(0, 4)}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    ListEmptyComponent={this.renderEmptyListComponent()}
                    contentContainerStyle={{
                      width: width - 24,
                      alignSelf: 'flex-start',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      flexWrap: 'wrap',
                      alignItems: 'center',
                      paddingHorizontal: 16,
                      paddingBottom: moderateScale(8),
                    }}
                    renderItem={({item}) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate(
                            'ProductDetailsScreen',
                            {
                              id: item.Product_Id,
                            },
                          );
                        }}
                        style={styles.gridItemView}>
                        <Image
                          style={styles.gridImage}
                          source={{uri: item.Product_Image}}
                          resizeMode={'contain'}></Image>
                        <View
                          style={{
                            padding: moderateScale(8),
                            width: '100%',
                            // alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              fontSize: textScale(14),
                              fontFamily: fonts.regular,
                            }}>
                            {item.Product_Name !== undefined
                              ? item.Product_Name.length > 20
                                ? `${item.Product_Name.substring(0, 20)}...`
                                : item.Product_Name
                              : ''}
                          </Text>
                          {user.KYC === true ? (
                            <Text
                              style={{
                                fontSize: textScale(16),
                                fontFamily: fonts.bold,
                                color: constants.colors.pink,
                                marginTop: 8,
                              }}>{`Price : ${
                              constants.strings.rupee_symbol
                            } ${parseFloat(item.Selling_Price).toFixed(
                              2,
                            )}`}</Text>
                          ) : null}
                        </View>
                      </TouchableOpacity>
                    )}
                  />
                </View>

                <View
                  style={{
                    marginTop: moderateScale(8),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: '100%',
                    paddingStart: moderateScale(16),
                    paddingTop: moderateScale(16),
                    alignItems: 'center',
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 3.5}}
                    colors={['#E03C3C', '#FF9403', '#FF9403', '#FF9403']}
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: 240,
                      height: 200,
                      borderBottomRightRadius: moderateScale(200),
                      borderBottomLeftRadius: moderateScale(4),
                      // marginTop:30
                    }}
                  />

                  <Text
                    style={[
                      styles.seePriceText,
                      {
                        fontSize: textScale(18),
                        color: constants.colors.white,
                        width: '50%',
                        alignSelf: 'flex-end',
                      },
                    ]}>
                    {responseData.Best_Selling_Bluetooth_Products_Title}
                  </Text>

                  <TouchableOpacity
                    onPress={() => {
                      let data = {};
                      data['Product_Name'] =
                        responseData.Best_Selling_Bluetooth_Products_Title;
                      data['type'] = 7;
                      data['productType'] = 'BestSellingBluetooth';
                      this.props.navigation.navigate('SearchResultsScreen', {
                        data: data,
                      });
                    }}
                    style={{
                      // alignItems: 'center',
                      // borderRadius: moderateScale(8),
                      // backgroundColor: constants.colors.pink,
                      paddingVertical: moderateScale(4),
                      marginEnd: moderateScale(16),
                      width: moderateScale(75),
                      flexDirection: 'row',
                      height: 30,
                      alignSelf: 'center',
                      alignItems: 'center',
                    }}>
                    <LinearGradient
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 2.7}}
                      colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                      style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        height: 30,
                        borderRadius: moderateScale(40),
                      }}
                    />
                    <Text
                      style={[
                        styles.seePriceText,
                        {
                          fontSize: textScale(12),
                          textAlign: 'center',
                          fontFamily: fonts.bold,
                          color: constants.colors.white,
                        },
                      ]}>
                      {'VIEW ALL'}
                    </Text>
                    {/* <Image
                      source={constants.images.dropdown}
                      style={{
                        tintColor: constants.colors.skyBlue,
                        rotation: 270,
                      }}
                    /> */}
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                    width: '95%',
                    alignItems: 'center',
                    marginStart: moderateScale(16),
                    marginEnd: moderateScale(16),
                    paddingBottom: moderateScale(16),
                  }}>
                  <FlatList
                    data={bestSellingProducts.slice(0, 4)}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    ListEmptyComponent={this.renderEmptyListComponent()}
                    contentContainerStyle={{
                      width: width - 24,
                      alignSelf: 'flex-start',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      flexWrap: 'wrap',
                      alignItems: 'center',
                      paddingHorizontal: 16,
                      paddingBottom: moderateScale(8),
                    }}
                    renderItem={({item}) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate(
                            'ProductDetailsScreen',
                            {
                              id: item.Product_Id,
                            },
                          );
                        }}
                        style={styles.gridItemView}>
                        <Image
                          style={styles.gridImage}
                          source={{uri: item.Product_Image}}
                          resizeMode={'contain'}></Image>
                        <View
                          style={{
                            padding: moderateScale(8),
                            width: '100%',
                            // alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              fontSize: textScale(14),
                              fontFamily: fonts.regular,
                            }}>
                            {item.Product_Name !== undefined
                              ? item.Product_Name.length > 20
                                ? `${item.Product_Name.substring(0, 20)}...`
                                : item.Product_Name
                              : ''}
                          </Text>
                          {user.KYC === true ? (
                            <Text
                              style={{
                                fontSize: textScale(16),
                                fontFamily: fonts.bold,
                                color: constants.colors.pink,
                                marginTop: 8,
                              }}>{`Price : ${
                              constants.strings.rupee_symbol
                            } ${parseFloat(item.Selling_Price).toFixed(
                              2,
                            )}`}</Text>
                          ) : null}
                        </View>
                      </TouchableOpacity>
                    )}
                  />
                </View>

                <View
                  style={{
                    marginTop: moderateScale(8),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: '100%',
                    paddingStart: moderateScale(16),
                    // paddingEnd:moderateScale(8),
                    paddingTop: moderateScale(16),
                    alignItems: 'center',
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 3.5}}
                    colors={['#E03C3C', '#FF9403', '#FF9403', '#FF9403']}
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: 240,
                      height: 200,
                      borderBottomRightRadius: moderateScale(200),
                      borderBottomLeftRadius: moderateScale(4),
                      // marginTop:30
                    }}
                  />
                  <Text
                    style={[
                      styles.seePriceText,
                      {
                        fontSize: textScale(18),
                        color: constants.colors.white,
                        width: '50%',
                        alignSelf: 'flex-end',
                      },
                    ]}>
                    {responseData.Best_Selling_Mobile_Products_Title}
                  </Text>

                  <TouchableOpacity
                    onPress={() => {
                      let data = {};
                      data['Product_Name'] =
                        responseData.Best_Selling_Mobile_Products_Title;
                      data['type'] = 7;
                      data['productType'] = 'BestSellingMobileCharger';
                      this.props.navigation.navigate('SearchResultsScreen', {
                        data: data,
                      });
                    }}
                    style={{
                      // alignItems: 'center',
                      // borderRadius: moderateScale(8),
                      // backgroundColor: constants.colors.pink,
                      paddingVertical: moderateScale(4),
                      marginEnd: moderateScale(16),
                      width: moderateScale(75),
                      flexDirection: 'row',
                      height: 30,
                      alignSelf: 'center',
                      alignItems: 'center',
                    }}>
                    <LinearGradient
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 2.7}}
                      colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                      style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        height: 30,
                        borderRadius: moderateScale(40),
                      }}
                    />
                    <Text
                      style={[
                        styles.seePriceText,
                        {
                          fontSize: textScale(12),
                          textAlign: 'center',
                          fontFamily: fonts.bold,
                          color: constants.colors.white,
                        },
                      ]}>
                      {'VIEW ALL'}
                    </Text>
                    {/* <Image
                      source={constants.images.dropdown}
                      style={{
                        tintColor: constants.colors.skyBlue,
                        rotation: 270,
                      }}
                    /> */}
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    backgroundColor: 'rgba(246, 121, 20, 0.1)',
                    width: '95%',
                    alignItems: 'center',
                    marginStart: moderateScale(16),
                    marginEnd: moderateScale(16),
                    paddingBottom: moderateScale(16),
                  }}>
                  <FlatList
                    data={bestSellingMobileProducts.slice(0, 4)}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    ListEmptyComponent={this.renderEmptyListComponent()}
                    contentContainerStyle={{
                      width: width - 24,
                      alignSelf: 'flex-start',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      flexWrap: 'wrap',
                      alignItems: 'center',
                      paddingHorizontal: 16,
                      paddingBottom: moderateScale(8),
                    }}
                    renderItem={({item}) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate(
                            'ProductDetailsScreen',
                            {
                              id: item.Product_Id,
                            },
                          );
                        }}
                        style={styles.gridItemView}>
                        <Image
                          style={styles.gridImage}
                          source={{uri: item.Product_Image}}
                          resizeMode={'contain'}></Image>
                        <View
                          style={{
                            padding: moderateScale(8),
                            width: '100%',
                            // alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              fontSize: textScale(14),
                              fontFamily: fonts.regular,
                            }}>
                            {item.Product_Name !== undefined
                              ? item.Product_Name.length > 20
                                ? `${item.Product_Name.substring(0, 20)}...`
                                : item.Product_Name
                              : ''}
                          </Text>
                          {user.KYC === true ? (
                            <Text
                              style={{
                                fontSize: textScale(16),
                                fontFamily: fonts.bold,
                                color: constants.colors.pink,
                                marginTop: 8,
                              }}>{`Price : ${
                              constants.strings.rupee_symbol
                            } ${parseFloat(item.Selling_Price).toFixed(
                              2,
                            )}`}</Text>
                          ) : null}
                        </View>
                      </TouchableOpacity>
                    )}
                  />
                </View>

                {/* {user.KYC === false ? (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('KYCStepOneScreen')
                    }
                    style={{
                      marginTop: moderateScale(8),
                      marginHorizontal: moderateScale(16),
                      width: '90%',
                      borderColor: constants.colors.red,
                      borderWidth: moderateScale(1),
                      borderRadius: moderateScale(8),
                      paddingVertical: moderateScale(16),
                      backgroundColor: constants.colors.blue_light,
                      flexDirection: 'row',
                      marginBottom: moderateScale(16),
                    }}>
                    <Text
                      style={[
                        styles.uploadKycDescText,
                        {
                          width: '60%',
                          backgroundColor: null,
                          color: constants.colors.black,
                          fontSize: RFValue(18),
                        },
                      ]}>
                      {'UNLOCK PRIZES OF 2 LAKHS + LISTINGS'}
                    </Text>

                    <Text
                      style={[
                        styles.uploadKycText,
                        {
                          width: moderateScale(120),
                          borderTopRightRadius: 0,
                          borderBottomRightRadius: 0,
                        },
                      ]}
                      color={constants.colors.white}>
                      {'UPLOAD KYC'}
                    </Text>
                  </TouchableOpacity>
                ) : null} */}
              </View>
            </ScrollView>
          </PTRView>
          {searchText && searchText.length > 0 && searchList.length > 0 ? (
            <FlatList
              data={this.state.searchList}
              scrollEnabled={true}
              contentContainerStyle={{marginTop: 24}}
              ItemSeparatorComponent={this.itemSeprator}
              // ListEmptyComponent = {this.emptyListComponent}
              style={{
                position: 'absolute',
                top: 0,
                marginTop: moderateScale(56),
                width: '100%',
              }}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  style={[
                    styles.labletextRowContainer,
                    {paddingHorizontal: 16},
                  ]}
                  onPress={() => {
                    // item.type = 0;
                    Keyboard.dismiss();
                    if (item.Search_type === 'Supplier') {
                      item.type = 4;
                      item.seller_id = item.Product_Id;
                      this.props.navigation.navigate('SearchResultsScreen', {
                        data: item,
                      });
                    } else if (item.Search_type === 'Product') {
                      item.type = 0;
                      item.cat_id = item.Product_Id;
                      this.props.navigation.navigate('SearchResultsScreen', {
                        data: item,
                      });
                    } else {
                      item.cat_id = item.Product_Id;
                      if (item.have_sub_cat === true) {
                        this.props.navigation.navigate('CategoryExpandScreen', {
                          data: item,
                        });
                      } else {
                        this.props.navigation.navigate('SearchResultsScreen', {
                          data: item,
                        });
                      }
                    }
                  }}>
                  <Image
                    source={constants.images.search}
                    style={{height: 20, width: 20}}
                  />
                  {item.Search_type === 'Supplier' ? (
                    <Text
                      style={{
                        marginLeft: moderateScale(3),
                        opacity: 0.7,
                        marginLeft: moderateScale(16),
                        paddingHorizontal: moderateScale(4),
                        paddingVertical: moderateScale(4),
                        backgroundColor: constants.colors.blue_light,
                        color: constants.colors.pink,
                        fontSize: textScale(12),
                        fontFamily: fonts.bold,
                      }}>
                      Supplier
                    </Text>
                  ) : null}
                  <Text
                    style={{
                      marginLeft: moderateScale(3),
                      opacity: 0.7,
                      fontFamily: fonts.regular,
                      paddingStart:
                        item.Search_type === 'Supplier'
                          ? moderateScale(4)
                          : moderateScale(16),
                    }}>
                    {item.Product_Name !== undefined
                      ? item.Product_Name.length > 20
                        ? `${item.Product_Name.substring(0, 20)}...`
                        : item.Product_Name
                      : ''}
                  </Text>
                </TouchableOpacity>
              )}
            />
          ) : null}

          {categoryClicked ? (
            <View
              style={{
                position: 'absolute',
                bottom: 0,
                width: width,
                backgroundColor: constants.colors.white,
              }}>
              <Text
                style={{
                  marginVertical: moderateScale(12),
                  marginHorizontal: moderateScale(24),
                  fontWeight: 'bold',
                }}>
                Select Category
              </Text>

              <FlatGrid
                data={this.state.sellingProductsData}
                style={{backgroundColor: '#F0F0F0'}}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() => {
                      actions.hideCategoryView(false);
                      // this.props.navigation.navigate('VideoPlayer', {
                      //   url: item.file_url,
                      // });
                    }}
                    style={[
                      styles.gridItemView,
                      {
                        backgroundColor: constants.colors.red_bg,
                        paddingTop: moderateScale(8),
                      },
                    ]}>
                    <Image
                      style={styles.circleImage}
                      source={{uri: item}}></Image>
                    <View
                      style={{
                        backgroundColor: constants.colors.white,
                        padding: moderateScale(4),
                        marginTop: moderateScale(8),
                        width: '100%',
                        alignItems: 'center',
                      }}>
                      <Text>Mac Book Pro</Text>
                      <Text>Apple</Text>
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          ) : null}
        </View>
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth, category}) => ({
  user: auth.userData,
  categoryClicked: category.setCategoryShown,
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

function RefreshData({onUpdate}) {
  useFocusEffect(
    React.useCallback(() => {
      new HomeScreen()._storeData('true');
      console.log('isFocuseddddddddddddddddddddddddd');
      onUpdate();
    }, [onUpdate]),
  );

  return null;
}
