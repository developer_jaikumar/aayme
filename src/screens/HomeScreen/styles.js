import {RFValue} from 'react-native-responsive-fontsize';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {fonts} from '../../assets';
import {moderateScaleVertical, textScale} from '../../utilities/responsiveSize';
import {Dimensions, StyleSheet} from 'react-native';
import constants from '../../utilities/index';

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: constants.colors.red,
  },

  labletextRowContainer: {
    flexDirection: 'row',
    paddingVertical: moderateScale(12),
    backgroundColor: constants.colors.white,
    width: '100%',
  },

  filterContainer: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
  },
  emailItem: {
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.3)',
    padding: 10,
  },
  emailSubject: {
    color: 'rgba(0,0,0,0.5)',
  },
  searchInput: {
    padding: 10,
    borderColor: '#CCC',
    borderWidth: 1,
  },

  menuHeader: {
    height: moderateScale(100),
    // backgroundColor: constants.colors.pink,
  },

  subMenuHeader: {
    flexDirection: 'row',
    marginHorizontal: moderateScale(16),
    marginTop: moderateScale(-8),
    marginBottom: 8,
    // alignItems: 'center',
  },

  categoriesContainer: {
    width: '100%',
    // height: moderateScale(120),
    backgroundColor: constants.colors.white,
    alignItems: 'center',
    // marginTop:moderateScale(20),
    // borderTopLeftRadius:moderateScale(24),
    // borderTopRightRadius:moderateScale(24),
  },

  dotsContainer: {
    width: '100%',
    height: moderateScale(20),
    // backgroundColor: constants.colors.white,
    // marginTop:moderateScale(20),
    // borderTopLeftRadius:moderateScale(24),
    // borderTopRightRadius:moderateScale(24),
    // position:'absolute',
    // left:0,
    // right:0,
    // bottom:0
  },

  searchEditTextView: {
    borderRadius: 4,
    // borderWidth: 1,
    marginStart: moderateScale(8),
    backgroundColor: '#EEEEEE',
    // borderColor: constants.colors.lightBlack,
    borderRadius: moderateScale(8),
    width: '65%',
    flexDirection: 'row',
    alignItems: 'center',
    height: moderateScale(40),
  },

  advImage: {
    width: width,
    height: moderateScale(175),
  },

  halfAdvImage: {
    width: width / 2,
    height: moderateScale(160),
  },

  gridItemView: {
    alignItems: 'center',
    justifyContent: 'center',
    width: width / 2 - 32,
    marginTop: 8,
    height: 193,
    shadowColor: '#ffffff',
    shadowOpacity: 4,
    shadowRadius: 16,
    shadowOffset: {
      height: 4,
      width: 4,
    },
    elevation:5,

    // marginStart:moderateScale(8),
    // marginEnd:moderateScale(8),
    borderRadius: moderateScale(5),
    // borderWidth: moderateScale(0.5),
    // borderColor: constants.colors.skyBlue,
    backgroundColor: constants.colors.white,
  },

  gridImage: {
    width: moderateScale(120),
    marginTop: moderateScale(4),
    height: moderateScale(80),
    borderRadius: moderateScale(4),
    overflow: 'hidden',
  },

  flagImage: {
    width: moderateScale(120),
    marginTop: moderateScale(4),
    height: moderateScale(120),
    borderRadius: moderateScale(4),
    overflow: 'hidden',
  },

  categoryItemView: {
    alignItems: 'center',
    paddingTop: moderateScale(8),
    // backgroundColor:constants.colors.defaultColor,
    // borderTopLeftRadius:moderateScale(50),
    // borderTopRightRadius:moderateScale(50),
    // borderBottomRightRadius:moderateScale(16),
    // borderBottomLeftRadius:moderateScale(16)
  },

  categoryGridImage: {
    width: moderateScale(66),
    height: moderateScale(66),
    borderWidth: moderateScale(0.5),
    borderColor: constants.colors.btnGray,
    borderRadius: moderateScale(33),
  },

  circleItemView: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: moderateScale(4),
    borderWidth: moderateScale(1),
    paddingBottom: moderateScale(4),
    borderColor: constants.colors.separatorColor,
  },

  circleImage: {
    width: moderateScale(40),
    height: moderateScale(40),
    borderRadius: moderateScale(20),
  },

  seePriceTextView: {
    borderRadius: 4,
    borderWidth: 1,
    marginStart: moderateScale(16),
    marginTop: moderateScale(16),
    backgroundColor: constants.colors.pink,
    borderColor: constants.colors.lightBlack,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: moderateScale(8),
  },

  seePriceText: {
    fontSize: RFValue(12),
    marginStart: moderateScale(8),
    width: '80%',
    // fontWeight: 'bold',
    fontFamily: fonts.bold,
    color: constants.colors.black,
  },

  uploadKycText: {
    fontSize: RFValue(16),
    paddingEnd: moderateScale(8),
    backgroundColor: constants.colors.pink,
    alignItems: 'center',
    width: '48%',
    borderRadius: moderateScale(16),
    paddingHorizontal: moderateScale(8),
    paddingVertical: moderateScale(8),
    flexDirection: 'row',
    justifyContent: 'space-around',
  },

  uploadKycDescText: {
    fontSize: RFValue(10),
    paddingHorizontal: moderateScale(16),
    borderRadius: moderateScale(16),
    alignSelf: 'center',
    // paddingVertical: moderateScale(8),
    textAlign: 'center',
    fontFamily: fonts.regular,
    color: constants.colors.skyBlue,
  },

  mobileNuberVerificationText: {
    fontSize: RFValue(20),
    fontFamily: fonts.bold,
    color: constants.colors.black,
    marginTop: 24,
  },

  enterMobileText: {
    fontSize: RFValue(14),
    fontFamily: fonts.bold,
    color: constants.colors.lightBlack,
    textAlign: 'center',
    width: '60%',
    marginTop: 8,
  },

  mobileEditTextView: {
    marginTop: moderateScale(24),
    borderRadius: 4,
    borderWidth: 1,
    borderColor: constants.colors.lightBlack,
    width: '70%',
    padding: moderateScale(2),
    flexDirection: 'row',
    alignItems: 'center',
  },

  ccText: {
    fontSize: RFValue(16),
    marginStart: moderateScale(8),
    fontFamily: fonts.bold,
    color: constants.colors.black,
  },

  mobileEditText: {
    fontSize: RFValue(14),
    marginStart: moderateScale(8),
    width: '80%',
    fontFamily: fonts.regular,
    color: constants.colors.black,
  },

  smallMobileImageView: {
    alignItems: 'center',
    justifyContent: 'center',
    marginStart: moderateScale(16),
  },

  mobileImageView: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: moderateScaleVertical(36),
  },
  verificationText: {
    alignItems: 'center',
    justifyContent: 'center',
    height: moderateScaleVertical(40),
    marginTop: moderateScale(22),
  },

  verificationText: {
    alignItems: 'center',
    justifyContent: 'center',
    height: moderateScaleVertical(40),
    marginTop: moderateScale(22),
  },
  otpTxt: {
    fontFamily: fonts.regular,
    fontSize: 14,
  },
  otpInputView: {
    marginTop: moderateScale(58),
    height: 58,
    width: null,
  },
  inputWrapper1: {
    paddingVertical: moderateScale(24),
    paddingHorizontal: 20,
  },
  otpExpire: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: moderateScale(16),
    flexDirection: 'row',
  },
  otpExpiresText: {
    fontFamily: fonts.regular,
    fontSize: moderateScale(14),
    color: constants.colors.lightBlack,
  },
  verifyView: {
    marginTop: moderateScaleVertical(34),
    marginLeft: moderateScale(88),
    marginRight: moderateScale(88),
    height: moderateScale(40),
    backgroundColor: constants.colors.pink,
    width: moderateScale(200),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: moderateScale(4),
  },
  verifyText: {
    fontFamily: fonts.bold,
    fontSize: RFValue(14),
    color: constants.colors.white,
  },
  resendOtpView: {
    height: moderateScaleVertical(24),
    marginTop: moderateScale(12),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: null,
  },
  didnotreceiveText: {
    fontSize: RFValue(13),
    fontFamily: fonts.regular,
    color: constants.colors.black,
  },
  resendText: {
    fontSize: RFValue(13),
    fontFamily: fonts.bold,
    color: constants.colors.pink,
  },
});

export default styles;
