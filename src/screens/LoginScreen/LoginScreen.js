import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Dimensions,
  FlatList,
  SafeAreaView,
  Text,
  StatusBar,
  TextInput,
  Platform,
} from 'react-native';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import styles from './styles';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import LinearGradient from 'react-native-linear-gradient';
import {textScale} from '../../utilities/responsiveSize';
import {ScrollView} from 'react-native-gesture-handler';

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneNumber: '',
    };
  }

  componentDidMount() {
    let {actions, dispatch} = this.props;
    actions.hideLoader(false);
  }

  handleChange = (value, name) => {
    this.setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  getVerificationCode = () => {
    const {phoneNumber} = this.state;
    if (phoneNumber.toString().trim() === '') {
      alert('Please enter mobile number');
    } else if (phoneNumber.toString().trim().length < 10) {
      alert('Please enter valid mobile number');
    } else {
      let {actions, dispatch} = this.props;
      actions.showLoader(true);
      // let data = {};
      // data["_mobile_number"] = phoneNumber;
      console.log('get Verification code reqq : ', phoneNumber);
      Request.get(`buyer_login_otp?_mobile_number=${phoneNumber}`, null)
        .then((res) => {
          console.log('verification code res : ', res);
          actions.hideLoader(false);
          if (res.result.status === 'OK') {
            this.props.navigation.navigate('OtpVerificationScreen', {
              data: res.data,
              phone_number: phoneNumber,
            });
          } else {
            alert('Server Error');
            console.log('api error : ', res);
          }
        })
        .catch((err) => {
          actions.hideLoader(false);
          MyToast.showToast(err.message());
        });
    }
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar barStyle="light-content" hidden={false} />
        <LinearGradient
          colors={[
            // Platform.OS === 'ios' ? '#00000000' : '#FFE03C3C',
            Platform.OS === 'ios' ? '#00000000' : '#FFE03C3C',
            Platform.OS === 'ios' ? '#00000000' : '#E03C3C',
            Platform.OS === 'ios' ? '#00000000' : '#FF9403',
            Platform.OS === 'ios' ? '#00000000' : '#FFE03C3C',
            Platform.OS === 'ios' ? '#00000000' : '#FFE03C3C',


          ]}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0,
            height: '100%',
          }}
        />
        <View style={styles.container}>
          <ScrollView
            style={{flex: 1}}
            contentContainerStyle={{paddingBottom: moderateScale(100)}}
            showsVerticalScrollIndicator={false}>
            <View style={styles.mobileImageView}>
              <Image
                source={constants.images.new_logo}
                style={{width: moderateScale(200), height: moderateScale(124)}}
                resizeMode={'contain'}></Image>
            </View>
            <View
              style={{
                // alignItems: 'center',
                justifyContent: 'center',
                margin: moderateScale(24),
                paddingHorizontal: moderateScale(36),
                paddingVertical: moderateScale(24),
                borderRadius: moderateScale(8),
                backgroundColor: constants.colors.white,
              }}>
              <Text
                style={{
                  ...styles.mobileNuberVerificationText,
                  // fontWeight: 'bold',
                  textAlign: 'left',
                  fontSize: textScale(24),
                }}>
                {'Enter Mobile Number'}
              </Text>
              {/* <Text
                style={{
                  ...styles.enterMobileText,
                  marginTop: moderateScale(36),
                  textAlign: 'left',
                  fontSize: textScale(20),
                  color: constants.colors.blackExtra,
                  // fontWeight: 'bold',
                }}>
                {'Phone Number'}
              </Text> */}

              <View style={styles.mobileEditTextView}>
                <View style={styles.smallMobileImageView}>
                  <Image
                    source={constants.images.flag}
                    style={{
                      height: moderateScale(36),
                      width: moderateScale(36),
                      borderRadius: moderateScale(18),
                    }}></Image>
                </View>
                <Image
                  source={constants.images.dropdown}
                  style={{marginStart: moderateScale(8)}}
                />
                <TextInput
                  style={styles.mobileEditText}
                  placeholder="Enter Number"
                  keyboardType={'numeric'}
                  placeholderTextColor={constants.colors.lightBlack}
                  onChangeText={(text) => {
                    this.handleChange(text, 'phoneNumber');
                  }}
                  maxLength={10}
                />
              </View>
              <View
                style={{
                  width: '100%',
                  backgroundColor: constants.colors.btnGray,
                  height: moderateScale(1),
                  marginTop: moderateScale(4),
                }}
              />
              <Text
                style={{
                  ...styles.enterMobileText,
                  marginTop: moderateScale(48),
                  textAlign: 'left',
                  fontSize: textScale(20),
                  color: constants.colors.blackligt,
                  // fontWeight: 'normal',
                }}>
                {
                  'A 4 Digit Verification Code will be sent via SMS to verify your Mobile number'
                }
              </Text>
            </View>
            <TouchableOpacity onPress={() => this.getVerificationCode()}>
              <View style={styles.verifyView}>
                <LinearGradient
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 3}}
                  colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                  style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    height: 56,
                    borderRadius: moderateScale(24),

                  }}
                />
                <Text style={styles.verifyText}>{'Continue'}</Text>
                <Image
                  source={constants.images.dropdown}
                  style={{
                    marginStart: moderateScale(4),
                    tintColor: constants.colors.white,
                    rotation: -90,
                  }}
                />
              </View>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({language}) => ({
  // language: language.language
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
