
import { RFValue } from 'react-native-responsive-fontsize';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { fonts } from '../../assets';
import { moderateScaleVertical, textScale } from '../../utilities/responsiveSize';
import {Dimensions,StyleSheet} from 'react-native'
import constants from '../../utilities/index';

const { width, height } = Dimensions.get('window');



const styles = StyleSheet.create({
    innercontainer: {
        backgroundColor:constants.colors.white,
        height:moderateScale(400),margin:moderateScale(48),justifyContent:'center',
    },

    container:{flex:1},
    mobileNuberVerificationText: {
        fontSize: RFValue(20),
        fontFamily: fonts.regular,
        color: constants.colors.black,
        marginTop:24
    },

    enterMobileText: {
        fontSize: RFValue(14),
        fontFamily: fonts.regular,
        color: constants.colors.lightBlack,
        textAlign : 'center',
        width:'100%',
        marginTop:8
        
    },

    mobileEditTextView:{
        marginTop:moderateScale(16),
        // borderRadius:4,
        // borderWidth:1,
        // borderColor:constants.colors.lightBlack,
        width:'100%',
        padding:moderateScale(2),
        flexDirection:'row',
        alignItems:'center',
        fontFamily:fonts.light
    
    },

    ccText: {
        fontSize: RFValue(16),
        marginStart:moderateScale(8),
        fontFamily: fonts.bold,
        color: constants.colors.black,
    },

    mobileEditText: {
        fontSize: RFValue(16),
        marginStart:moderateScale(8),
        width:'100%',
        fontFamily: fonts.bold,
        color: constants.colors.black,
    },



    smallMobileImageView: {
        // alignItems: 'center',
        // justifyContent: 'center',
        // marginStart: moderateScale(16)

    },


    mobileImageView: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: moderateScaleVertical(36)
    },
    verificationText: {
        alignItems: 'center',
        justifyContent: 'center',
        height: moderateScaleVertical(40),
        marginTop: moderateScale(22)
    },
    otpTxt: {
        fontFamily: fonts.regular,
        fontSize: (14)
    },
    otpInputView: {
        marginTop: moderateScale(58), height: 58, width: null
    },
    inputWrapper1: {
        paddingVertical: moderateScale(24),
        paddingHorizontal: 20,
    },
    otpExpire: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: moderateScale(16),
        flexDirection: 'row'
    },
    otpExpiresText: {
        fontFamily: fonts.regular,
        fontSize: moderateScale(14),
        color: constants.colors.lightBlack
    },
    verifyView: {
        marginTop: moderateScaleVertical(56),
        marginLeft: moderateScale(88),
        marginRight: moderateScale(88),
        height: moderateScale(50),
        backgroundColor: constants.colors.skyBlue,
        // width: moderateScale(288),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: moderateScale(24),
        flexDirection:'row'
    },
    verifyText: {
        fontFamily: fonts.bold,
        fontSize: RFValue(16),
        color: constants.colors.white,
        fontWeight:'bold'
        
    },
    resendOtpView: {
        height: moderateScaleVertical(24),
        marginTop: moderateScale(12),
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'center',
        width: null
    },
    didnotreceiveText: {
        fontSize: RFValue(13),
        fontFamily: fonts.regular,
        color: constants.colors.black
    },
    resendText: {
        fontSize: RFValue(13),
        fontFamily: fonts.bold,
        color: constants.colors.pink
    }


});

export default styles