import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
  Alert,
  FlatList,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import ScrollableTabView, {
  ScrollableTabBar,
} from 'react-native-scrollable-tab-view';
import {textScale} from '../../utilities/responsiveSize';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import AsyncStorage from '@react-native-community/async-storage';
import {useFocusEffect} from '@react-navigation/native';
import {fonts} from '../../assets'

import moment from 'moment';

const {height, width} = Dimensions.get('window');

class YourOrdersScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ordersData: [],
    };
  }

  getMyOrders = () => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(`Get_YourOrder?BuyerId=${user.Buyer_Id}`, null)
      .then((res) => {
        console.log('your orders res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({ordersData: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  componentDidMount() {
    this.getMyOrders();
  }

  refreshData = ()=>{
    this.componentDidMount()
  }

  renderEmptyListComponent = () => {
    return (
      <View style={{alignItems: 'center', marginTop: moderateScale(16)}}>
        <Text style = {{fontFamily:fonts.bold}}>No Order to Show</Text>
      </View>
    );
  };

  customAlert = (title, message,item) => {
    Alert.alert(
      title,
      message,
      [
        {
          text: 'OK',
          onPress: () => {
            this.cancelReturn(item)
          },
        },
      ],
      {
        cancelable: true,
      },
    );
  };

  cancelReturn = (item) => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    let data = {};
    data.Order_Id = item.Ord_Id;
    Request.post(`Cancel_Order`, data)
      .then((res) => {
        console.log('cancel return res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.getMyOrders();
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  _storeData = async (value) => {
    try {
      await AsyncStorage.setItem('update', value);
      console.log('value storrredd');
    } catch (error) {
      console.log('error stored data', error);
      // Error saving data
    }
  };

  _retrieveData = async () => {
    console.log('reterived method called');
    try {
      const value = await AsyncStorage.getItem('update');
      if (value !== null) {
        // We have data!!
        console.log('value retereived : ', value);
        this.getMyOrders();
        this._storeData('false');
      }
    } catch (error) {
      console.log('error reterieve data data', error);

      // Error retrieving data
    }
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar barStyle="dark-content" hidden={false} />
        <RefreshData onUpdate={this._retrieveData} />

        <HeaderComp
          leftText={'Your Orders'}
          leftTextStyle={{color: constants.colors.white}}
          imageLeft={constants.images.backArrowBlack}
          leftImageTint={constants.colors.white}
          // imageRight={constants.images.cart}
          headerContainerStyle={{backgroundColor: constants.colors.pink}}
          leftImgHandler={() => this.props.navigation.goBack()}
        />
        {/* <View style={{
            width:width,
            height:1,
            backgroundColor:'#DFDFDF',
            shadowColor: '#000000',
            shadowOffset: {
              width: 0,
              height: 3
            },
            shadowRadius: 8,
            shadowOpacity: 0.9}}>
          </View> */}
        {/* <View style={styles.searchEditTextView}>
          <View style={styles.smallMobileImageView}>
            <Image
              source={constants.images.search}
              style={{height: 16, width: 16}}></Image>
          </View>
          <TextInput
            style={styles.mobileEditText}
            placeholder="Search for Orders"
            placeholderTextColor={constants.colors.lightBlack}></TextInput>
        </View> */}
        <FlatList
          data={this.state.ordersData}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('OrderDetailsScreen', {
                    orderId: item.Ord_Id,
                    refreshData: this.refreshData
                  })
                }
                style={{
                  width: width,
                  flexDirection: 'row',
                  padding: moderateScale(16),
                  marginTop: index === 0 ? moderateScale(0) : moderateScale(4),
                  backgroundColor: constants.colors.white,
                }}>
                <Image
                  style={{
                    height: moderateScale(60),
                    width: moderateScale(60),
                  }}
                  source={{
                    uri: item.Product_Image,
                  }}
                  resizeMode="contain"
                />
                <View style={{width: '75%', marginStart: moderateScale(16)}}>
                  <Text
                    style={{
                      width: '95%',
                      fontSize: textScale(12),
                      fontFamily:fonts.regular,
                      color: constants.colors.grayLight,
                    }}>{`Ordered From`}</Text>

                  <Text
                    style={{
                      width: '100%',
                      fontFamily: fonts.bold,
                      fontSize: textScale(16),
                    }}>{`${item.Seller_Firm_Name}`}</Text>
                  <Text
                    style={{
                      // color:
                      //   item.Order_Status === 'Pending'
                      //     ? constants.colors.red
                      //     : constants.colors.green,
                      fontFamily: fonts.bold,
                      marginTop: moderateScale(4),
                      color: item.Order_Status_Color

                    }}>{`${item.Order_Status}`}</Text>
                  <Text
                    style={{
                      fontSize: textScale(16),
                      fontFamily: fonts.bold,
                      marginTop: moderateScale(8),
                    }}>{`${constants.strings.rupee_symbol}${parseFloat(item.Total_amt).toFixed(2)}`}</Text>
                  <Text
                    style={{
                      width: '95%',
                      fontSize: textScale(14),
                      fontFamily:fonts.regular,
                      marginTop: moderateScale(8),
                      color: constants.colors.grayLight,
                    }}>{`Order ID : ${item.Order_ID}`}</Text>
                  <Text
                    style={{
                      width: '95%',
                      fontSize: textScale(14),
                      fontFamily:fonts.regular,
                      color: constants.colors.grayLight,
                    }}>{`Placed On : ${item.Placed_Date}`}</Text>
                    {item.Expected_Date!==""?
                  <Text
                    style={{
                      width: '95%',
                      fontSize: textScale(14),
                      fontFamily:fonts.regular,
                      color: constants.colors.grayLight,
                    }}>{`Expected Date : ${item.Expected_Date}`}</Text>
                    :null}
                  {/* {item.Order_Status === 'Pending' ? (
                    <TouchableOpacity
                      onPress={() => {
                        this.customAlert(
                          'Cancel Return',
                          'Are you sure you want to cancel your return?',
                          item
                        );
                      }}>
                      <Text
                        style={{
                          color:
                            item.Order_Status === 'Pending'
                              ? constants.colors.red
                              : constants.colors.red,
                          fontWeight: 'bold',
                          marginTop: moderateScale(4),
                        }}>{`Cancel Return`}</Text>
                    </TouchableOpacity>
                  ) : null} */}
                </View>
              </TouchableOpacity>
            );
          }}
          ListEmptyComponent={this.renderEmptyListComponent()}
        />

        {/* <ScrollableTabView
          tabBarUnderlineStyle={styles.lineBar}
          tabBarActiveTextColor={constants.colors.pink}
          tabBarInactiveTextColor={constants.colors.black}
          tabBarTextStyle={styles.barText}
          initialPage={0}
          onChangeTab={(i) => {
            null;
          }}
          renderTabBar={() => <ScrollableTabBar />}>

          <View
            tabLabel={'All'}
            style={{backgroundColor: constants.colors.screen_bg}}>
            <View
              style={[
                styles.parentView,
                {backgroundColor: constants.colors.white, height: height},
              ]}>
              <FlatList
                data={this.state.ordersData}
                renderItem={(item) => {
                  return <View></View>;
                }}
                ListEmptyComponent={this.renderEmptyListComponent()}></FlatList>
            </View>
          </View>

          <View
            tabLabel={'Pending'}
            style={{backgroundColor: constants.colors.screen_bg}}>
            <View
              style={[
                styles.parentView,
                {backgroundColor: constants.colors.white, height: height},
              ]}>
              <FlatList
                data={this.state.ordersData}
                renderItem={(item) => {
                  return <View></View>;
                }}
                ListEmptyComponent={this.renderEmptyListComponent()}></FlatList>
            </View>
          </View>


          <View
            tabLabel={'Rescheduled'}
            style={{backgroundColor: constants.colors.screen_bg}}>
            <View
              style={[
                styles.parentView,
                {backgroundColor: constants.colors.white, height: height},
              ]}>
              <FlatList
                data={this.state.ordersData}
                renderItem={(item) => {
                  return <View></View>;
                }}
                ListEmptyComponent={this.renderEmptyListComponent()}></FlatList>
            </View>
          </View>


          <View
            tabLabel={'Packed'}
            style={{backgroundColor: constants.colors.screen_bg}}>
            <View
              style={[
                styles.parentView,
                {backgroundColor: constants.colors.white, height: height},
              ]}>
              <FlatList
                data={this.state.ordersData}
                renderItem={(item) => {
                  return <View></View>;
                }}
                ListEmptyComponent={this.renderEmptyListComponent()}></FlatList>
            </View>
          </View>


          <View
            tabLabel={'Shiped'}
            style={{backgroundColor: constants.colors.screen_bg}}>
            <View
              style={[
                styles.parentView,
                {backgroundColor: constants.colors.white, height: height},
              ]}>
              <FlatList
                data={this.state.ordersData}
                renderItem={(item) => {
                  return <View></View>;
                }}
                ListEmptyComponent={this.renderEmptyListComponent()}></FlatList>
            </View>
          </View>
        </ScrollableTabView> */}
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth, category}) => ({
  user: auth.userData,
});

export default connect(mapStateToProps, mapDispatchToProps)(YourOrdersScreen);

function RefreshData({onUpdate}) {
  useFocusEffect(
    React.useCallback(() => {
      new YourOrdersScreen()._storeData('true');
      console.log('isFocuseddddddddddddddddddddddddd');
      onUpdate();
    }, [onUpdate]),
  );

  return null;
}
