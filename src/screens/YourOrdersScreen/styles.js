
import { RFValue } from 'react-native-responsive-fontsize';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
// import { fonts } from '../../assets';
import { moderateScaleVertical, textScale } from '../../utilities/responsiveSize';
import {Dimensions,StyleSheet} from 'react-native'
import constants from '../../utilities/index';


const { width, height } = Dimensions.get('window');



const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    barText: {
        // fontFamily: fonts.mediumBold,
        fontSize: RFValue(14),
      },
      lineBar: {
        height: 2,
        backgroundColor: constants.colors.pink,
        // width:width/5,
        // marginStart:-50
      },
      scene: {
        flex: 1,
      },

      parentView: {
        padding: 16,
      },
    
    
    menuHeader:{
        height:moderateScale(56),
        backgroundColor:constants.colors.pink
    },

    subMenuHeader : {
        flexDirection:'row',
        marginHorizontal:moderateScale(16),
        marginTop:moderateScale(8),
        alignItems:'center'
    },

    categoriesContainer : {
        width:'100%',
        height:moderateScale(150),
        backgroundColor:constants.colors.red_bg,
        marginTop:moderateScale(4)
    },

    searchEditTextView:{
        borderRadius:4,
        borderWidth:1,
        backgroundColor:constants.colors.white,
        borderColor:constants.colors.lightBlack,
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        height:moderateScale(40)
    
    },

    advImage : {
        width:width,
        height: moderateScale(220)
    },

    gridItemView:{
        alignItems: 'center', 
        justifyContent: 'center',
        borderRadius:moderateScale(4),
        borderWidth:moderateScale(1),
        paddingBottom:moderateScale(4),
        borderColor:constants.colors.separatorColor,
    },

    gridImage : {
        width: width/2 -30,
        height:moderateScale(150),
        overflow:'hidden'
    },

    circleItemView:{
        alignItems: 'center', 
        justifyContent: 'center',
        borderRadius:moderateScale(4),
        borderWidth:moderateScale(1),
        paddingBottom:moderateScale(4),
        borderColor:constants.colors.separatorColor,
    },


    circleImage : {
        width: moderateScale(40),
        height:moderateScale(40),
        borderRadius : moderateScale(20)

    },

    seePriceTextView:{
        borderRadius:4,
        borderWidth:1,
        marginStart:moderateScale(16),
        marginTop:moderateScale(16),
        backgroundColor:constants.colors.pink,
        borderColor:constants.colors.lightBlack,
        flexDirection:'row',
        alignItems:'center',
        paddingVertical: moderateScale(8)
    },

    seePriceText: {
        fontSize: RFValue(12),
        marginStart:moderateScale(8),
        width:'80%',
        fontWeight:'bold',
        // fontFamily: fonts.bold,
        color: constants.colors.white,
    },

    uploadKycText : {
        fontSize: RFValue(16),
        marginStart:moderateScale(8),
        backgroundColor:constants.colors.red,
        alignSelf:'center',
        width: moderateScale(150),
        fontWeight:'bold',
        borderRadius: moderateScale(16),
        padding: moderateScale(8),
        textAlign:'center',
        // fontFamily: fonts.bold,
        color: constants.colors.white,
    },

    uploadKycDescText : {
        fontSize: RFValue(12),
        marginStart:moderateScale(8),
        marginTop:moderateScale(8),
        backgroundColor:constants.colors.red,
        paddingHorizontal:moderateScale(16),
        alignSelf:'center',
        fontWeight:'bold',
        borderRadius: moderateScale(16),
        padding: moderateScale(8),
        textAlign:'center',
        // fontFamily: fonts.bold,
        color: constants.colors.white,

    },

    mobileNuberVerificationText: {
        fontSize: RFValue(20),
        // fontFamily: fonts.bold,
        color: constants.colors.black,
        marginTop:24
    },

    enterMobileText: {
        fontSize: RFValue(14),
        // fontFamily: fonts.bold,
        color: constants.colors.lightBlack,
        textAlign : 'center',
        width:'60%',
        marginTop:8
        
    },

    mobileEditTextView:{
        marginTop:moderateScale(24),
        borderRadius:4,
        borderWidth:1,
        borderColor:constants.colors.lightBlack,
        width:'70%',
        padding:moderateScale(2),
        flexDirection:'row',
        alignItems:'center',
    
    },

    ccText: {
        fontSize: RFValue(16),
        marginStart:moderateScale(8),
        // fontFamily: fonts.bold,
        color: constants.colors.black,
    },

    mobileEditText: {
        fontSize: RFValue(14),
        marginStart:moderateScale(8),
        width:'80%',
        // fontFamily: fonts.bold,
        color: constants.colors.black,
    },



    smallMobileImageView: {
        alignItems: 'center',
        justifyContent: 'center',
        marginStart: moderateScale(16)

    },


    mobileImageView: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: moderateScaleVertical(36)
    },
    verificationText: {
        alignItems: 'center',
        justifyContent: 'center',
        height: moderateScaleVertical(40),
        marginTop: moderateScale(22)
    },

    verificationText: {
        alignItems: 'center',
        justifyContent: 'center',
        height: moderateScaleVertical(40),
        marginTop: moderateScale(22)
    },
    otpTxt: {
        // fontFamily: fonts.regular,
        fontSize: (14)
    },
    otpInputView: {
        marginTop: moderateScale(58), height: 58, width: null
    },
    inputWrapper1: {
        paddingVertical: moderateScale(24),
        paddingHorizontal: 20,
    },
    otpExpire: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: moderateScale(16),
        flexDirection: 'row'
    },
    otpExpiresText: {
        // fontFamily: fonts.regular,
        fontSize: moderateScale(14),
        color: constants.colors.lightBlack
    },
    verifyView: {
        marginTop: moderateScaleVertical(34),
        marginLeft: moderateScale(88),
        marginRight: moderateScale(88),
        height: moderateScale(40),
        backgroundColor: constants.colors.pink,
        width: moderateScale(200),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: moderateScale(4)
    },
    verifyText: {
        // fontFamily: fonts.bold,
        fontSize: RFValue(14),
        color: constants.colors.white
    },
    resendOtpView: {
        height: moderateScaleVertical(24),
        marginTop: moderateScale(12),
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'center',
        width: null
    },
    didnotreceiveText: {
        fontSize: RFValue(13),
        // fontFamily: fonts.regular,
        color: constants.colors.black
    },
    resendText: {
        fontSize: RFValue(13),
        // fontFamily: fonts.bold,
        color: constants.colors.pink
    }


});

export default styles