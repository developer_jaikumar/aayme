import React from 'react';
import {
  StatusBar,
  View,
  SafeAreaView,
  ActivityIndicator,
  StyleSheet,
  Dimensions,
} from 'react-native';
import WebView from 'react-native-webview';
import Config from '../../config/index';
import constants from '../../utilities';
import HeaderComp from '../../components/Header';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import {useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';

class WebViewScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      url: '',
      showLoader: true,
      paymentUrl: undefined,
    };
  }

  componentDidMount() {
    if (this.props.route.params !== undefined) {
      const {type, title} = this.props.route.params;
      if (type === 1) {
        this.setState({title: title});
        this.getWebViewData('Get_Page_Content?pagename=aboutus');
      } else if (type === 2) {
        this.setState({title: title});
        this.getWebViewData('Get_Page_Content?pagename=termofuse');
      } else if (type === 3) {
        this.setState({title: title});
        this.getWebViewData('Get_Page_Content?pagename=policies');
      } else if (type === 4) {
        const {data} = this.props.route.params;
        this.setState({title: title});
        this.getWebViewData(
          `Get_ProductReturnPolicy?Product_Id=${data.Product_Id}`,
        );
      } else if (type === 11) {
        /*Payment Url*/
        const {paymentUrl} = this.props.route.params;
        console.log('payment url :', paymentUrl);
        this.setState({title: title, paymentUrl: paymentUrl, url: undefined});
        this.getPaymentStatusInterval();
      } else {
        this.setState({title: title});
        this.getWebViewData('Get_Contactus_Page');
      }
    }
  }

  componentWillUnmount(){
    clearInterval(this.paymentInterval)
  }

  getWebViewData = (url) => {
    let {actions, dispatch} = this.props;
    // actions.showLoader(true);
    console.log('webview reqq : ', url);
    Request.get(url, null)
      .then((res) => {
        console.log('webview res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({url: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  getPaymentStatusInterval = () => {
    this.paymentInterval = setInterval(this.checkPaymentStatus, 5000);
  };

  checkPaymentStatus = () => {
    let {actions, dispatch, user} = this.props;
    const {type, refreshData} = this.props.route.params;
    if (type === 11) {
      const {uniqueId} = this.props.route.params;
      // actions.showLoader(true);
      Request.get(
        `check_Order_Payment?unquie_id=${uniqueId}&buyer_id=${user.Buyer_Id}`,
        null,
      )
        .then((res) => {
          console.log('checkPaymentStatus res : ', res);
          actions.hideLoader(false);
          if (res.result.status === 'OK') {
            if (res.data.length > 0) {
              clearInterval(this.paymentInterval);
              refreshData(res.data[0])
              this.props.navigation.goBack();
            } 
          } else {
            alert('Server Error');
            console.log('api error : ', res);
            clearInterval(this.paymentInterval);

          }
        })
        .catch((err) => {
          actions.hideLoader(false);
          MyToast.showToast(err.message());
          clearInterval(this.paymentInterval);

        });
    }
  };

  onPressBackHandler = () => {
    this.props.navigation.goBack();
  };

  _storeData = async (value) => {
    try {
      await AsyncStorage.setItem('update', value);
      console.log('value storrredd');
    } catch (error) {
      console.log('error stored data', error);
      // Error saving data
    }
  };

  _retrieveData = async () => {
    console.log('reterived method called');
    const {user} = this.props;
    try {
      const value = await AsyncStorage.getItem('update');
      if (value !== null) {
        // We have data!!
        console.log('value retereived : ', value);
        const {index, routes} = this.props.navigation.dangerouslyGetState();
        const currentRoute = routes[index].name;
        console.log('current screen', currentRoute);
        if (this.props.route.params === undefined) {
          if (currentRoute === 'Enquiry') {
            this.setState({title: 'Enquiry'});
            this.getWebViewData('Get_Inquiry_Page');
          } else {
            this.setState({title: 'Notifications'});
            this.getWebViewData(`Get_Notifications?buyer_id=${user.Buyer_Id}`);
          }
        }
        // this.setState({opacity: 1, searchList: [], searchText: ''});
        this._storeData('false');
      }
    } catch (error) {
      console.log('error reterieve data data', error);

      // Error retrieving data
    }
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <RefreshData onUpdate={this._retrieveData} />
        <HeaderComp
          leftText={this.state.title}
          leftTextStyle={{color: constants.colors.white}}
          imageLeft={
            this.props.route.params !== undefined
              ? constants.images.backArrowBlack
              : null
          }
          leftImageTint={constants.colors.white}
          // headerContainerStyle={{backgroundColor: constants.colors.pink}}
          leftImgHandler={() => this.props.navigation.goBack()}
        />
        <View
          style={{
            width: Dimensions.get('window').width,
            height: 1,
            backgroundColor: '#DFDFDF',
            shadowColor: '#000000',
            shadowOffset: {
              width: 0,
              height: 3,
            },
            shadowRadius: 5,
            shadowOpacity: 0.9,
          }}></View>
        {this.state.showLoader && (
          <ActivityIndicator
            style={styles.container}
            size={'large'}
            color={constants.colors.pink}
          />
        )}

        <WebView
          source={{
            html: this.state.url,
            uri: this.state.paymentUrl,
            // headers: {'Authorization':`Bearer ${userReducer.userData.access_token}`,
            // 'ACCEPT':'application/json'}
          }}
          onLoad={() => this.setState({showLoader: false})}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: constants.colors.white,
    justifyContent: 'center',
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth, category}) => ({
  user: auth.userData,
  categoryClicked: category.setCategoryShown,
});

export default connect(mapStateToProps, mapDispatchToProps)(WebViewScreen);

function RefreshData({onUpdate}) {
  useFocusEffect(
    React.useCallback(() => {
      new WebViewScreen()._storeData('true');
      console.log('isFocuseddddddddddddddddddddddddd');
      onUpdate();
    }, [onUpdate]),
  );
  return null;
}
