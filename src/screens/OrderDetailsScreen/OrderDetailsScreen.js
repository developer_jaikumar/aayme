import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  ScrollView,
  TextInput,
  Linking,
  Alert,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {textScale} from '../../utilities/responsiveSize';
import SwiperFlatList from 'react-native-swiper-flatlist';
import {FlatList} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import {fonts} from '../../assets';
const {height, width} = Dimensions.get('window');

class OrderDetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orderData: {},
    };
  }

  componentDidMount() {
    this.getOrderDetails();
  }

  getOrderDetails = () => {
    let {actions, user} = this.props;
    let {orderId} = this.props.route.params;
    actions.showLoader(true);
    Request.get(`Get_OrderDetail?OrderId=${orderId}`, null)
      .then((res) => {
        console.log('order details res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({orderData: res.data[0]});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  openMailTo = (mail) => {
    if (mail !== '' && mail !== undefined) {
      console.log('email', mail);
      Linking.openURL(
        `mailto:${mail}?subject=${'Return Items'}&body=${'How we can help you'}`,
      );
    } else {
      alert(
        'Currently this service in not working.Please try after some time.',
      );
    }
  };

  customAlert = (title, message) => {
    Alert.alert(
      title,
      message,
      [
        {text: 'CANCEL'},
        {
          text: 'OK',
          onPress: () => {
            let {orderId} = this.props.route.params;
            this.cancelReturn(orderId);
          },
        },
      ],
      {
        cancelable: true,
      },
    );
  };

  cancelReturn = (orderId) => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    let data = {};
    data.Order_Id = orderId;
    Request.post(`Cancel_Order`, data)
      .then((res) => {
        console.log('cancel return res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          let {refreshData} = this.props.route.params;
          refreshData();
          this.props.navigation.goBack();
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  itemSeprator = () => {
    return (
      <View
        style={{
          height: moderateScale(2),
          marginTop: moderateScale(16),
          marginBottom: moderateScale(16),
          width: '100%',
          backgroundColor: constants.colors.separatorColor,
        }}></View>
    );
  };

  itemDarkSeprator = () => {
    return (
      <View
        style={{
          height: moderateScale(8),
          marginTop: moderateScale(16),
          marginBottom: moderateScale(16),
          width: '100%',
          backgroundColor: constants.colors.separatorColor,
        }}></View>
    );
  };

  updateData = () => {
    let {refreshData} = this.props.route.params;
    refreshData();
    this.props.navigation.goBack();
  };

  render() {
    const {orderData} = this.state;
    return (
      <View style={{flex: 1, backgroundColor: constants.colors.white}}>
        <StatusBar barStyle="dark-content" hidden={false} />
        <HeaderComp
          leftText={'Order Details'}
          leftTextStyle={{color: constants.colors.white}}
          imageLeft={constants.images.backArrowBlack}
          leftImageTint={constants.colors.white}
          // imageRight={constants.images.cart}
          headerContainerStyle={{backgroundColor: constants.colors.pink}}
          leftImgHandler={() => this.props.navigation.goBack()}
        />
        <ScrollView>
          <View style={{margin: moderateScale(16)}}>
            <View
              style={{
                height: moderateScale(150),
                padding: moderateScale(16),
                backgroundColor: constants.colors.separatorColor,
              }}>
              <Text
                style={{
                  fontSize: moderateScale(16),
                  fontFamily: fonts.bold,
                  color: orderData.Order_Status_Color,
                }}>
                {orderData.Order_Delivered_Title}
              </Text>
              <Text
                style={{
                  color: constants.colors.black,
                  fontSize: moderateScale(12),
                  fontFamily: fonts.regular,
                }}>
                {orderData.Order_Delivered_Title_Description}
              </Text>
              {orderData.Delivered_on !== '' ? (
                <Text
                  style={{
                    color: constants.colors.black,
                    fontSize: moderateScale(12),
                    position: 'absolute',
                    bottom: 0,
                    padding: moderateScale(16),
                  }}>
                  Delivered On :{' '}
                  <Text
                    style={{
                      color: constants.colors.grayishBlue,
                      fontFamily: fonts.regular,
                    }}>
                    {orderData.Delivered_on}
                  </Text>
                </Text>
              ) : null}
            </View>
            {orderData.Return_Status === true ? (
              <View
                style={{
                  marginTop: moderateScale(16),
                  padding: moderateScale(8),
                  backgroundColor: constants.colors.separatorColor,
                  borderColor: constants.colors.separatorColor,
                  borderWidth: moderateScale(1),
                }}>
                <Text
                  style={{
                    fontSize: moderateScale(14),
                    fontFamily: fonts.bold,
                  }}>
                  Want to return items?
                </Text>
                <Text
                  style={{
                    fontSize: moderateScale(12),
                    fontFamily: fonts.regular,
                  }}>{`You can create a return for the order by ${orderData.Return_Eligible_Date}`}</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('WebViewScreen', {
                      title: 'Return Policy',
                      type: 4,
                      data: {},
                    });
                  }}>
                  <Text
                    style={{
                      color: constants.colors.pink,
                      fontSize: moderateScale(12),
                      fontFamily: fonts.bold,
                    }}>
                    View Return Policy
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    orderData.Return_Redirect_URL !== '' &&
                      Linking.openURL(orderData.Return_Redirect_URL);
                  }}
                  style={{
                    alignItems: 'center',
                    borderColor: constants.colors.pink,
                    borderWidth: moderateScale(1),
                    width: '100%',
                    paddingVertical: moderateScale(8),
                    marginTop: moderateScale(16),
                  }}>
                  <Text
                    style={{
                      color: constants.colors.pink,
                      fontFamily: fonts.bold,
                    }}>
                    Return Items
                  </Text>
                </TouchableOpacity>
              </View>
            ) : null}
            <Text
              style={{
                color: constants.colors.btnGray,
                fontSize: moderateScale(12),
                marginTop: moderateScale(16),
                fontFamily: fonts.bold,
              }}>
              ORDERED ON
            </Text>
            <Text
              style={{
                color: constants.colors.black,
                fontSize: moderateScale(12),
                marginTop: moderateScale(4),
                fontFamily: fonts.bold,
              }}>
              {orderData.Order_On}
            </Text>
            {this.itemSeprator()}
            <Text
              style={{
                color: constants.colors.btnGray,
                fontSize: moderateScale(12),
                fontFamily: fonts.bold,
              }}>
              TOTAL ORDERED AMOUNT
            </Text>
            <Text
              style={{
                color: constants.colors.black,
                fontSize: moderateScale(12),
                marginTop: moderateScale(4),
                fontFamily: fonts.bold,
              }}>
              {`${constants.strings.rupee_symbol}${parseFloat(
                orderData.Total_Order_Amount,
              ).toFixed(2)}`}
            </Text>
            {this.itemSeprator()}
            <Text
              style={{
                color: constants.colors.btnGray,
                fontSize: moderateScale(12),
                fontFamily: fonts.bold,
              }}>
              ORDER ID
            </Text>
            <Text
              style={{
                color: constants.colors.black,
                fontSize: moderateScale(12),
                marginTop: moderateScale(4),
                fontFamily: fonts.bold,
              }}>
              {orderData.Order_ID}
            </Text>
            {this.itemSeprator()}
            {orderData.Invoice_Box === true ? (
              <>
                <Text
                  style={{
                    color: constants.colors.btnGray,
                    fontSize: moderateScale(12),
                    fontFamily: fonts.bold,
                  }}>
                  INVOICE AMOUNT
                </Text>
                <Text
                  style={{
                    color: constants.colors.black,
                    fontSize: moderateScale(12),
                    marginTop: moderateScale(4),
                    fontFamily: fonts.bold,
                  }}>
                  {`${constants.strings.rupee_symbol}${parseFloat(
                    orderData.Invoice_Amount,
                  ).toFixed(2)}`}
                </Text>
                {this.itemSeprator()}
                <Text
                  style={{
                    color: constants.colors.btnGray,
                    fontSize: moderateScale(12),
                    fontFamily: fonts.bold,
                  }}>
                  INVOICE ID
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text
                    style={{
                      color: constants.colors.black,
                      fontSize: moderateScale(12),
                      marginTop: moderateScale(4),
                      fontFamily: fonts.bold,
                    }}>
                    {orderData.Invoice_ID}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      if (
                        orderData.Invoice_URL !== '' &&
                        orderData.Invoice_URL !== undefined
                      ) {
                        orderData.Invoice_URL &&
                          Linking.openURL(orderData.Invoice_URL);
                      } else {
                        alert(
                          'Currently this service in not working.Please try after some time.',
                        );
                      }
                    }}>
                    <Text
                      style={{
                        color: constants.colors.pink,
                        fontSize: moderateScale(12),
                        marginTop: moderateScale(4),
                        fontFamily: fonts.bold,
                      }}>
                      {'Download Invoice'}
                    </Text>
                  </TouchableOpacity>
                </View>
                {this.itemSeprator()}
              </>
            ) : null}
            <Text
              style={{
                color: constants.colors.btnGray,
                fontSize: moderateScale(12),
                fontFamily: fonts.bold,
              }}>
              DELIVERY ADDRESS
            </Text>
            <Text
              style={{
                color: constants.colors.black,
                fontSize: moderateScale(12),
                marginTop: moderateScale(4),
                fontFamily: fonts.bold,
              }}>
              {orderData.Delivery_Address}
            </Text>
            {this.itemDarkSeprator()}
            <Text
              style={{
                color: constants.colors.btnGray,
                fontSize: moderateScale(12),
                fontFamily: fonts.bold,
              }}>
              ORDERED ITEMS
            </Text>
            <FlatList
              data={orderData.Product_Detail}
              renderItem={({item, index}) => {
                return (
                  <View
                    style={{
                      width: width,
                      flexDirection: 'row',
                      paddingVertical: moderateScale(16),
                    }}>
                    <Image
                      style={{
                        height: moderateScale(60),
                        width: moderateScale(60),
                      }}
                      source={{
                        uri: item.Product_Image,
                      }}
                      resizeMode="contain"
                    />
                    <View
                      style={{width: '75%', marginStart: moderateScale(16)}}>
                      <Text
                        style={{
                          width: '100%',
                          fontFamily: fonts.bold,
                          fontSize: textScale(16),
                        }}>{`${item.Product_Name}`}</Text>
                      <Text
                        style={{
                          width: '95%',
                          fontFamily: fonts.regular,
                        }}>{`${item.Product_Description}`}</Text>
                      <Text
                        style={{
                          fontFamily: fonts.regular,
                        }}>{`Quantity : ${item.Order_Item_Qty}`}</Text>
                      <Text
                        style={{
                          fontSize: textScale(16),
                          fontFamily: fonts.bold,
                          marginTop: moderateScale(8),
                        }}>{`${constants.strings.rupee_symbol}${parseFloat(
                        item.Order_Item_Price,
                      ).toFixed(2)} per pcs`}</Text>
                    </View>
                  </View>
                );
              }}
              ItemSeparatorComponent={this.itemSeprator}
              ListFooterComponent={this.renderFooterComponent}
            />
            {this.itemDarkSeprator()}
            {orderData.Tracking_Box === true ? (
              <>
                <Text
                  style={{
                    color: constants.colors.btnGray,
                    fontSize: moderateScale(12),
                    fontFamily: fonts.bold,
                  }}>
                  TRACK ORDER
                </Text>
                <TouchableOpacity
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}
                  onPress={() =>
                    orderData.tracking_url &&
                    Linking.openURL(orderData.tracking_url)
                  }>
                  <Text
                    style={{
                      color: constants.colors.black,
                      fontSize: moderateScale(12),
                      marginTop: moderateScale(4),
                      fontFamily: fonts.bold,
                    }}>
                    {`Tracking ID : ${orderData.tracking_id}`}
                  </Text>
                  <Image
                    style={{
                      width: moderateScale(24),
                      height: moderateScale(24),
                      tintColor: constants.colors.black,
                      rotation: 180,
                    }}
                    source={constants.images.backArrowBlack}
                  />
                </TouchableOpacity>

                {this.itemDarkSeprator()}
              </>
            ) : null}
            <Text
              style={{
                color: constants.colors.btnGray,
                fontSize: moderateScale(12),
                fontFamily: fonts.bold,
              }}>
              SUPPORT
            </Text>
            <TouchableOpacity
              style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
              onPress={() =>
                this.props.navigation.navigate('WebViewScreen', {
                  title: 'Support',
                })
              }>
              <Text
                style={{
                  color: constants.colors.black,
                  fontSize: moderateScale(12),
                  marginTop: moderateScale(4),
                  fontFamily: fonts.bold,
                }}>
                {`AayMe Customer Support`}
              </Text>
              <Image
                style={{
                  width: moderateScale(24),
                  height: moderateScale(24),
                  tintColor: constants.colors.black,
                  rotation: 180,
                }}
                source={constants.images.backArrowBlack}
              />
            </TouchableOpacity>

            {this.itemDarkSeprator()}
            <Text
              style={{
                color: constants.colors.btnGray,
                fontSize: moderateScale(12),
                fontFamily: fonts.bold,
              }}>
              COST SUMMARY
            </Text>

            <View
              style={{
                marginTop: moderateScale(4),
                borderWidth: 2,
                borderColor: constants.colors.separatorColor,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: moderateScale(8),
                  paddingTop: moderateScale(8),
                }}>
                <Text style={{textAlign: 'left', fontFamily: fonts.regular}}>
                  Total Goods Amount
                </Text>
                <Text style={{fontFamily: fonts.regular}}>{`${
                  constants.strings.rupee_symbol
                }${parseFloat(orderData.Cost_Goods_Amount).toFixed(2)}`}</Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: moderateScale(8),
                  paddingTop: moderateScale(4),
                }}>
                <Text style={{textAlign: 'left', fontFamily: fonts.regular}}>
                  GST
                </Text>
                <Text style={{fontFamily: fonts.regular}}>{`${
                  constants.strings.rupee_symbol
                }${parseFloat(orderData.Cost_GST_Amount).toFixed(2)}`}</Text>
              </View>
              {/* <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: moderateScale(8),
                  paddingTop: moderateScale(4),
                }}>
                <Text style={{textAlign: 'left', fontFamily: fonts.regular}}>
                  Delivery Charges
                </Text>
                <Text style={{fontFamily: fonts.regular}}>{`${
                  constants.strings.rupee_symbol
                }${parseFloat(orderData.Cost_Delivery_Amount).toFixed(
                  2,
                )}`}</Text>
              </View> */}
              {this.itemSeprator()}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: moderateScale(8),
                  paddingBottom: moderateScale(8),
                }}>
                <Text style={{textAlign: 'left', fontFamily: fonts.bold}}>
                  Total
                </Text>
                <Text style={{fontFamily: fonts.bold}}>{`${
                  constants.strings.rupee_symbol
                }${parseFloat(orderData.Cost_Total_Amount).toFixed(2)}`}</Text>
              </View>
            </View>
            {orderData.Cancel_Button === true ? (
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('CancelOrderScreen', {
                    orderId: this.props.route.params.orderId,
                    updateData: this.updateData,
                  });
                }}
                style={{
                  alignItems: 'center',
                  borderColor: constants.colors.pink,
                  borderWidth: moderateScale(1),
                  width: '100%',
                  paddingVertical: moderateScale(8),
                  marginTop: moderateScale(16),
                }}>
                <Text
                  style={{
                    color: constants.colors.pink,
                    fontFamily: fonts.regular,
                  }}>
                  Cancel Order
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth, category}) => ({
  user: auth.userData,
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetailsScreen);
