import React, {Component, useState} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import ImageView from 'react-native-image-viewing';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';

const images = [
  {
    uri: 'https://images.unsplash.com/photo-1571501679680-de32f1e7aad4',
  },
  {
    uri: 'https://images.unsplash.com/photo-1573273787173-0eb81a833b34',
  },
  {
    uri: 'https://images.unsplash.com/photo-1569569970363-df7b6160d111',
  },
];

class ZoomImagesScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageList: [],
      visible:true
    };
  }

  componentDidMount() {
    const {data} = this.props.route.params;
    const{imageList} = this.state
    data.map((item,index) =>{
        imageList.push({uri:item.ImagePath})
    })
    this.setState({imageList});
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageView
          images={this.state.imageList}
          imageIndex={0}
          visible={this.state.visible}
          swipeToCloseEnabled = {false}
          onRequestClose={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth, category}) => ({
  user: auth.userData,
});

export default connect(mapStateToProps, mapDispatchToProps)(ZoomImagesScreen);
