
import { RFValue } from 'react-native-responsive-fontsize';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { fonts } from '../../assets';
import { moderateScaleVertical, textScale } from '../../utilities/responsiveSize';
import {Dimensions,StyleSheet} from 'react-native'
import constants from '../../utilities/index';

const { width, height } = Dimensions.get('window');



const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    mobileNuberVerificationText: {
        fontSize: RFValue(20),
        fontFamily: fonts.bold,
        color: constants.colors.blackExtra
    },
    mobileImageView: {
        alignItems: 'center',
        justifyContent: 'center',
        // marginTop: moderateScaleVertical(27)
    },
    verificationText: {
        alignItems: 'center',
        justifyContent: 'center',
        height: moderateScaleVertical(40),
        marginTop: moderateScale(22)
    },
    otpTxt: {
        fontFamily: fonts.regular,
        fontSize: (14)
    },
    otpInputView: {
        marginTop: moderateScale(58), height: 58, width: null
    },
    inputWrapper1: {
        paddingVertical: moderateScale(24),
        // alignItems:'flex-start',
        // justifyContent:'flex-start',
        // alignSelf:'flex-start'
        // paddingHorizontal: 20,
        // width:width-100

    },
    otpExpire: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: moderateScale(16),
        flexDirection: 'row'
    },
    otpExpiresText: {
        fontFamily: fonts.regular,
        fontSize: moderateScale(14),
        color: constants.colors.lightBlack
    },
    verifyView: {
        marginTop: moderateScaleVertical(56),
        marginLeft: moderateScale(88),
        marginRight: moderateScale(88),
        height: moderateScale(50),
        backgroundColor: constants.colors.skyBlue,
        borderRadius: moderateScale(24),

        // width: moderateScale(200),
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection:'row'

    },
    verifyText: {
        fontFamily: fonts.bold,
        fontSize: RFValue(14),
        color: constants.colors.white
    },
    resendOtpView: {
        height: moderateScaleVertical(24),
        marginTop: moderateScale(12),
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'center',
        width: null
    },
    didnotreceiveText: {
        fontSize: RFValue(13),
        fontFamily: fonts.mediumBold,
        color: constants.colors.skyBlue,
    },
    resendText: {
        fontSize: RFValue(14),
        fontFamily: fonts.mediumBold,
        color: constants.colors.pink,
        width:'100%'
    },
    enterMobileText: {
        fontSize: RFValue(14),
        fontFamily: fonts.regular,
        color: constants.colors.lightBlack,
        textAlign : 'center',
        width:'100%',
        marginTop:8
        
    },



});

export default styles