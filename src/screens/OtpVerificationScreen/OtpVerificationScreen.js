import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  SafeAreaView,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  Keyboard,
  TouchableOpacity,
  StatusBar,
  FlatList,
  ImageBackground,
} from 'react-native';
const {width, height} = Dimensions.get('window');
import {RFValue} from 'react-native-responsive-fontsize';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {moderateScaleVertical, textScale} from '../../utilities/responsiveSize';
import CodeInput from 'react-native-confirmation-code-input';
import CountDown from 'react-native-countdown-component';
import Toast from 'react-native-root-toast';
import styles from './styles';
import HeaderComp from '../../components/Header';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import LinearGradient from 'react-native-linear-gradient';
import {fonts} from '../../assets';

class OtpVerificationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  otpInputHandler = async (code) => {
    const {actions} = this.props;
    const {data, phone_number} = this.props.route.params;
    if (data.otp === undefined || data.otp.length < 4) {
      alert('Invalid Otp');
    } else if (code === data.otp) {
      await actions.saveUserData(data);
      if (data.first_time_login === false) {
        this.props.navigation.replace('HomeTabFlow');
      } else {
        this.props.navigation.replace('SignupScreen', {
          phone_number: phone_number,
        });
      }
    } else {
      alert('Incorrect otp');
    }
  };
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar barStyle="light-content" hidden={false} />
        <LinearGradient
          colors={[
            Platform.OS === 'ios' ? '#00000000' : '#FFE03C3C',
            Platform.OS === 'ios' ? '#00000000' : '#FFE03C3C',
            Platform.OS === 'ios' ? '#00000000' : '#E03C3C',
            Platform.OS === 'ios' ? '#00000000' : '#FF9403',
            Platform.OS === 'ios' ? '#00000000' : '#FFE03C3C',
            Platform.OS === 'ios' ? '#00000000' : '#FFE03C3C',
          ]}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0,
            height: '100%',
          }}
        />

        <View style={styles.container}>
          {/* <Toast ref="toast"/> */}

          <HeaderComp
            leftImgHandler={() => this.props.navigation.goBack()}
            imageLeft={constants.images.backArrowBlack}
          />
          <ScrollView
            style={{flex: 1}}
            contentContainerStyle={{paddingBottom: moderateScale(100)}}
            showsVerticalScrollIndicator={false}>
            <View style={styles.mobileImageView}>
              <Image
                source={constants.images.new_logo}
                style={{width: moderateScale(200), height: moderateScale(124)}}
                resizeMode={'contain'}></Image>
            </View>

            <View
              style={{
                // alignItems: 'center',
                justifyContent: 'center',
                margin: moderateScale(24),
                paddingHorizontal: moderateScale(36),
                paddingVertical: moderateScale(24),
                borderRadius: moderateScale(8),
                backgroundColor: constants.colors.white,
              }}>
              <Text
                style={{
                  ...styles.mobileNuberVerificationText,
                  textAlign: 'left',
                  fontSize: textScale(24),
                }}>
                {'OTP Verification'}
              </Text>
              <Text
                style={{
                  ...styles.enterMobileText,
                  marginTop: moderateScale(36),
                  textAlign: 'left',
                  fontSize: textScale(20),
                  color: constants.colors.blackExtra,
                }}>
                {'Enter the OTP you received to'}
              </Text>
              <Text
                style={{
                  ...styles.resendText,
                  textAlign: 'left',
                  fontSize: textScale(20),
                  color: constants.colors.blackExtra,
                }}>{`+91${this.props.route.params.phone_number} `}</Text>

              <View style={styles.inputWrapper1}>
                <CodeInput
                  ref="codeInputRef1"
                  secureTextEntry={false}
                  className={'border-box'}
                  containerStyle={{borderRadius: moderateScale(8)}}
                  activeColor={constants.colors.pink}
                  inactiveColor={constants.colors.pink}
                  space={20}
                  codeLength={4}
                  size={50}
                  autoFocus={true}
                  keyboardType={'numeric'}
                  codeInputStyle={{
                    fontSize: RFValue(16),
                    borderRadius: moderateScale(4),
                    backgroundColor: '#FFFF9403',
                    height: 60,
                    justifyContent: 'center',
                    color: constants.colors.black,
                    fontFamily: fonts.regular,
                  }}
                  inputPosition="center"
                  onFulfill={(code) => this.otpInputHandler(code)}
                  onCodeChange={(code) => {
                    this.state.code = code;
                  }}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: moderateScale(56),
                }}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}>
                  <Text
                    style={{
                      ...styles.didnotreceiveText,
                      textAlign: 'left',
                      fontSize: textScale(20),
                      color: constants.colors.pink,
                      fontWeight: 'normal',
                    }}>
                    {'Resend OTP'}
                  </Text>
                </TouchableOpacity>
                <Image
                  source={constants.images.dropdown}
                  style={{
                    marginStart: moderateScale(4),
                    tintColor: constants.colors.pink,
                    rotation: 270,
                  }}
                />
              </View>
            </View>

            {/* <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: moderateScale(16),
              }}>
              <Text style={styles.mobileNuberVerificationText}>
                {constants.strings.mobileNumberVerificationText}
              </Text>
            </View>
            <View style={styles.mobileImageView}>
              <Image
                source={constants.images.smallPhone1}
                style={{tintColor: constants.colors.pink}}></Image>
            </View> */}

            {/* <View style={styles.verificationText}>
              <Text style={styles.otpTxt}>{constants.strings.otpText}</Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.otpTxt}>{constants.strings.number}</Text>
                <Text style={styles.otpTxt}>{`+91 8950618636`}</Text>
                <TouchableOpacity onPress={this.onClickEditMobileNumber}>
                  <Image source={constants.images.icEdit}></Image>
                </TouchableOpacity>
              </View>
            </View> */}
            {/* <View style={styles.otpExpire}>
              <Text style={styles.otpExpiresText}>
                {constants.strings.optExpirestext}
              </Text>
              <CountDown
                until={60 * 2}
                size={11}
                onFinish={(finish) => console.log(finish)}
                digitStyle={{backgroundColor: '#FFF'}}
                digitTxtStyle={{color: constants.colors.pink, fontSize: 14}}
                timeToShow={['M', 'S']}
              />
            </View> */}
            <TouchableOpacity onPress={() => this.otpInputHandler()}>
              <View style={styles.verifyView}>
                <LinearGradient
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 3}}
                  colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                  style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    height: 56,
                    borderRadius: moderateScale(24),
                  }}
                />
                <Text style={styles.verifyText}>{'Continue'}</Text>
                <Image
                  source={constants.images.dropdown}
                  style={{
                    marginStart: moderateScale(4),
                    tintColor: constants.colors.white,
                    rotation: -90,
                  }}
                />
              </View>
            </TouchableOpacity>
            {/* <View style={styles.resendOtpView}>
              <Text style={styles.didnotreceiveText}>
                {constants.strings.didnotReceiveText}
              </Text>
              <TouchableOpacity onPress={this.resendOtpHandler}>
                <Text style={styles.resendText}>
                  {constants.strings.resendCode}
                </Text>
              </TouchableOpacity>
            </View> */}
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth}) => ({
  user: auth.userData,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OtpVerificationScreen);
