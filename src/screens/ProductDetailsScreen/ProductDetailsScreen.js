import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  ScrollView,
  TextInput,
  Alert,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {textScale} from '../../utilities/responsiveSize';
import SwiperFlatList from 'react-native-swiper-flatlist';
import {FlatList} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import Share from 'react-native-share';
import {fonts} from '../../assets';
import LinearGradient from 'react-native-linear-gradient';

const {height, width} = Dimensions.get('window');

class ProductDetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prodData: {Selling_Price: 0, Before_GST_Price: 0, gst: 0, MinQty: 0},
      productImages: [],
      specificationData: [],
      multiProducts: [],
      tagsData: [],
      returnPolicyData: [],
    };
  }

  componentDidMount() {
    this.getProductDetails();
  }

  itemSeprator = () => {
    return (
      <View
        style={{
          height: moderateScale(1),
          marginTop: moderateScale(8),
          width: '100%',
          backgroundColor: constants.colors.separatorColor,
        }}></View>
    );
  };

  itemSeprator_2 = () => {
    return (
      <View
        style={{
          height: moderateScale(1),
          marginTop: moderateScale(-8),
          width: '100%',
          backgroundColor: constants.colors.separatorColor,
        }}></View>
    );
  };

  emptyView = () => {
    return (
      <View
        style={{
          paddingVertical: moderateScale(4),
          width: width,
          backgroundColor: constants.colors.separatorColor,
          marginVertical: moderateScale(16),
        }}
      />
    );
  };

  getProductDetails = () => {
    let {actions, user} = this.props;
    let {id} = this.props.route.params;
    actions.showLoader(true);
    console.log('productId: ', id);
    Request.get(`Get_Product_Detail?id=${id}&Buyer_id=${user.Buyer_Id}`, null)
      .then((res) => {
        console.log('prod details res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.state.returnPolicyData.push({
            name: 'Qty',
            value: 'Price',
            gst: '',
          });

          for (var i = 0; i < res.data.Product_Qty_Price.length; i++) {
            this.state.returnPolicyData.push({
              name: `${res.data.Product_Qty_Price[i].From_Qty} - ${res.data.Product_Qty_Price[i].To_Qty} pcs`,
              value: `${constants.strings.rupee_symbol}${parseFloat(
                res.data.Product_Qty_Price[i].price_with_gst,
              ).toFixed(2)}`,
              gst: `${res.data.Product_Qty_Price[i].price_with_gst_persantage}`,
            });
          }

          this.setState({
            prodData: res.data,
            productImages: res.data.Product_Images,
            specificationData: res.data.Product_Category_Specfication,
            tagsData: res.data.tags,
            multiProducts: res.data.Multi_Products,
          });
        } else {
          alert('Server Error');
          // alert(res.result.status)
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message);
      });
  };

  addDataInCart = () => {
    let {actions, user} = this.props;
    let {prodData} = this.state;
    let data = {};
    data['Buyer_Id'] = user.Buyer_Id;
    data['Product_Id'] = prodData.Product_Id;
    data['Qty'] = prodData.MinQty;
    data['Seller_Id'] = prodData.Seller_Id;
    actions.showLoader(true);
    console.log('add Data in Cart : ', data);
    Request.post(`Save_AddToCart`, data)
      .then((res) => {
        console.log('add Item in cart res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.props.navigation.navigate('YourCartScreen');
        } else if (res.result.status === 'DIFFERENTSELLER') {
          this.customAlert('Alert', res.data);
        } else if (res.result.status === 'DUPLICATE') {
          this.customAlert('Alert', res.data);
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message);
      });
  };

  customAlert = (title, message) => {
    Alert.alert(
      title,
      message,
      [
        {
          text: 'OK',
          onPress: () => {
            this.props.navigation.navigate('YourCartScreen');
          },
        },
      ],
      {
        cancelable: true,
      },
    );
  };

  onShareHandler = () => {
    const shareOptions = {
      title: 'Share via',
      message: `Click Now`,
      url: `https://www.aayme.com/ProductDetail?id=${this.props.route.params.id}`,
      // social: Share.Social.WHATSAPP,
      whatsAppNumber: '9199999999', // country code + phone number(currently only works on Android)
      filename: 'test', // only for base64 file in Android
    };

    Share.open(shareOptions)
      .then((res) => {
        console.log('share response : ', res);
      })
      .catch((err) => {
        err && console.log(err);
      });
  };

  render() {
    let {prodData} = this.state;
    const {user} = this.props;
    return (
      <View style={{flex: 1, backgroundColor: constants.colors.white}}>
        <StatusBar barStyle="dark-content" hidden={false} />
        <View style={styles.container}>
          {/* <Toast ref="toast"/> */}
          <View style={styles.menuHeader}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 6.5}}
              colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                height: 56,
              }}

              // useAngle = {true}
              // angle = {75}
              // angleCenter = {{x:0.5,y:0.5}}
            />
            <View style={styles.subMenuHeader}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image
                  source={constants.images.backArrowBlack}
                  style={{
                    height: moderateScale(24),
                    width: moderateScale(24),
                    tintColor: constants.colors.white,
                  }}></Image>
              </TouchableOpacity>

              <View
                style={{
                  flexDirection: 'row',
                  width: '80%',
                  marginHorizontal: moderateScale(16),
                  alignItems: 'center',
                }}>
                {/* <Image
                  style={{
                    height: moderateScale(30),
                    width: moderateScale(30),
                    tintColor: constants.colors.white,
                  }}
                  source={constants.images.policy}
                /> */}
                <View
                  style={{
                    marginVertical: moderateScale(16),
                    // marginHorizontal: moderateScale(8),
                    justifyContent: 'center',
                    width: '90%',
                    // alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      color: constants.colors.white,
                      fontSize: textScale(16),
                      fontFamily: fonts.bold,
                    }}>
                    {prodData && prodData.Seller_Name !== undefined
                      ? prodData.Seller_Name.length > 28
                        ? `${prodData.Seller_Name.substring(0, 28)}...`
                        : prodData.Seller_Name
                      : ''}
                  </Text>
                  <Text
                    style={{
                      color: constants.colors.white,
                      fontSize: textScale(12),
                      fontFamily: fonts.regular,
                    }}>
                    {prodData && prodData.Seller_Location}
                  </Text>
                </View>
              </View>
              <TouchableOpacity
                style={{
                  height: moderateScale(40),
                  width: moderateScale(24),
                  position: 'absolute',
                  right: 0,
                  marginEnd: moderateScale(40),
                }}
                onPress={() => {
                  this.onShareHandler();
                }}>
                <Image
                  source={constants.images.share}
                  style={{
                    height: moderateScale(28),
                    width: moderateScale(28),
                    position: 'absolute',
                    right: 0,

                    // tintColor: constants.colors.white,
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: moderateScale(34),
                  width: moderateScale(24),
                  position: 'absolute',
                  right: 0,
                }}
                onPress={() => {
                  this.props.navigation.navigate('YourCartScreen');
                }}>
                <Image
                  source={constants.images.cart}
                  style={{
                    marginStart: moderateScale(16),
                    height: moderateScale(24),
                    width: moderateScale(24),
                    position: 'absolute',
                    right: 0,
                    tintColor: constants.colors.white,
                  }}></Image>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{flex: user.KYC === true ? 0.9 : 1}}>
            <ScrollView style={{flex: 1, opacity: this.state.opacity}}>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View style={{height: 320, width: '100%'}}>
                  {this.state.productImages &&
                  this.state.productImages.length > 0 ? (
                    <SwiperFlatList
                      // autoplay
                      // autoplayDelay={2}
                      // autoplayLoop
                      data={this.state.productImages}
                      paginationDefaultColor={'black'}
                      paginationActiveColor={constants.colors.pink}
                      paginationStyleItem={{
                        width: moderateScale(6),
                        height: moderateScale(6),
                        borderRadius: 5,
                        borderColor: 'white',
                        borderWidth: 1,
                        marginTop: moderateScale(8),
                      }}
                      renderItem={({item}) => (
                        <TouchableOpacity
                          onPress={() => {
                            this.props.navigation.navigate('ZoomImagesScreen', {
                              data: this.state.productImages,
                            });
                          }}
                          style={
                            {
                              // alignItems: 'center',
                              // justifyContent: 'center',
                              // marginTop: moderateScale(16),
                            }
                          }>
                          <ImageBackground
                            style={styles.advImage}
                            source={{uri: item.ImagePath}}
                            resizeMode={'contain'}
                          />
                        </TouchableOpacity>
                      )}
                      showPagination
                    />
                  ) : null}
                </View>
              </View>
              {/* <View
                style={{
                  flexDirection: 'row',
                  width: '60%',
                  marginHorizontal: moderateScale(16),
                  alignItems: 'center',
                }}>
                <Image
                  style={{
                    height: moderateScale(30),
                    width: moderateScale(30),
                    tintColor: constants.colors.pink,
                  }}
                  source={constants.images.support}
                />

                <View
                  style={{
                    marginVertical: moderateScale(16),
                    marginHorizontal: moderateScale(8),
                    justifyContent: 'center',
                    // alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      color: constants.colors.pink,
                      fontSize: textScale(14),
                      fontStyle: 'normal',
                      fontWeight: 'bold',
                    }}>
                    Best Seller
                  </Text>
                  <Text
                    style={{
                      color: constants.colors.pink,
                      fontSize: textScale(10),
                      fontStyle: 'normal',
                    }}>
                    Top 5 products in LED Bulbs
                  </Text>
                </View>
              </View> */}
              <Text
                style={{
                  color: constants.colors.black,
                  fontSize: textScale(18),
                  fontFamily: fonts.bold,
                  marginHorizontal: moderateScale(16),
                  marginTop: moderateScale(16),
                }}>
                {prodData && prodData.Product_Name}
              </Text>
              {user.KYC === false ? (
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('KYCStepOneScreen');
                  }}
                  style={{
                    flexDirection: 'row',
                    backgroundColor: constants.colors.pink,
                    padding: moderateScale(16),
                    margin: moderateScale(16),
                    borderRadius: moderateScale(8),
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{
                      height: moderateScale(16),
                      width: moderateScale(16),
                      tintColor: constants.colors.white,
                    }}
                    source={constants.images.category}
                  />
                  <Text
                    style={{
                      color: constants.colors.white,
                      fontSize: textScale(16),
                      marginStart: moderateScale(16),
                      fontFamily: fonts.bold,
                    }}>
                    Complete Shop KYC to view price
                  </Text>
                </TouchableOpacity>
              ) : null}
              {prodData && prodData.OutofStock === true ? (
                <Text
                  style={{
                    color: constants.colors.red,
                    fontSize: textScale(20),
                    fontFamily: fonts.bold,
                    textAlign: 'center',
                    marginEnd: moderateScale(16),
                    marginTop: moderateScale(4),
                    right: 0,
                  }}>
                  {prodData.OutofStockMessage}
                </Text>
              ) : null}

              {user.KYC === true ? (
                <View
                  style={{
                    marginTop: moderateScale(16),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginHorizontal: moderateScale(16),
                  }}>
                  <View>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Text
                        style={{
                          color: constants.colors.red,
                          fontSize: textScale(18),
                          fontFamily: fonts.bold,
                        }}>
                        {`${constants.strings.rupee_symbol}${parseFloat(
                          prodData && prodData.Selling_Price,
                        ).toFixed(2)}`}
                      </Text>
                      <Text
                        style={{
                          color: constants.colors.black,
                          fontSize: textScale(14),
                          marginStart: moderateScale(8),
                          fontFamily: fonts.regular,
                        }}>
                        Per pc
                      </Text>
                    </View>
                    <Text
                      style={{
                        color: constants.colors.black,
                        fontSize: textScale(12),
                        fontFamily: fonts.regular,
                      }}>
                      {`(${constants.strings.rupee_symbol}${parseFloat(
                        prodData && prodData.Before_GST_Price,
                      ).toFixed(2)} + ${prodData && prodData.gst}% GST)`}
                    </Text>
                    {/* <Text
                      style={{
                        color: constants.colors.black,
                        fontSize: textScale(12),
                        fontWeight: 'normal',
                      }}>
                      {`Min Qty : ${prodData && prodData.MinQty} pcs`} */}

                    {/* {`${
                        prodData.delivery_amount === 0 ? 'Free Delivery' : ''
                      } ${
                        prodData.delivery_location !== ''
                          ? `at ${prodData.delivery_location}`
                          : ''
                      } ${
                        prodData.delivery_date !== ''
                          ? `on ${prodData.delivery_date}`
                          : ''
                      }`} */}
                    {/* </Text> */}
                  </View>
                  <View>
                    <Text
                      style={{
                        color: constants.colors.black,
                        fontSize: textScale(12),
                        fontFamily: fonts.bold,
                      }}>
                      {`Min Qty : ${prodData && prodData.MinQty} pcs`}
                    </Text>
                    {/* <Text
                      style={{
                        color: constants.colors.black,
                        fontSize: textScale(12),
                        fontStyle: 'normal',
                        textAlign: 'right',
                        fontWeight: 'bold',
                      }}>
                      {`Max Qty : ${prodData && prodData.MaxQty} pcs`}
                    </Text> */}
                  </View>
                </View>
              ) : null}
              {this.itemSeprator_2()}
              {/* {user.KYC === true ? (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginHorizontal: moderateScale(16),
                    marginStart: moderateScale(0),
                    marginTop: moderateScale(16),
                  }}>
                  <View>
                    <Text
                      style={{
                        color: constants.colors.black,
                        fontSize: textScale(14),
                        marginStart: moderateScale(16),
                        fontStyle: 'normal',
                      }}>
                      {`Min Order Price : ${constants.strings.rupee_symbol} ${
                        prodData && prodData.Min_order_price
                      }`}
                    </Text>
                    <Text
                      style={{
                        color: constants.colors.black,
                        fontSize: textScale(14),
                        marginStart: moderateScale(16),
                        fontStyle: 'normal',
                      }}>
                      {`Retail Margin : ${prodData && prodData.Retail_Margin}%`}
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={{
                      flexDirection: 'row',
                      marginTop: moderateScale(8),
                      alignItems: 'center',
                    }}
                    onPress={() =>
                      this.props.navigation.navigate('WebViewScreen', {
                        title: 'Return Policy',
                        type: 4,
                        data: prodData,
                      })
                    }>
                    <Image
                      source={constants.images.return_box}
                      style={{
                        height: moderateScale(24),
                        width: moderateScale(24),
                      }}
                    />
                    <Text
                      style={{
                        color: constants.colors.pink,
                        fontSize: textScale(14),
                        fontWeight: '900',
                        marginStart: moderateScale(4),
                        fontStyle: 'normal',
                      }}>
                      {`10 Days Replacement`}
                    </Text>
                  </TouchableOpacity>
                </View>
              ) : null} */}

              {this.emptyView()}

              <View
                style={{
                  paddingHorizontal: moderateScale(16),
                  marginBottom: moderateScale(16),
                }}>
                <Text
                  style={{
                    fontSize: textScale(16),
                    fontFamily: fonts.bold,
                    color: constants.colors.black,
                    paddingTop: textScale(4),
                  }}>
                  Specifications
                </Text>
                <FlatList
                  data={this.state.specificationData}
                  scrollEnabled={false}
                  style={{
                    backgroundColor: constants.colors.separatorColor,
                    marginTop: moderateScale(16),
                  }}
                  renderItem={({item}) => (
                    <View
                      style={{
                        backgroundColor: constants.colors.separatorColor,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        borderColor: constants.colors.separatorColor,
                        borderWidth: moderateScale(1),

                        // marginTop: moderateScale(2),
                      }}>
                      <Text
                        style={{
                          padding: moderateScale(8),
                          backgroundColor: constants.colors.white,
                          width: width / 2 - 20,
                          fontFamily: fonts.mediumBold,
                        }}>
                        {item.Specfication_Name}
                      </Text>
                      <Text
                        style={{
                          padding: moderateScale(8),
                          backgroundColor: constants.colors.white,
                          width: width / 2 - 20,
                          color: constants.colors.pink,
                          fontFamily: fonts.regular,
                        }}>
                        {item.Specfication_Name_Value}
                      </Text>
                    </View>
                  )}
                />
              </View>

              {this.emptyView()}
              {this.state.multiProducts.length > 0 && (
                <View
                  style={{
                    paddingHorizontal: moderateScale(16),
                    marginBottom: moderateScale(16),
                  }}>
                  <Text
                    style={{
                      fontSize: textScale(16),
                      fontFamily: fonts.bold,
                      color: constants.colors.black,
                      paddingTop: textScale(4),
                    }}>
                    Similar Products
                  </Text>
                  <FlatList
                    data={this.state.multiProducts}
                    scrollEnabled={false}
                    style={{
                      backgroundColor: constants.colors.separatorColor,
                      marginTop: moderateScale(16),
                    }}
                    renderItem={({item, index}) => (
                      <View style={{backgroundColor: constants.colors.white}}>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            backgroundColor: constants.colors.white,

                            // borderColor: constants.colors.separatorColor,
                            // borderWidth: moderateScale(1),

                            // marginTop: moderateScale(2),
                          }}>
                          <View style={{width: '80%'}}>
                            <Text
                              style={{
                                padding: moderateScale(8),
                                backgroundColor: constants.colors.white,
                                width: '100%',
                                fontFamily: fonts.mediumBold,
                              }}>
                              {item.Product_Name}
                            </Text>
                            <Text
                              style={{
                                paddingHorizontal: moderateScale(8),
                                backgroundColor: constants.colors.white,
                                width: '100%',
                                fontSize: 13,
                                fontFamily: fonts.regular,
                                marginTop: -8,
                              }}>
                              {item.Product_Description}
                            </Text>
                          </View>
                          <Image
                            style={{width: 120, height: 90}}
                            source={{
                              uri: item.Product_Img,
                            }}
                          />
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            width: '100%',
                            paddingHorizontal: 8,
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              color: constants.colors.pink,
                              width: '50%',
                              fontFamily: fonts.bold,
                            }}>
                            {`${constants.strings.rupee_symbol}${parseFloat(
                              item.Price,
                            ).toFixed(2)} per pc\n`}
                            <Text
                              style={{
                                color: constants.colors.black,
                                width: '50%',
                                fontSize: 12,
                                fontFamily: fonts.regular,
                              }}>{`${item.Price_GST}`}</Text>
                          </Text>
                          <Text
                            style={{
                              // color: constants.colors.pink,
                              fontFamily: fonts.bold,
                            }}>
                            {`Min Qty : ${item.MinQty} pcs`}
                          </Text>
                        </View>
                        {index !== this.state.multiProducts.length - 1 && (
                          <View
                            style={{
                              width: '100%',
                              height: 1,
                              backgroundColor: 'black',
                              marginTop: 16,
                              marginBottom: 8,
                              opacity: 0.1,
                            }}
                          />
                        )}
                      </View>
                    )}
                  />
                </View>
              )}

              {this.state.multiProducts.length > 0 && this.emptyView()}

              <View
                style={{
                  paddingHorizontal: moderateScale(16),
                  marginBottom: moderateScale(16),
                }}>
                <Text
                  style={{
                    fontSize: textScale(16),
                    color: constants.colors.black,
                    paddingTop: textScale(4),
                    fontFamily: fonts.bold,
                  }}>
                  Description
                </Text>

                <Text
                  style={{
                    backgroundColor: constants.colors.white,
                    fontSize: textScale(12),
                    fontFamily: fonts.regular,
                  }}>
                  {prodData && prodData.Product_Description}
                </Text>
              </View>

              {this.emptyView()}

              <View
                style={{
                  paddingHorizontal: moderateScale(16),
                  marginBottom: moderateScale(16),
                }}>
                <Text
                  style={{
                    fontSize: textScale(16),
                    fontFamily: fonts.bold,
                    color: constants.colors.black,
                    paddingTop: textScale(4),
                  }}>
                  Tags
                </Text>
                <FlatList
                  data={this.state.tagsData}
                  style={{width: width}}
                  // numColumns={this.state.tagsData / 1}
                  contentContainerStyle={{
                    alignSelf: 'flex-start',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                  }}
                  renderItem={({item}) => (
                    <TouchableOpacity
                      onPress={() => {
                        let data = {};
                        data.type = 6;
                        data.Product_Name = `${item.tag}`;
                        this.props.navigation.navigate('SearchResultsScreen', {
                          data: data,
                        });
                      }}>
                      <Text
                        style={{
                          color: constants.colors.pink,
                          padding: moderateScale(4),
                          borderWidth: moderateScale(1),
                          marginEnd: moderateScale(8),
                          marginTop: moderateScale(4),
                          borderColor: constants.colors.blue_light,
                          fontFamily: fonts.regular,
                        }}>{`#${item.tag}`}</Text>
                    </TouchableOpacity>
                  )}
                />
              </View>

              {this.emptyView()}

              <View
                style={{
                  paddingHorizontal: moderateScale(16),
                  marginBottom: moderateScale(16),
                }}>
                <Text
                  style={{
                    fontSize: textScale(16),
                    fontFamily: fonts.bold,
                    color: constants.colors.black,
                    paddingTop: textScale(4),
                  }}>
                  Return Policy
                </Text>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}
                  onPress={() => {
                    this.props.navigation.navigate('WebViewScreen', {
                      title: 'Return Policy',
                      type: 4,
                      data: prodData,
                    });
                  }}>
                  <Text
                    style={{
                      backgroundColor: constants.colors.white,
                      fontSize: textScale(14),
                      fontFamily: fonts.regular,
                    }}>
                    {`Fast and easy Returns with in`}{' '}
                    {
                      <Text style={{fontFamily: fonts.bold}}>{`${
                        prodData && prodData.ReturnDays
                      } days`}</Text>
                    }{' '}
                    {` of delivery`}
                  </Text>
                  <Image
                    source={constants.images.backArrowBlack}
                    style={{rotation: 180}}
                  />
                </TouchableOpacity>
              </View>

              {this.emptyView()}

              {this.state.returnPolicyData.length > 1 ? (
                <TouchableOpacity
                  style={{
                    paddingHorizontal: moderateScale(16),
                    marginBottom: moderateScale(16),
                  }}>
                  <Text
                    style={{
                      fontSize: textScale(16),
                      fontFamily: fonts.bold,
                      color: constants.colors.black,
                      paddingTop: textScale(4),
                    }}>
                    Bulk Buying Options
                  </Text>

                  <FlatList
                    data={this.state.returnPolicyData}
                    scrollEnabled={false}
                    style={{
                      backgroundColor: constants.colors.separatorColor,
                      marginTop: moderateScale(16),
                    }}
                    renderItem={({item, index}) => (
                      <View
                        style={{
                          backgroundColor: constants.colors.separatorColor,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          borderColor: constants.colors.separatorColor,
                          borderWidth: moderateScale(1),
                          // marginTop: moderateScale(2),
                        }}>
                        <Text
                          style={{
                            padding: moderateScale(8),
                            backgroundColor: constants.colors.white,
                            width: width / 2 - 20,
                            fontFamily: fonts.regular,
                          }}>
                          {item.name}
                        </Text>
                        <Text
                          style={{
                            padding: moderateScale(8),
                            backgroundColor: constants.colors.white,
                            fontFamily:
                              item.name === 'Return Policy'
                                ? fonts.bold
                                : fonts.regular,
                            color:
                              index !== 0
                                ? constants.colors.red
                                : constants.colors.black,
                            width: width / 2 - 20,
                          }}>
                          {`${user.KYC === true ? item.value : '-'}`}{' '}
                          <Text style={{color: constants.colors.black}}>
                            per pcs
                          </Text>
                          {}
                          <Text
                            style={{
                              color: constants.colors.black,
                              fontSize: textScale(12),
                            }}>
                            {`\n${item.gst}`}
                          </Text>
                        </Text>
                      </View>
                    )}
                  />

                  {/* <View
                  style={{
                    marginTop: moderateScale(16),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    borderWidth: moderateScale(1),
                    borderColor: constants.colors.blue_light,
                    padding: moderateScale(8),
                  }}>
                  <View>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Text
                        style={{
                          color: constants.colors.black,
                          fontSize: textScale(14),
                          fontStyle: 'normal',
                          fontWeight: 'bold',
                        }}>
                        $ 151
                      </Text>
                      <Text
                        style={{
                          color: constants.colors.black,
                          fontSize: textScale(12),
                          marginStart: moderateScale(8),
                          fontStyle: 'normal',
                        }}>
                        Per pc
                      </Text>
                    </View>
                    <Text
                      style={{
                        color: constants.colors.black,
                        fontSize: textScale(12),
                        fontStyle: 'normal',
                      }}>
                      (135 + 12% GST)
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        color: constants.colors.black,
                        fontSize: textScale(12),
                        fontStyle: 'normal',
                        fontWeight: '100',
                      }}>
                      Min Qty
                    </Text>
                    <Text
                      style={{
                        color: constants.colors.black,
                        fontSize: textScale(12),
                        fontStyle: 'normal',
                        textAlign: 'right',
                      }}>
                      40 pcs
                    </Text>
                  </View>
                </View> */}
                </TouchableOpacity>
              ) : null}
            </ScrollView>
          </View>
          {user.KYC === true ? (
            <View style={{flex: 0.1, marginHorizontal: moderateScale(16)}}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  position: 'absolute',
                  bottom: 0,
                  marginBottom: moderateScale(12),
                }}>
                {/* <Text
                style={{
                  width: '30%',
                  padding: moderateScale(8),
                  borderRadius: moderateScale(4),
                  borderWidth: moderateScale(1),
                  borderColor: constants.colors.gray,
                  textAlign: 'center',
                }}>
                Enquiry
              </Text> */}

                <TouchableOpacity
                  style={{width: '100%', alignSelf: 'center'}}
                  onPress={() => {
                    if (prodData && prodData.OutofStock === false) {
                      if (prodData.inventory_stock >= prodData.MinQty) {
                        this.addDataInCart();
                      } else {
                        this.customAlert(
                          'Alert',
                          'Stock is less than your quantity. Please contact with support.',
                        );
                      }
                    }
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 7}}
                    colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      right: 0,
                      height: 48,
                    }}
                  />
                  <Text
                    style={{
                      // width: '68%',
                      padding: moderateScale(12),
                      // marginStart: moderateScale(8),
                      borderRadius: moderateScale(4),
                      fontFamily: fonts.bold,
                      textAlign: 'center',
                      fontSize: textScale(16),
                      color: constants.colors.white,
                      // backgroundColor: constants.colors.pink,
                      opacity:
                        prodData && prodData.OutofStock === false ? 1 : 0.5,
                    }}>
                    Buy Now
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth, category}) => ({
  user: auth.userData,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductDetailsScreen);
