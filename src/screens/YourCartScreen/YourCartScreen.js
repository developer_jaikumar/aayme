import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  FlatList,
  Alert,
  TextInput,
  Picker,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {textScale} from '../../utilities/responsiveSize';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import AsyncStorage from '@react-native-community/async-storage';
import {useFocusEffect} from '@react-navigation/native';
import {fonts} from '../../assets';
import LinearGradient from 'react-native-linear-gradient';

const {height, width} = Dimensions.get('window');

class YourCartScreen extends Component {
  constructor(props) {
    super(props);
    this.pickerr;
    this.isStockAvailable = true;
    this.beforeGstTotal = 0;
    (this.totalGstPrice = 0), (this.totalDeliveryCharges = 0);
    this.minOrderPrice = 0;
    this.state = {
      cartData: [],
      totalAmount: 0,
    };
  }

  componentDidMount() {
    this.getCartData();
  }

  calculateTotalCartValue = () => {
    let {cartData} = this.state;
    let total = 0;
    for (let i = 0; i < cartData.length; i++) {
      total += cartData[i].totalPrice;
    }
    this.totalGstPrice = parseFloat(
      (total * cartData[0].product_detail.gst) / 100,
    );
    this.beforeGstTotal = total;
    this.totalDeliveryCharges = cartData[0].product_detail.delivery_amount;
    this.minOrderPrice = cartData[0].product_detail.Min_order_price;
    this.setState({totalAmount: total});
  };

  getCartData = () => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(`Get_AddToCart_BuyerId?BuyerId=${user.Buyer_Id}`, null)
      .then((res) => {
        console.log('getCart res : ', res);
        actions.hideLoader('false');
        if (res.result.status === 'OK') {
          this._storeData(false);
          for (let i = 0; i < res.data.length; i++) {
            if (res.data[i].product_detail.Product_Qty_Price.length === 0) {
              res.data[i].totalPrice =
                res.data[i].product_detail.Before_GST_Price * res.data[i].Qty;
            } else {
              for (
                let j = 0;
                j < res.data[i].product_detail.Product_Qty_Price.length;
                j++
              ) {
                if (
                  res.data[i].Qty <=
                  res.data[i].product_detail.Product_Qty_Price[j].To_Qty
                ) {
                  res.data[i].totalPrice =
                    res.data[i].product_detail.Product_Qty_Price[j].price *
                    res.data[i].Qty;
                  break;
                }
              }
            }
          }
          this.setState({cartData: res.data});
          this.calculateTotalCartValue(res.data);
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  itemSeprator = () => {
    return (
      <View
        style={{
          height: moderateScale(0.5),
          width: '100%',
          backgroundColor: constants.colors.gray,
        }}></View>
    );
  };

  renderEmptyListComponent = () => {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: height / 2 + 100,
        }}>
        <Image
          style={{height: moderateScale(100), width: moderateScale(100)}}
          source={constants.images.return_box}></Image>
        <Text
          style={{
            marginTop: moderateScale(16),
            fontSize: textScale(14),
            fontFamily: fonts.regular,
          }}>
          No Items Added Yet
        </Text>
        {/* <Text style={{marginTop: moderateScale(4), fontSize: textScale(12)}}>
          View your delivered shipments
        </Text>
        <TouchableOpacity onPress={() => null}>
          <View style={[styles.verifyView,{marginTop:16}]}>
            <Text style={styles.verifyText}>
              {'Create Return'}
            </Text>
          </View>
        </TouchableOpacity> */}
      </View>
    );
  };

  onValueChanged = (value, index) => {
    this.setState({selectedcat: ''});
  };

  onAddItemClick = (item, index) => {
    const {cartData, totalAmount} = this.state;
    let targetCell = cartData[index];
    let currentValue = targetCell.Qty;
    //  alert(currentValue)
    if (currentValue + 1 > targetCell.product_detail.MaxQty) {
      alert(`Max quantity limit is ${targetCell.product_detail.MaxQty}`);
    } else {
      targetCell.Qty = currentValue + 1;
      if (targetCell.product_detail.Product_Qty_Price.length === 0) {
        targetCell.totalPrice =
          targetCell.product_detail.Before_GST_Price * targetCell.Qty;
        this.setState({
          cartData,
          totalAmount: totalAmount + targetCell.totalPrice / targetCell.Qty,
        });
        this.calculateTotalCartValue();
      } else {
        for (
          let i = 0;
          i < targetCell.product_detail.Product_Qty_Price.length;
          i++
        ) {
          if (
            targetCell.Qty <=
            targetCell.product_detail.Product_Qty_Price[i].To_Qty
          ) {
            targetCell.totalPrice =
              targetCell.product_detail.Product_Qty_Price[i].price *
              targetCell.Qty;
            // alert(targetCell.totalPrice)
            // let{totalAmount} = this.state
            // totalAmount -=(targetCell.Selling_Price * (targetCell.Qty-1))
            // totalAmount += targetCell.totalPrice
            this.setState({
              cartData,
            });
            this.calculateTotalCartValue();

            break;
          } else {
            if (i === targetCell.product_detail.Product_Qty_Price.length - 1) {
              if (
                targetCell.Qty >=
                targetCell.product_detail.Product_Qty_Price[i].To_Qty
              ) {
                targetCell.totalPrice =
                  targetCell.product_detail.Before_GST_Price * currentValue;
                this.setState({
                  cartData,
                  totalAmount:
                    totalAmount + targetCell.totalPrice / currentValue,
                });
                this.calculateTotalCartValue();
                break;
              }
            }
          }
        }
      }
      cartData[index] = targetCell;
    }
  };

  onRemoveItemClick = (item, index) => {
    const {cartData, totalAmount} = this.state;
    let targetCell = cartData[index];
    let currentValue = targetCell.Qty;
    if (currentValue > targetCell.MinQty) {
      targetCell.Qty = currentValue - 1;
    }
    console.log('target cell : ', targetCell);
    if (currentValue <= targetCell.MinQty) {
      alert(`Min quantity limit is ${targetCell.MinQty}`);
    } else {
      if (targetCell.product_detail.Product_Qty_Price.length === 0) {
        targetCell.totalPrice =
          targetCell.product_detail.Before_GST_Price * targetCell.Qty;
        this.setState({
          cartData,
          totalAmount: totalAmount - targetCell.totalPrice / targetCell.Qty,
        });
        this.calculateTotalCartValue();
      } else {
        for (
          let i = 0;
          i < targetCell.product_detail.Product_Qty_Price.length;
          i++
        ) {
          if (
            targetCell.Qty <=
            targetCell.product_detail.Product_Qty_Price[i].To_Qty
          ) {
            targetCell.totalPrice =
              targetCell.product_detail.Product_Qty_Price[i].price *
              targetCell.Qty;
            // alert(targetCell.totalPrice)
            // let{totalAmount} = this.state
            // totalAmount -=(targetCell.Selling_Price * (targetCell.Qty-1))
            // totalAmount += targetCell.totalPrice
            this.setState({
              cartData,
            });
            this.calculateTotalCartValue();
            break;
          } else {
            if (i === targetCell.product_detail.Product_Qty_Price.length - 1) {
              if (
                targetCell.Qty >
                targetCell.product_detail.Product_Qty_Price[i].To_Qty
              ) {
                targetCell.totalPrice =
                  targetCell.product_detail.Before_GST_Price * currentValue;
                this.setState({
                  cartData,
                  totalAmount:
                    totalAmount + targetCell.totalPrice / currentValue,
                });
                break;
              }
            }
          }
        }
      }
      cartData[index] = targetCell;
    }
  };

  customAlert = (title, message, item) => {
    Alert.alert(
      title,
      message,
      [
        item !== null ? {text: 'CANCEL'} : null,
        {
          text: 'OK',
          onPress: () => {
            if (item !== null) {
              this.deleteItemFromCart(item);
            }
          },
        },
      ],
      {
        cancelable: true,
      },
    );
  };

  deleteItemFromCart = (item) => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    let data = {};
    data.AddToCart_Id = item.AddToCart_Id;
    Request.post(`Delete_AddToCart_Item`, data)
      .then((res) => {
        console.log('delete cart res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.getCartData();
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  refreshData = () => {
    this.componentDidMount();
  };

  _storeData = async (value) => {
    try {
      await AsyncStorage.setItem('update', value);
      console.log('value storrredd');
    } catch (error) {
      console.log('error stored data', error);
      // Error saving data
    }
  };

  _retrieveData = async () => {
    console.log('reterived method called');
    try {
      const value = await AsyncStorage.getItem('update');
      if (value !== null) {
        // We have data!!
        console.log('value retereived : ', value);
        this.getCartData();
      }
    } catch (error) {
      console.log('error reterieve data data', error);

      // Error retrieving data
    }
  };

  renderFooterComponent = () => {
    return (
      <>
        {this.state.cartData.length > 0 ? (
          <View style={{margin: moderateScale(16)}}>
            {this.itemSeprator()}

            <Text
              style={{
                color: constants.colors.btnGray,
                fontSize: moderateScale(12),
                marginTop: moderateScale(16),
                fontFamily: fonts.bold,
              }}>
              COST SUMMARY
            </Text>

            <View
              style={{
                marginTop: moderateScale(4),
                borderWidth: 2,
                borderColor: constants.colors.separatorColor,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: moderateScale(8),
                  paddingTop: moderateScale(8),
                }}>
                <Text style={{textAlign: 'left', fontFamily: fonts.regular}}>
                  Sub Total
                </Text>
                <Text style={{fontFamily: fonts.regular}}>{`${
                  constants.strings.rupee_symbol
                }${parseFloat(this.beforeGstTotal).toFixed(2)}`}</Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: moderateScale(8),
                  paddingTop: moderateScale(4),
                  paddingBottom: moderateScale(8),
                }}>
                <Text style={{textAlign: 'left', fontFamily: fonts.bold}}>
                  GST
                </Text>
                <Text style={{fontFamily: fonts.bold}}>{`${
                  constants.strings.rupee_symbol
                }${parseFloat(this.totalGstPrice).toFixed(2)}`}</Text>
              </View>
              {/* <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: moderateScale(8),
                  paddingTop: moderateScale(4),
                  paddingBottom: moderateScale(8),
                }}>
                <Text style={{textAlign: 'left'}}>Delivery Charges</Text>
                <Text>{`${constants.strings.rupee_symbol}${parseFloat(
                  this.totalDeliveryCharges,
                ).toFixed(2)}`}</Text>
              </View> */}
              {this.itemSeprator()}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: moderateScale(8),
                  paddingBottom: moderateScale(8),
                  paddingTop: moderateScale(8),
                }}>
                <Text style={{textAlign: 'left', fontFamily: fonts.bold}}>
                  Total
                </Text>
                <Text style={{fontFamily: fonts.bold, textAlign: 'right'}}>{`${
                  constants.strings.rupee_symbol
                }${parseFloat(this.beforeGstTotal + this.totalGstPrice).toFixed(
                  2,
                )}\n (+delivery charges)`}</Text>
              </View>
            </View>
          </View>
        ) : null}
      </>
    );
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar barStyle="dark-content" hidden={false} />
        <RefreshData onUpdate={this._retrieveData} />
        <HeaderComp
          leftText={'Your Cart'}
          leftTextStyle={{color: constants.colors.white}}
          imageLeft={constants.images.backArrowBlack}
          leftImageTint={constants.colors.white}
          headerContainerStyle={{backgroundColor: constants.colors.pink}}
          leftImgHandler={() => this.props.navigation.goBack()}
        />
        <View style={{flex: 0.9}}>
          <FlatList
            contentContainerStyle={{paddingBottom: moderateScale(160)}}
            data={this.state.cartData}
            renderItem={({item, index}) => {
              return (
                <View
                  style={{
                    width: width,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginHorizontal: moderateScale(16),
                    paddingVertical: moderateScale(16),
                  }}>
                  <View style={{width: '60%'}}>
                    <Text
                      style={{
                        fontFamily: fonts.bold,
                        fontSize: textScale(16),
                      }}>{`${item.Product_Name}`}</Text>
                    {/* <Text
                      style={{
                        fontSize: textScale(14),
                      }}>{`${item.Product_Description}`}</Text> */}
                    <Text
                      style={{
                        fontFamily: fonts.regular,
                      }}>{`Min Qty : ${item.MinQty}`}</Text>
                  </View>
                  <View style={{width: '50%'}}>
                    {/* <View
                      style={{
                        width: '40%',
                        height: moderateScale(30),
                      }}
                      onPress={() => null}> */}
                    <View
                      style={{
                        flexDirection: 'row',
                        borderColor: constants.colors.btnGray,
                        borderWidth: moderateScale(1),
                        width: moderateScale(108),
                        // padding:moderateScale(4),
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        style={{
                          width: moderateScale(15),
                          height: moderateScale(2),
                          backgroundColor: constants.colors.black,
                          marginStart: moderateScale(8),
                        }}
                        hitSlop={{top: 32, bottom: 32, left: 16, right: 16}}
                        onPress={() =>
                          this.onRemoveItemClick(item, index)
                        }></TouchableOpacity>
                      <View
                        style={{
                          width: moderateScale(1),
                          height: moderateScale(36),
                          marginHorizontal: moderateScale(10),
                          backgroundColor: constants.colors.btnGray,
                        }}></View>

                      <Text style={{fontFamily: fonts.regular}}>
                        {item.Qty}
                      </Text>
                      <View
                        style={{
                          width: moderateScale(1),
                          height: moderateScale(30),
                          marginHorizontal: moderateScale(10),
                          backgroundColor: constants.colors.btnGray,
                        }}></View>
                      <TouchableOpacity
                        onPress={() => this.onAddItemClick(item, index)}
                        hitSlop={{top: 32, bottom: 32, left: 16, right: 32}}>
                        <Text style={{fontSize: textScale(24)}}>+</Text>
                      </TouchableOpacity>
                    </View>
                    {/* </View> */}
                    <TouchableOpacity
                      style={{
                        width: moderateScale(108),
                        marginTop: moderateScale(16),
                      }}
                      onPress={() =>
                        this.customAlert(
                          'Delete Item',
                          'Are you sure you want to delete this item from your cart?',
                          item,
                        )
                      }
                      // hitSlop={{top: 32, bottom: 32, left: 16, right:0}}
                    >
                      <Text
                        style={{
                          width: '100%',
                          fontSize: textScale(14),
                          fontFamily: fonts.bold,
                          color: constants.colors.red,
                          marginTop: moderateScale(16),
                          textAlign: 'right',
                        }}>
                        {'Remove'}
                      </Text>
                    </TouchableOpacity>
                    <Text
                      style={{
                        fontSize: textScale(14),
                        width: moderateScale(108),
                        fontFamily: fonts.bold,
                        marginTop: moderateScale(16),
                        textAlign: 'right',
                      }}>{`${constants.strings.rupee_symbol}${parseFloat(
                      item.totalPrice,
                    ).toFixed(2)}`}</Text>
                  </View>
                </View>
              );
            }}
            ListEmptyComponent={this.renderEmptyListComponent()}
            ItemSeparatorComponent={this.itemSeprator}
            ListFooterComponent={this.renderFooterComponent()}
          />
        </View>

        <View style={{flex: 0.1, marginHorizontal: moderateScale(16)}}>
          <View
            style={{
              position: 'absolute',
              bottom: 0,
              marginBottom: moderateScale(56),
            }}>
            {/* <View
              style={{
                backgroundColor: constants.colors.separatorColor,
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderColor: constants.colors.separatorColor,
                borderWidth: moderateScale(1),

                // marginTop: moderateScale(2),
              }}>
              <Text
                style={{
                  padding: moderateScale(8),
                  backgroundColor: constants.colors.white,
                  width: width / 2 - 20,
                  fontWeight: 'bold',
                }}>
                {'Delivery Charges'}
              </Text>
              <Text
                style={{
                  padding: moderateScale(8),
                  backgroundColor: constants.colors.white,
                  width: width / 2 - 20,
                  fontWeight: 'bold',
                  fontSize: textScale(18),
                  textAlign: 'right',
                }}>
                {`${constants.strings.rupee_symbol}${0}`}
              </Text>
            </View> */}
            <View
              style={{
                backgroundColor: constants.colors.separatorColor,
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderColor: constants.colors.separatorColor,
                borderWidth: moderateScale(1),

                // marginTop: moderateScale(2),
              }}>
              {this.state.cartData.length > 0 &&
              this.minOrderPrice >= this.state.totalAmount ? (
                <Text
                  style={{
                    padding: moderateScale(8),
                    backgroundColor: constants.colors.white,
                    width: '100%',
                    fontFamily: fonts.regular,
                  }}>
                  {`Add`}{' '}
                  <Text
                    style={{
                      color: constants.colors.red,
                      fontFamily: fonts.regular,
                    }}>{`Rs ${parseFloat(
                    this.minOrderPrice - this.state.totalAmount,
                  ).toFixed(2)}`}</Text>{' '}
                  {`more to complete this order`}
                </Text>
              ) : null}
              {/* <Text
                style={{
                  padding: moderateScale(8),
                  backgroundColor: constants.colors.white,
                  width: width / 2 - 20,
                  fontWeight: 'bold',
                  fontSize: textScale(18),
                  textAlign: 'right',
                }}>
                {`${constants.strings.rupee_symbol}${this.state.totalAmount}`}
              </Text> */}
            </View>
          </View>
          {this.state.cartData.length > 0 ? (
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-around',
                position: 'absolute',
                bottom: 0,
                marginTop: moderateScale(32),
                marginBottom: moderateScale(12),
              }}>
              <TouchableOpacity
                style={{width: '50%', alignSelf: 'center'}}
                onPress={() => {
                  const {cartData} = this.state;
                  let data = {};
                  data.type = 4;
                  data.from = 'cart';
                  data.seller_id =
                    cartData && cartData[0].product_detail.Seller_Id;
                  data.Product_Name =
                    cartData && cartData[0].product_detail.Seller_Name;
                  this.props.navigation.push('SearchResultsScreen', {
                    data: data,
                    refreshData: this.refreshData,
                  });
                }}>
                <LinearGradient
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 4}}
                  colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                  style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    height: 48,
                    borderRadius: moderateScale(4),
                  }}
                />
                <Text
                  style={{
                    // width: '68%',
                    padding: moderateScale(12),
                    // marginStart: moderateScale(8),
                    borderRadius: moderateScale(4),
                    fontFamily: fonts.bold,
                    textAlign: 'center',
                    fontSize: textScale(16),
                    color: constants.colors.white,
                    // backgroundColor: constants.colors.pink,
                  }}>
                  Add New Item
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{width: '50%', alignSelf: 'center'}}
                onPress={() => {
                  if (this.minOrderPrice <= this.state.totalAmount) {
                    for (let i = 0; i < this.state.cartData.length; i++) {
                      if (
                        this.state.cartData[i].product_detail.inventory_stock <
                        this.state.cartData[i].Qty
                      ) {
                        this.isStockAvailable = false;
                        this.customAlert(
                          'Alert',
                          'Stock is less than your quantity. Please contact with support.',
                          null,
                        );
                        break;
                      }
                    }
                    if (this.isStockAvailable) {
                      this.props.navigation.navigate('EditProfileScreen', {
                        cartData: this.state.cartData,
                        totalAmount: this.state.totalAmount,
                      });
                    }
                  } else {
                    let diff = this.minOrderPrice - this.state.totalAmount;
                    this.customAlert(
                      'Alert',
                      `Add Rs. ${parseFloat(diff).toFixed(
                        2,
                      )} more to complete this order`,
                      null,
                    );
                  }
                }}>
                <LinearGradient
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 4}}
                  colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                  style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    height: 48,
                    borderRadius: moderateScale(4),
                    marginStart: moderateScale(16),
                  }}
                />
                <Text
                  style={{
                    // width: '68%',
                    padding: moderateScale(12),
                    // marginStart: moderateScale(8),
                    borderRadius: moderateScale(4),
                    fontFamily: fonts.bold,
                    textAlign: 'center',
                    marginStart: moderateScale(16),
                    fontSize: textScale(16),
                    color: constants.colors.white,
                    // backgroundColor: constants.colors.pink,
                  }}>
                  Buy Now
                </Text>
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth, category}) => ({
  user: auth.userData,
  categoryClicked: category.setCategoryShown,
});

export default connect(mapStateToProps, mapDispatchToProps)(YourCartScreen);

function RefreshData({onUpdate}) {
  useFocusEffect(
    React.useCallback(() => {
      new YourCartScreen()._storeData('true');
      console.log('isFocuseddddddddddddddddddddddddd');
      onUpdate();
    }, [onUpdate]),
  );

  return null;
}
