import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {FlatGrid} from 'react-native-super-grid';

import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {textScale} from '../../utilities/responsiveSize';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import { fonts } from '../../assets';
const {height, width} = Dimensions.get('window');

const res =
  'https://raw.githubusercontent.com/instamobile/tinder-react-native/master/assets/5.jpg';
const res1 =
  'https://raw.githubusercontent.com/instamobile/tinder-react-native/master/assets/1.jpg';
const res2 =
  'https://raw.githubusercontent.com/instamobile/tinder-react-native/master/assets/2.jpg';
const res3 =
  'https://raw.githubusercontent.com/instamobile/tinder-react-native/master/assets/3.jpg';
const res4 =
  'https://raw.githubusercontent.com/instamobile/tinder-react-native/master/assets/4.jpg';
const res5 =
  'http://gorestaurants.net/wp-content/uploads/2012/06/Portrait-Restaurant.jpg';

class CategoriesScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryData: [],
    };
  }

  componentDidMount(){
    this.getCategoryData()
  }

//   renderEmptyListComponent = () => {
//     return (
//       <View
//         style={{
//           alignItems: 'center',
//           justifyContent: 'center',
//           height: height / 2 + 100,
//         }}>
//         <Image
//           style={{height: moderateScale(100), width: moderateScale(100)}}
//           source={constants.images.return_box}></Image>
//         <Text
//           style={{
//             marginTop: moderateScale(16),
//             fontSize: textScale(14),
//             fontWeight: 'bold',
//           }}>
//           No Results Found
//         </Text>
//         <Text style={{marginTop: moderateScale(4), fontSize: textScale(12)}}>
//           View your delivered shipments
//         </Text>
//         <TouchableOpacity onPress={() => null}>
//           <View style={[styles.verifyView,{marginTop:16}]}>
//             <Text style={styles.verifyText}>
//               {'Create Return'}
//             </Text>
//           </View>
//         </TouchableOpacity>

//       </View>
//     );
//   };

getCategoryData = () => {
  let {actions, user} = this.props;
  actions.showLoader(true);
  Request.get(`Get_Categories?buyer_id=${user.Buyer_Id}`, null)
    .then((res) => {
      console.log('categories res : ', res);
      actions.hideLoader(false);
      if (res.result.status === 'OK') {
        this.setState({categoryData: res.data});
      } else {
        alert('Server Error');
        console.log('api error : ', res);
      }
    })
    .catch((err) => {
      actions.hideLoader(false);
      MyToast.showToast(err.message());
    });
};

renderEmptyListComponent = () => {
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        height: height / 2 + 100,
      }}>
      <Image
        style={{height: moderateScale(100), width: moderateScale(100)}}
        source={constants.images.return_box}></Image>
      <Text
        style={{
          marginTop: moderateScale(16),
          fontSize: textScale(14),
          fontWeight: 'bold',
        }}>
        No Data
      </Text>
      {/* <Text style={{marginTop: moderateScale(4), fontSize: textScale(12)}}>
        View your delivered shipments
      </Text>
      <TouchableOpacity onPress={() => null}>
        <View style={[styles.verifyView,{marginTop:16}]}>
          <Text style={styles.verifyText}>
            {'Create Return'}
          </Text>
        </View>
      </TouchableOpacity> */}
    </View>
  );
};

  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar barStyle="dark-content" hidden={false} />
        <HeaderComp
          leftText={'Select Category'}
          leftTextStyle={{color: constants.colors.white}}
          imageLeft={constants.images.backArrowBlack}
          leftImageTint = {constants.colors.white}
          headerContainerStyle={{backgroundColor: constants.colors.pink}}
          leftImgHandler={() => this.props.navigation.goBack()}
        />
              <FlatGrid
                data={this.state.categoryData}
                style={{backgroundColor: '#F0F0F0'}}
                itemDimension={135}
                spacing={2}
                ListEmptyComponent = {this.renderEmptyListComponent()}
              renderItem={({item}) => (
                  <TouchableOpacity
                  onPress={() => {
                    let data = {};
                    data['Product_Name'] = item.cat_name;
                    // data['type'] = 0;
                    data['cat_id'] = item.cat_id;
                    if(item.have_sub_cat === true){
                      this.props.navigation.navigate('CategoryExpandScreen', {
                        data: data,
                      });
                    }else{
                      this.props.navigation.navigate('SearchResultsScreen', {
                        data: data,
                      });
                    }
                  }}
                activeOpacity = {0.5}
                    style={[
                      styles.gridItemView,
                      {
                        backgroundColor: constants.colors.white,
                        paddingTop: moderateScale(8),
                      },
                    ]}>
                    <Image
                      style={styles.circleImage}
                      source={{uri: item.cat_img}}></Image>
                    <View
                      style={{
                        backgroundColor: constants.colors.white,
                        padding: moderateScale(4),
                        marginTop: moderateScale(8),
                        width: '100%',
                        alignItems: 'center',
                      }}>
                      <Text style = {{fontFamily:fonts.mediumBold}}>{item.cat_name}</Text>
                    </View>
                  </TouchableOpacity>
                )}
              />
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth, category}) => ({
  user: auth.userData,
  categoryClicked: category.setCategoryShown,
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesScreen);
