import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
  Alert,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import {textScale} from '../../utilities/responsiveSize';
import {fonts} from '../../assets';
import LinearGradient from 'react-native-linear-gradient';

const {height, width} = Dimensions.get('window');

class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        {name: `Categories`, image: constants.images.category},
        {name: `Your Orders`, image: constants.images.order},
        {name: `Your Returns`, image: constants.images.parcel},
        {name: `About Aayme`, image: constants.images.information},
        {name: `Terms Of Use`, image: constants.images.terms},
        {name: `Policies`, image: constants.images.contract},
        {name: `Support`, image: constants.images.call_support},
        {name: `Logout`, image: constants.images.signout},
      ],
    };
  }

  itemSeprator = () => {
    return (
      <View
        style={{
          height: moderateScale(1),
          // width: '100%',
          alignItems: 'center',
          justifyContent: 'center',
          marginHorizontal: moderateScale(16),
          backgroundColor: constants.colors.btnGray,
        }}></View>
    );
  };

  goToScreen = (screen) => {
    console.log('screen is', screen);
    const {actions} = this.props;
    switch (screen) {
      case 'Categories':
        this.props.navigation.navigate('CategoriesScreen');
        // actions.showCategoryView(true);
        // this.props.navigation.navigate('HomeScreen');
        break;

      case 'Your Orders':
        this.props.navigation.navigate('YourOrdersScreen');
        break;

      case 'Your Returns':
        this.props.navigation.navigate('YourReturnsScreen');
        break;

      case 'Logout':
        this.logOut();
        break;

      case 'About Aayme':
        this.props.navigation.navigate('WebViewScreen', {
          title: 'About aayme',
          type: 1,
        });
        break;

      case 'Terms Of Use':
        this.props.navigation.navigate('WebViewScreen', {
          title: 'Terms Of Use',
          type: 2,
        });
        break;

      case 'Policies':
        this.props.navigation.navigate('WebViewScreen', {
          title: 'Policies',
          type: 3,
        });
        break;

      case 'Support':
        this.props.navigation.navigate('WebViewScreen', {title: 'Support'});
        break;

      default:
        this.props.navigation.navigate('HomeScreen');
    }
    // this.props.navigation.navigate('ChatHistoryScreen')
  };

  logOut = () => {
    Alert.alert(
      'Logout',
      'Are you sure you want to logout?',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'Ok',
          onPress: () => this.exit(),
        },
      ],
      {
        cancelable: false,
      },
    );
  };

  exit = () => {
    const {actions} = this.props;
    actions.saveUserData(null);
    this.props.navigation.push('UserAuthFlow');
  };

  renderFooter = () => {
    return (
      <View
        style={{
          width: '100%',
          backgroundColor: constants.colors.white,
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
          paddingHorizontal: moderateScale(16),
          paddingBottom: moderateScale(8),
        }}>
        <Image
          source={constants.images.new_logo}
          style={{
            height: moderateScale(48),
            width: moderateScale(150),
            marginStart: moderateScale(-8),
          }}
        />
        <Text
          style={{
            fontFamily: fonts.regular,
            fontSize: textScale(8),
            marginTop: moderateScale(8),
          }}>
          Copyright © 2020 AAYME - All rights reserved
        </Text>
      </View>
    );
  };

  render() {
    const {user} = this.props;
    console.log('userrrrr: ', user);
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.navHeader}
          onPress={() => this.props.navigation.navigate('MyAccountsScreen')}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1.5}}
            colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              height: 174,
              borderRadius: moderateScale(4),
              borderBottomRightRadius:moderateScale(150),
              borderBottomLeftRadius:moderateScale(8)
            }}
          />
          <Image
            // source = {constants.images.userPlaceHolder}
            source={{uri: user && user.Profile_Pic}}
            style={styles.image}
          />
          <View
            style={{
              height: 32,
              width: 32,
              borderRadius: 16,
              position: 'absolute',
              left: 0,
              marginTop: moderateScale(24),
              marginStart: moderateScale(54),
              borderStyle: 'dashed',
              borderColor: constants.colors.white,
              borderWidth: moderateScale(1),
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              style={{
                height: 24,
                width: 24,
                borderRadius: 12,
                backgroundColor: constants.colors.white,
                marginTop: moderateScale(0),
                marginStart: moderateScale(0),
              }}
              source={constants.images.edit}
              tintColor={constants.colors.skyBlue}
            />
          </View>
          <View
            style={{
              alignItems: 'flex-start',
              marginTop: moderateScale(8),
              marginStart: moderateScale(8),
            }}>
            <Text
              style={
                ([styles.mobileNuberVerificationText],
                {
                  fontSize: textScale(20),
                  fontFamily: fonts.regular,
                  color: constants.colors.white,
                })
              }>
              {user.Buyer_Name}
            </Text>
            <Text
              style={
                ([styles.enterMobileText],
                {
                  fontSize: textScale(14),
                  color: constants.colors.white,
                  fontFamily: fonts.mediumBold,
                })
              }>{`+91 ${user.Mobile_Number}`}</Text>
            <Text
              style={
                ([styles.enterMobileText],
                {
                  width: '80%',
                  fontSize: textScale(14),
                  color: constants.colors.white,
                  marginTop: moderateScale(0),
                  fontFamily: fonts.mediumBold,
                })
              }>{`${user.Firm_Name}`}</Text>
          </View>
        </TouchableOpacity>
        <FlatList
          data={this.state.items}
          style={{height: height, backgroundColor: constants.colors.white}}
          ItemSeparatorComponent={this.itemSeprator}
          ListFooterComponent={this.renderFooter}
          renderItem={({item, index}) => (
            <TouchableOpacity
              style={[
                styles.labletextRowContainer,
                {
                  paddingHorizontal: moderateScale(16),
                  paddingVertical: moderateScale(16),
                },
              ]}
              onPress={() => {
                this.goToScreen(item.name);
              }}>
              <TouchableOpacity>
                <Image source={item.image} style={{height: 20, width: 20}} />
              </TouchableOpacity>
              {/*<View style={styles.emptyWidth5}/>*/}
              <Text
                style={{
                  marginLeft: moderateScale(3),
                  opacity: 0.7,
                  paddingStart: moderateScale(16),
                  fontFamily: fonts.mediumBold,
                  color:
                    index === this.state.items.length - 1
                      ? constants.colors.red
                      : constants.colors.black,
                }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth}) => ({
  user: auth.userData,
});

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu);
