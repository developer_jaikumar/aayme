import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {RFValue} from 'react-native-responsive-fontsize';

import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {ScrollView, FlatList} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';

const {height, width} = Dimensions.get('window');

class KYCStepOneScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionData: [],
    };
  }

  componentDidMount(){
    this.getKycOptions()
  }

  getKycOptions = () => {
    let {actions, } = this.props;
    actions.showLoader(true);
    Request.get('Buyer_KYC_Option_List?user_type=buyer', null)
      .then((res) => {
        console.log('get kyc options res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({optionData: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };


  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar barStyle="dark-content" hidden={false} />
        <HeaderComp
          leftText={'KYC Verification'}
          leftTextStyle={{color: constants.colors.white}}
          imageLeft={constants.images.backArrowBlack}
          leftImageTint={constants.colors.white}
          imageRight={constants.images.cart}
          rightImgHandler ={()=>this.props.navigation.navigate("YourCartScreen")}
          headerContainerStyle={{backgroundColor: constants.colors.pink}}
          leftImgHandler={() => this.props.navigation.goBack()}
        />
          <View
            style={{
              height: height,
              backgroundColor: constants.colors.white,
              paddingHorizontal: moderateScale(24),
              paddingTop: moderateScale(16),
            }}>
            <Text style={{fontWeight: 'bold', fontSize: RFValue(18)}}>
              Select any one of the following documents
            </Text>
            <FlatList
              data={this.state.optionData}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginTop: moderateScale(32),
                  }}
                  onPress={() =>
                    this.props.navigation.navigate('KYCStepTwoScreen',{data:item})
                  }>
                  <View>
                    <View style={{flexDirection: 'row'}}>
                      <Text>{item.Name}</Text>
                      {item.Name === "GST Certificate"?
                      <Text
                        style={{
                          fontSize: RFValue(10),
                          backgroundColor: constants.colors.pink,
                          padding: moderateScale(4),
                          color: constants.colors.white,
                          marginStart: moderateScale(4),
                        }}>
                        INSTANT VERIFICATION
                      </Text>
                      :null}
                    </View>
                    {item.Name === "GST Certificate"?

                    <Text style={{fontSize: RFValue(12)}}>
                      Get verified in just 2 minutes
                    </Text>
                    :null}
                  </View>
                  <Image
                    source={constants.images.backArrowBlack}
                    style={{rotation: 180}}></Image>
                </TouchableOpacity>
              )}
            />

          </View>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth}) => ({
  user: auth.userData,
});

export default connect(mapStateToProps, mapDispatchToProps) (KYCStepOneScreen);
