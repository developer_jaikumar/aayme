import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
  Alert
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import { textScale } from '../../utilities/responsiveSize';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import ImagePicker from 'react-native-image-picker';

const{height,width} = Dimensions.get('window')

class KYCStepTwoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imagePath: '',
    };
  }

  openImagePicker = () => {
    const options = {
      title: 'Select Avatar',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
     
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };     
         this.setState({
          imagePath: response.uri,
        });
        this.uploadKycDocument(response.data)
      }
    });
  }

  uploadKycDocument = (imagePath) => {
    const{data} = this.props.route.params
    const{user} = this.props
      let{actions} = this.props
      actions.showLoader(true);
      let kycData = new FormData();
      kycData.append("BuyerId",user.Buyer_Id);
      kycData.append("KYC_Type_Id",data.KYC_Type_Id);
      kycData.append("KYCFile",imagePath);
      // let finalTextOrder = dynamicRandomName();
      // data.append('ProfilePic', {
      //   uri: imagePath,
      //   name: finalTextOrder + `.png`,
      //   type: 'multipart/form-data'
      // });

  
      console.log("Kyc upload image req Req : ",kycData);
      Request.post("Buyer_KYC_Update",kycData)
      .then((res) => {
        console.log('kyc response : ',res)
        actions.hideLoader(false);
        if(res.result.status === 'OK'){
          this.customAlert('KYC Verification','Your KYC request has been sent. Aayme takes 2-3 working days to verify your documents.')
        }else{
          alert('Server Error')
          console.log('api error : ',res)
        }

      }).catch((err) =>{
        actions.hideLoader(false);
        MyToast.showToast(err.message())
      })
  }

  customAlert = (title, message) => {
    Alert.alert(
      title,
      message,
      [
        {
          text: "OK",
          onPress: () =>
          this.props.navigation.navigate('HomeScreen')

        },
      ],
      {
        cancelable: true,
      }
    );
  };

  render() {
    const{data} = this.props.route.params
    return (
        <View style={{flex: 1}}>
        <StatusBar barStyle="dark-content" hidden={false} />
        <HeaderComp
          leftText={data.Name}
          leftTextStyle={{color: constants.colors.white}}
          leftImageTint = {constants.colors.white}
          imageLeft={constants.images.backArrowBlack}
          imageRight={constants.images.cart}
          rightImgHandler ={()=>this.props.navigation.navigate("YourCartScreen")}
          headerContainerStyle={{backgroundColor: constants.colors.pink}}
          leftImgHandler={() => this.props.navigation.goBack()}
        />
        <Text style ={{fontWeight:'bold',fontSize:textScale(14),marginHorizontal:moderateScale(16),marginTop:moderateScale(16)}}>
          Please upload an valid document otherwise your verification request will be rejected by aayMe.</Text>
          <View style = {{height:moderateScale(200),alignItems:'center',justifyContent:'center',
          marginTop:moderateScale(48),
            marginHorizontal:moderateScale(16),
            borderWidth:1,
            borderStyle:'dashed',
            borderRadius:1,
            borderColor:constants.colors.pink,
            backgroundColor:constants.colors.separatorColor}}>
              <View style = {{alignItems:'center'}}>
                <Image source = {constants.images.camera}
                style = {{height:moderateScale(24),width:moderateScale(24)}}/>
                <Text style = {{color:constants.colors.grayDark,width:250,textAlign:'center',marginTop:moderateScale(8)}}>Upload High resolution for better approval chances</Text>
                <TouchableOpacity onPress={() => this.openImagePicker()}>
              <View style={styles.verifyView}>
                <Text style={styles.verifyText}>{'Upload Document'}</Text>
              </View>
            </TouchableOpacity>
              </View>

          </View>
        </View>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth}) => ({
  user: auth.userData,
});

export default connect(mapStateToProps, mapDispatchToProps)(KYCStepTwoScreen)
