import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
  FlatList,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {textScale} from '../../utilities/responsiveSize';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import { ScrollView } from 'react-native-gesture-handler';
import { fonts } from '../../assets';
const {height, width} = Dimensions.get('window');

class MyAccountScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      businessData: [],
    };
  }

  componentDidMount() {
    const {user} = this.props;
    const {businessData} = this.state;
    businessData.push({name: 'Firm Name', value: user.Firm_Name});
    if(user.Email!== undefined && user.Email!=="" ){
      businessData.push({
        name: 'Email',
        value: `${user.Email}`,
      });  
    }

    if(user.City!== undefined && user.City!=="" ){
      businessData.push({
        name: 'City',
        value: `${user.City}`,
      });  
    }

    if(user.State!== undefined && user.State!=="" ){
      businessData.push({
        name: 'State',
        value: `${user.State}`,
      });  
    }


    businessData.push({name: 'Pin Code', value: user.Pincode});
    businessData.push({name: 'Address', value: user.Address});
    businessData.push({name: 'Billing Address', value: user.BillingAddress});
    businessData.push({name: 'Shipping Address', value: user.DeliveryAddress});
    if(user.Alternate_Number!== undefined && user.Alternate_Number!=="" ){
      businessData.push({
        name: 'Alternate Number',
        value: `+91 ${user.Alternate_Number}`,
      });  
    }

    if(user.Bank_Name!== undefined && user.Bank_Name!=="" ){
      businessData.push({
        name: 'Bank Name',
        value: `${user.Bank_Name}`,
      });  
    }

    if(user.Bank_AC_No!== undefined && user.Bank_AC_No!=="" ){
      businessData.push({
        name: 'Bank A/C No.',
        value: `${user.Bank_AC_No}`,
      });  
    }

    if(user.Bank_Ifsc_Code!== undefined && user.Bank_Ifsc_Code!=="" ){
      businessData.push({
        name: 'Bank IFSC Code',
        value: `${user.Bank_Ifsc_Code}`,
      });  
    }




    this.setState({businessData});
  }

  emptyView = () => {
    return (
      <View
        style={{
          paddingVertical: moderateScale(4),
          width: width,
          backgroundColor: constants.colors.separatorColor,
          marginVertical: moderateScale(16),
        }}
      />
    );
  };

  updateData = () => {
    this.setState({businessData:[]})
    this.componentDidMount()
  }

  render() {
    const {user} = this.props;
    return (
      <View style={styles.container}>
        <HeaderComp
          leftText={'My Account'}
          leftTextStyle={{color: constants.colors.white}}
          imageRight = {constants.images.edit}
          rightImgHandler={()=>this.props.navigation.navigate('EditProfileScreen',{'updateData':this.updateData})}
          // imageLeft={
          //   this.props.route.params !== undefined
          //     ? constants.images.backArrowBlack
          //     : null
          // }
          // leftImageTint={constants.colors.white}
          
          headerContainerStyle={{backgroundColor: constants.colors.pink}}
          leftImgHandler={() => this.props.navigation.goBack()}
        />

        {/* <View style={{height: moderateScale(56)}}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('EditProfileScreen')}
            style={{
              height: moderateScale(56),
              width: moderateScale(56),
              right: 0,
              position: 'absolute',
            }}>
            <Image
              source={constants.images.edit}
              style={{
                tintColor: constants.colors.pink,
                padding: moderateScale(16),
                margin: moderateScale(16),
              }}
            />
          </TouchableOpacity>
        </View> */}
        <ScrollView>
          <> 
        <View
          style={{
            alignItems: 'flex-start',
            paddingHorizontal: moderateScale(16),
            flexDirection: 'row',
            marginTop:moderateScale(16)
          }}>
          <Image
            style={{
              height: moderateScale(100),
              width: moderateScale(100),
              borderRadius: moderateScale(50),
            }}
            source={{uri: user.Profile_Pic}}
          />
          <View
            style={{
              justifyContent: 'center',
              marginTop: moderateScale(16),
              marginStart: moderateScale(8),
            }}>
            <Text style={{fontSize: moderateScale(20), fontFamily:fonts.bold}}>
              {user.Buyer_Name}
            </Text>
            <Text
              style={{
                fontSize: moderateScale(14),
                fontFamily:fonts.regular}}>{`+91 ${user.Mobile_Number}`}</Text>
          </View>
        </View>
        {this.emptyView()}

        <View
          style={{
            paddingHorizontal: moderateScale(16),
            marginBottom: moderateScale(16),
          }}>
          {/* <Text
            style={{
              fontSize: textScale(16),
              fontWeight: 'bold',
              color: constants.colors.black,
              paddingTop: textScale(4),
            }}>
            Business Details
          </Text> */}
          <FlatList
            data={this.state.businessData}
            scrollEnabled={true}
            // contentContainerStyle = {{paddingBottom:moderateScale(100)}}
            style={{
              backgroundColor: constants.colors.separatorColor,
              marginTop: moderateScale(4),
            }}
            renderItem={({item}) => (
              <View
                style={{
                  backgroundColor: constants.colors.separatorColor,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  // borderColor: constants.colors.separatorColor,
                  // borderWidth: moderateScale(1),

                  // marginTop: moderateScale(2),
                }}>
                <Text
                  style={{
                    padding: moderateScale(8),
                    backgroundColor: constants.colors.white,
                    width: width / 2 - 50,
                    fontFamily:fonts.bold}}>
                  {item.name}
                </Text>
                <Text
                  style={{
                    padding: moderateScale(8),
                    backgroundColor: constants.colors.white,
                    width: moderateScale(20),
                    fontFamily:fonts.regular
                  }}>
                  {':'}
                </Text>

                <Text
                  style={{
                    padding: moderateScale(8),
                    backgroundColor: constants.colors.white,
                    width: width / 2 + 40,
                    fontFamily:fonts.regular

                  }}>
                  {item.value}
                </Text>
              </View>
            )}
          />
        </View>
        </>
        </ScrollView>

      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth, category}) => ({
  user: auth.userData,
  categoryClicked: category.setCategoryShown,
});

export default connect(mapStateToProps, mapDispatchToProps)(MyAccountScreen);
