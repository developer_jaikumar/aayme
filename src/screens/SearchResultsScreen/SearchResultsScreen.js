import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {FlatList} from 'react-native-gesture-handler';
import {textScale} from '../../utilities/responsiveSize';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import {fonts} from '../../assets';
import * as MyToast from '../../components/Toast';
import LinearGradient from 'react-native-linear-gradient';

const {height, width} = Dimensions.get('window');

class SearchResultsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [{}],
    };
  }

  componentDidMount() {
    const {data} = this.props.route.params;
    if (data.type === 0) {
      this.state.list.push(data);
      this.setState({
        list: (this.state.list = this.state.list.filter((item, index) => {
          return index !== 0;
        })),
      });
    } else if (data.type === 1) {
      this.getAllLatestProducts();
    } else if (data.type === 2) {
      this.getAllFeaturedProducts();
    } else if (data.type === 3) {
      this.getAllTopSellingProducts();
    } else if (data.type === 4) {
      this.getSupplierProducts(data.seller_id);
    } else if (data.type === 5) {
      this.getAdProducts(data.prod_ids);
    } else if (data.type === 6) {
      this.getSearchResults(data.Product_Name);
    } else if(data.type === 7){
      this.getAllViewAllProducts(data.productType)
    }
    else {
      this.getSelectedCategoryProducts(data.cat_id);
    }
  }

  itemSeprator = () => {
    return (
      <View
        style={{
          height: moderateScale(0.5),
          width: '100%',
          backgroundColor: constants.colors.gray,
        }}></View>
    );
  };

  renderEmptyListComponent = () => {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: height / 2 + 100,
        }}>
        <Image
          style={{height: moderateScale(100), width: moderateScale(100)}}
          source={constants.images.return_box}></Image>
        <Text
          style={{
            marginTop: moderateScale(16),
            fontSize: textScale(14),
            fontFamily: fonts.bold,
          }}>
          No Results Found
        </Text>
        {/* <Text style={{marginTop: moderateScale(4), fontSize: textScale(12)}}>
          View your delivered shipments
        </Text>
        <TouchableOpacity onPress={() => null}>
          <View style={[styles.verifyView,{marginTop:16}]}>
            <Text style={styles.verifyText}>
              {'Create Return'}
            </Text>
          </View>
        </TouchableOpacity> */}
      </View>
    );
  };

  getSearchResults = (text) => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(`Get_ProductByTag?tag=${text}&buyer_id=${user.Buyer_Id}`, null)
      .then((res) => {
        console.log('search results res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({list: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  getSelectedCategoryProducts = (catId) => {
    console.log('catId : ', catId);
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(
      `Get_ProductByCatId?CatId=${catId}&buyer_id=${user.Buyer_Id}`,
      null,
    )
      .then((res) => {
        console.log('category products res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({list: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  getSupplierProducts = (sellerId) => {
    console.log('supplierId : ', sellerId);
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(
      `Get_ProductBySellerId?SellerId=${sellerId}&buyer_id=${user.Buyer_Id}`,
      null,
    )
      .then((res) => {
        console.log('supplier products res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({list: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  getAdProducts = (prod_ids) => {
    console.log('prod_ids : ', prod_ids);
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(
      `Get_Slider_Products?prod_ids=${prod_ids}&buyer_id=${user.Buyer_Id}`,
      null,
    )
      .then((res) => {
        console.log('ad products res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({list: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  getAllViewAllProducts = (productType) => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(`Get_All_Products_From_Home?buyer_id=${user.Buyer_Id}&product_type=${productType}`, null)
      .then((res) => {
        console.log('latest prod res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({list: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  getAllLatestProducts = () => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(`Get_All_Latest_Products?buyer_id=${user.Buyer_Id}`, null)
      .then((res) => {
        console.log('latest prod res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({list: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  getAllFeaturedProducts = () => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(`Get_All_Featured_Products?buyer_id=${user.Buyer_Id}`, null)
      .then((res) => {
        console.log('featured prod res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({list: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  getAllTopSellingProducts = () => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    Request.get(`Get_All_Top_Selling_Products?buyer_id=${user.Buyer_Id}`, null)
      .then((res) => {
        console.log('topSelling prod res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          this.setState({list: res.data});
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  render() {
    const {data} = this.props.route.params;
    const {list} = this.state;
    const {user} = this.props;
    return (
      <View style={{flex: 1}}>
        <StatusBar barStyle="dark-content" hidden={false} />
        <View style={styles.container}>
          {/* <Toast ref="toast"/> */}
          <View style={styles.menuHeader}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 7}}
              colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                height: 64,
                // borderRadius: moderateScale(4),
              }}
            />
            <View style={styles.subMenuHeader}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image
                  source={constants.images.backArrowBlack}
                  style={{
                    height: moderateScale(24),
                    width: moderateScale(24),
                    tintColor: constants.colors.white,
                  }}></Image>
              </TouchableOpacity>

              <View style={styles.searchEditTextView}>
                <View style={styles.smallMobileImageView}>
                  <Image
                    source={constants.images.search}
                    style={{height: 16, width: 16}}></Image>
                </View>
                <TextInput
                  style={styles.mobileEditText}
                  placeholder="Search"
                  value={data.Product_Name}
                  editable={false}
                  placeholderTextColor={constants.colors.lightBlack}
                  onChangeText={(text) =>
                    text.length > 0
                      ? this.onSearchItem(text)
                      : this.setState({opacity: 1, searchList: []})
                  }></TextInput>
              </View>
              <TouchableOpacity
                style={{position: 'absolute', right: 0}}
                onPress={() =>
                  this.props.navigation.navigate('YourCartScreen')
                }>
                <Image
                  source={constants.images.cart}
                  style={{
                    marginStart: moderateScale(16),
                    height: moderateScale(24),
                    width: moderateScale(24),
                    tintColor:constants.colors.white
                  }}></Image>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              height: moderateScale(36),
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text
              style={{
                fontSize: textScale(12),
                fontFamily: fonts.bold,
                color: constants.colors.black,
                paddingVertical: moderateScale(8),
                marginHorizontal: moderateScale(16),
              }}>
              {`${list && list.length} Items`}
            </Text>
            {/* <View style={{flexDirection: 'row', marginEnd: moderateScale(16)}}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  style={{height: moderateScale(12), width: moderateScale(12)}}
                  source={constants.images.sort}
                />
                <Text style={{marginStart: moderateScale(4)}}>Sort</Text>
              </View>

              <View
                style={{
                  height: moderateScale(32),
                  width: moderateScale(1),
                  marginHorizontal: moderateScale(8),
                  backgroundColor: constants.colors.lightGray,
                  alignSelf: 'center',
                }}></View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}>
                <Image
                  style={{height: moderateScale(12), width: moderateScale(12)}}
                  source={constants.images.filter}
                />
                <Text style={{marginStart: moderateScale(4)}}>Filter</Text>
              </View>
            </View> */}
          </View>
          {this.itemSeprator()}
          <FlatList
            data={list}
            ItemSeparatorComponent={this.itemSeprator}
            ListEmptyComponent={this.renderEmptyListComponent}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  width: '100%',
                  marginTop: moderateScale(16),
                  marginHorizontal: moderateScale(16),
                }}
                onPress={() => {
                  this.props.navigation.push('ProductDetailsScreen', {
                    id:
                      data.catId === undefined
                        ? item.Product_Id === undefined
                          ? item.ProductId
                          : item.Product_Id
                        : item.ProductId,
                    from: data.from,
                    refreshData:
                      data.type === 'cart'
                        ? this.props.route.params.refreshData
                        : null,
                  });
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    source={{
                      uri: item.Product_Image,
                    }}
                    style={{
                      width: '20%',
                      height: moderateScale(80),
                      borderRadius: moderateScale(8),
                    }}
                    resizeMode={'contain'}
                  />
                  <View
                    style={{marginHorizontal: moderateScale(8), width: '60%'}}>
                    <Text
                      style={{
                        fontSize: textScale(14),
                        fontFamily: fonts.bold,
                        color: constants.colors.black,
                      }}>
                      {item.Product_Name !== undefined
                        ? item.Product_Name.length > 60
                          ? `${item.Product_Name.substring(0, 60)}...`
                          : item.Product_Name
                        : ''}{' '}
                    </Text>
                    <Text
                      style={{
                        fontSize: textScale(12),
                        fontFamily: fonts.regular,
                        color: constants.colors.black,
                      }}>
                      {item.Product_Description &&
                      item.Product_Description.length > 60
                        ? `${item.Product_Description.substring(0, 60)}...`
                        : item.Product_Description}
                    </Text>

                    <Text
                      style={{
                        fontSize: textScale(12),
                        fontFamily: fonts.bold,
                        color: constants.colors.black,
                        marginTop: moderateScale(8),
                      }}>
                      {item.MinQty !== undefined
                        ? `Min Qty : ${item.MinQty} pcs`
                        : ''}
                    </Text>
                    {user.KYC === true ? (
                      <Text
                        style={{
                          fontSize: textScale(12),
                          fontFamily: fonts.bold,
                        }}>
                        {item.Selling_Price !== undefined
                          ? `${constants.strings.rupee_symbol} ${parseFloat(
                              item.Selling_Price,
                            ).toFixed(2)}`
                          : ''}
                      </Text>
                    ) : null}
                  </View>
                </View>
                <View style={{marginTop: moderateScale(16)}}></View>
              </TouchableOpacity>
            )}
          />
        </View>
        {user.KYC === false ? (
          <TouchableOpacity
            style={{
              marginVertical: moderateScale(8),
              marginHorizontal: moderateScale(16),
              backgroundColor: constants.colors.pink,
              width: '90%',
              height: moderateScale(80),
              position: 'absolute',
              flexDirection: 'row',
              borderRadius: moderateScale(8),
              bottom: 0,
            }}
            onPress={() => this.props.navigation.navigate('KYCStepOneScreen')}>
            <Image
              source={constants.images.policy}
              style={{
                height: moderateScale(50),
                width: moderateScale(50),
                margin: moderateScale(16),
                tintColor: constants.colors.white,
              }}
            />
            <View
              style={{
                marginVertical: moderateScale(16),
                // marginHorizontal: moderateScale(4),
                justifyContent: 'center',
                // alignItems: 'center',
              }}>
              <Text
                style={{
                  color: constants.colors.white,
                  fontSize: textScale(16),
                  fontFamily: fonts.bold,
                }}>
                Want to see Prices ?
              </Text>
              <Text
                style={{
                  color: constants.colors.white,
                  fontSize: textScale(14),
                  marginTop: moderateScale(8),
                  fontFamily: fonts.regular,
                }}>
                Complete your Shop KYC
              </Text>
            </View>
            <Image
              source={constants.images.backArrowBlack}
              style={{
                height: moderateScale(20),
                width: moderateScale(20),
                margin: moderateScale(16),
                rotation: 180,
                position: 'absolute',
                right: 0,
                alignSelf: 'center',
                tintColor: constants.colors.white,
              }}
            />
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth, category}) => ({
  user: auth.userData,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchResultsScreen);
