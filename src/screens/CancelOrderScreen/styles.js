
import { RFValue } from 'react-native-responsive-fontsize';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
// import { fonts } from '../../assets';
import { moderateScaleVertical, textScale } from '../../utilities/responsiveSize';
import {Dimensions,StyleSheet} from 'react-native'
import constants from '../../utilities/index';


const { width, height } = Dimensions.get('window');



const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    mobileNuberVerificationText: {
        fontSize: RFValue(20),
        // fontFamily: fonts.bold,
        color: constants.colors.black,
        marginTop:24
    },

    enterMobileText: {
        fontSize: RFValue(14),
        // fontFamily: fonts.bold,
        color: constants.colors.lightBlack,
        textAlign : 'center',
        width:'60%',
        marginTop:8
        
    },

    mobileEditTextView:{
        marginTop:moderateScale(24),
        borderRadius:4,
        borderWidth:1,
        borderColor:constants.colors.lightBlack,
        width:'70%',
        padding:moderateScale(2),
        flexDirection:'row',
        alignItems:'center',
    
    },

    ccText: {
        fontSize: RFValue(16),
        marginStart:moderateScale(8),
        // fontFamily: fonts.bold,
        color: constants.colors.black,
    },

    mobileEditText: {
        fontSize: RFValue(16),
        marginStart:moderateScale(8),
        paddingEnd:moderateScale(8),
        width:'85%',
        // fontFamily: fonts.bold,
        color: constants.colors.black,
    },



    smallMobileImageView: {
        alignItems: 'center',
        justifyContent: 'center',
        marginStart: moderateScale(16)

    },


    mobileImageView: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: moderateScaleVertical(36)
    },
    verificationText: {
        alignItems: 'center',
        justifyContent: 'center',
        height: moderateScaleVertical(40),
        marginTop: moderateScale(22)
    },

    verificationText: {
        alignItems: 'center',
        justifyContent: 'center',
        height: moderateScaleVertical(40),
        marginTop: moderateScale(22)
    },
    otpTxt: {
        // fontFamily: fonts.regular,
        fontSize: (14)
    },
    otpInputView: {
        marginTop: moderateScale(58), height: 58, width: null
    },
    inputWrapper1: {
        paddingVertical: moderateScale(24),
        paddingHorizontal: 20,
    },
    otpExpire: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: moderateScale(16),
        flexDirection: 'row'
    },
    otpExpiresText: {
        // fontFamily: fonts.regular,
        fontSize: moderateScale(14),
        color: constants.colors.lightBlack
    },
    verifyView: {
        marginTop: moderateScaleVertical(34),
        // marginLeft: moderateScale(88),
        // marginRight: moderateScale(88),
        height: moderateScale(50),
        width:'70%',
        backgroundColor: constants.colors.pink,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf:'center',
        borderRadius: moderateScale(4)
    },
    verifyText: {
        // fontFamily: fonts.bold,
        fontSize: RFValue(16),
        fontWeight:'bold',
        color: constants.colors.white
    },
    resendOtpView: {
        height: moderateScaleVertical(24),
        marginTop: moderateScale(12),
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'center',
        width: null
    },
    didnotreceiveText: {
        fontSize: RFValue(13),
        // fontFamily: fonts.regular,
        color: constants.colors.black
    },
    resendText: {
        fontSize: RFValue(13),
        // fontFamily: fonts.bold,
        color: constants.colors.pink
    }


});

export default styles