import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  TextInput,
  Alert
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import {ScrollView} from 'react-native-gesture-handler';

class CancelOrderScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reason: '',
    };
  }

  customAlert = (title, message) => {
    Alert.alert(
      title,
      message,
      [
        {text: 'CANCEL'},
        {
          text: 'OK',
          onPress: () => {
            let {orderId} = this.props.route.params;
            this.cancelReturn(orderId);
          },
        },
      ],
      {
        cancelable: true,
      },
    );
  };

  cancelReturn = (orderId) => {
    let {actions, user} = this.props;
    actions.showLoader(true);
    let data = {};
    data.Order_Id = orderId;
    Request.post(`Cancel_Order`, data)
      .then((res) => {
        console.log('cancel return res : ', res);
        actions.hideLoader(false);
        if (res.result.status === 'OK') {
          let {updateData} = this.props.route.params;
          updateData();
          this.props.navigation.goBack();
        } else {
          alert('Server Error');
          console.log('api error : ', res);
        }
      })
      .catch((err) => {
        actions.hideLoader(false);
        MyToast.showToast(err.message());
      });
  };

  handleChange = (value, name) => {
    this.setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  cancelOrderClick = () => {
    let {reason} = this.state;
    if (reason === '') {
      alert('Please enter your cancellation reason');
    } else {
      this.customAlert(
        'Cancel Order',
        'Are you sure you want to Cancel this order?',
      );
    }
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar barStyle="light-content" hidden={false} />
        <ScrollView>
          <View style={styles.container}>
            {/* <Toast ref="toast"/> */}

            <HeaderComp
              leftText={'Cancel Order'}
              leftTextStyle={{color: constants.colors.white}}
              imageLeft={constants.images.backArrowBlack}
              leftImageTint={constants.colors.white}
              // imageRight={constants.images.cart}
              headerContainerStyle={{backgroundColor: constants.colors.pink}}
              leftImgHandler={() => this.props.navigation.goBack()}
            />

            <View
              style={[
                styles.mobileEditTextView,
                {
                  marginTop: moderateScale(8),
                  alignItems: 'flex-start',
                  height: moderateScale(200),
                  marginHorizontal: moderateScale(20),
                  width: '90%',
                  //   justifyContent:'center'
                },
              ]}>
              <TextInput
                style={[styles.mobileEditText, {justifyContent: 'flex-start'}]}
                placeholder="Enter your cancellation reason"
                multiline={true}
                placeholderTextColor={constants.colors.lightBlack}
                onChangeText={(text) =>
                  this.handleChange(text, 'reason')
                }></TextInput>
            </View>
          </View>
          <TouchableOpacity onPress={() => this.cancelOrderClick()}>
            <View style={styles.verifyView}>
              <Text style={styles.verifyText}>{'Submit'}</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth}) => ({
  user: auth.userData,
});

export default connect(mapStateToProps, mapDispatchToProps)(CancelOrderScreen);
