import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StatusBar,
  FlatList,
  Alert,
  TextInput,
  Picker,
  Switch,
} from 'react-native';
import styles from './styles';
import HeaderComp from '../../components/Header';
import constants from '../../utilities/index';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {textScale} from '../../utilities/responsiveSize';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Request} from '../../services/index';
import actions from '../../redux/actions';
import * as MyToast from '../../components/Toast';
import {fonts} from '../../assets';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from 'react-native-raw-bottom-sheet';
const {height, width} = Dimensions.get('window');

class DeliveryAddressScreen extends Component {
  constructor(props) {
    super(props);
    this.beforeGstTotal = 0;
    (this.totalGstPrice = 0), (this.totalDeliveryCharges = 0);
    this.totalQuantity = 0;
    this.state = {
      products: [],
      buyer_gst: '',
      isEnabled: false,
      isVisible: false,
      selectedPaymentMode: 'COD',
      selectedPaymentModeId: '1',
    };
  }

  componentDidMount() {
    const {cartData, totalAmount} = this.props.route.params;
    const {products} = this.state;
    this.totalGstPrice = parseFloat(
      (totalAmount * cartData[0].product_detail.gst) / 100,
    );
    this.beforeGstTotal = totalAmount;
    this.totalDeliveryCharges = cartData[0].product_detail.delivery_amount;

    for (let i = 0; i < cartData.length; i++) {
      this.totalQuantity += cartData[i].Qty;
      products.push({
        Product_Id: cartData[i].ProductId,
        Price_Per_Item: parseFloat(
          cartData[i].totalPrice / cartData[i].Qty,
        ).toFixed(2),
        Discount: parseFloat(cartData[i].Discount).toFixed(2),
        Qty: cartData[i].Qty,
      });
    }
    this.setState({
      products,
      selectedPaymentMode: cartData[0].product_detail.paymentoptions[0].value,
      selectedPaymentModeId: cartData[0].product_detail.paymentoptions[0].id,
    });
  }

  refreshData = (paymentData) => {
    console.log('paymentData : ',paymentData)
    this.placeOrder(paymentData)
  }

  placeOrder = (paymentData) => {
    const {user, actions} = this.props;
    const {
      cartData,
      totalAmount,
      shippingAddress,
      pincode,
      email,
      city,
      state,
    } = this.props.route.params;
    const {products, selectedPaymentMode} = this.state;
    if (selectedPaymentMode === 'COD' || paymentData!==undefined) {
      if (cartData.length > 0) {
        actions.showLoader(true);
        let data = {};
        if(paymentData!==undefined){
          data['paymentId'] = paymentData.paymentId
          data['orderId'] = paymentData.orderId
        }
        data['Seller_Id'] = cartData[0].product_detail.Seller_Id;
        data['Buyer_Id'] = user.Buyer_Id;
        data['Buyer_Mobile_No'] = user.Mobile_Number;
        data['Buyer_Name'] = user.Buyer_Name;
        data['Buyer_Firm_Name'] = user.Firm_Name;
        data['Buyer_Pincode'] = pincode;
        data['Buyer_Billing_Address'] = user.BillingAddress;
        data['Buyer_Delivery_Address'] = shippingAddress;
        data['Buyer_Email'] = email;
        data['Buyer_City'] = city;
        data['Buyer_State'] = state;
        data['Sub_Total_Price'] = parseFloat(this.beforeGstTotal).toFixed(2);
        data['Total_Price'] = (
          parseFloat(totalAmount) + parseFloat(this.totalGstPrice)
        ).toFixed(2);
        data['Total_Qty'] = parseFloat(this.totalQuantity).toFixed(2);
        data['Total_GST'] = parseFloat(this.totalGstPrice).toFixed(2);
        data['Delivery_Amount'] = parseFloat(this.totalDeliveryCharges).toFixed(
          2,
        );
        data['products'] = products;
        if (this.state.isEnabled) {
          data['Buyer_GST'] = this.state.buyer_gst;
        }
        console.log('place order Req : ', data);
        Request.post('Save_Order', data)
          .then((res) => {
            console.log('update profile res : ', res);
            actions.hideLoader(false);
            if (res.result.status === 'OK') {
              this.customAlert('Congratulations!', res.data, undefined);
            } else {
              alert('Server Error');
              console.log('api error : ', res);
            }
          })
          .catch((err) => {
            actions.hideLoader(false);
            MyToast.showToast(err.message());
          });
      }
    } else {
      let uniqueId = new Date().getTime()
      let url = `${cartData[0].product_detail.payment_url}?product_id=${
        cartData[0].ProductId
      }&buyer_id=${user.Buyer_Id}&amount=${(
        parseInt(totalAmount) +
        parseInt(this.totalGstPrice) +
        parseInt(this.totalDeliveryCharges)
      )}&unquie_id=${uniqueId}`;
      this.props.navigation.navigate('WebViewScreen', {
        title: 'Aayme Payment Gateway',
        type: 11,
        paymentUrl: url,
        uniqueId:uniqueId,
        refreshData:this.refreshData
      });
    }
  };

  onSwitchChanged = (value) => {
    this.setState({isEnabled: value});
  };

  handleChange = (value, name) => {
    this.setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  itemSeprator = () => {
    return (
      <View
        style={{
          height: moderateScale(0.5),
          width: '100%',
          backgroundColor: constants.colors.gray,
        }}></View>
    );
  };

  renderEmptyListComponent = () => {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: height / 2 + 100,
        }}>
        <Image
          style={{height: moderateScale(100), width: moderateScale(100)}}
          source={constants.images.return_box}></Image>
        <Text
          style={{
            marginTop: moderateScale(16),
            fontSize: textScale(14),
            fontFamily: fonts.bold,
          }}>
          No Items Added Yet
        </Text>
        {/* <Text style={{marginTop: moderateScale(4), fontSize: textScale(12)}}>
          View your delivered shipments
        </Text>
        <TouchableOpacity onPress={() => null}>
          <View style={[styles.verifyView,{marginTop:16}]}>
            <Text style={styles.verifyText}>
              {'Create Return'}
            </Text>
          </View>
        </TouchableOpacity> */}
      </View>
    );
  };

  itemSeprator = () => {
    return (
      <View
        style={{
          height: moderateScale(2),
          marginTop: moderateScale(16),
          marginBottom: moderateScale(16),
          width: '100%',
          backgroundColor: constants.colors.separatorColor,
        }}></View>
    );
  };

  renderFooterComponent = () => {
    const {user} = this.props;
    const {
      shippingAddress,
      billingAddress,
      pincode,
      totalAmount,
      email,
      city,
      state,
      cartData,
    } = this.props.route.params;
    return (
      <View style={{margin: moderateScale(16)}}>
        <View
          style={{
            // alignItems: 'center',
            // justifyContent: 'center',
            backgroundColor: constants.colors.white,
            borderColor: constants.colors.btnGray,
            padding: moderateScale(8),
            borderWidth: moderateScale(1),
            // height: height / 2 + 100,
          }}>
          <Text
            style={{
              fontSize: textScale(14),
              fontFamily: fonts.regular,
            }}>
            Deliver To :
          </Text>
          <Text
            style={{
              marginTop: moderateScale(4),
              fontSize: textScale(14),
              fontFamily: fonts.bold,
            }}>
            {user.Buyer_Name}
          </Text>
          <Text
            style={{
              fontSize: textScale(14),
              fontFamily: fonts.regular,
            }}>
            {`${user.Firm_Name}, ${billingAddress},${shippingAddress}, ${city}, ${state}, Pincode : ${pincode}`}
          </Text>
        </View>
        <Text
          style={{
            color: constants.colors.btnGray,
            fontSize: moderateScale(12),
            marginTop: moderateScale(16),
            fontFamily: fonts.bold,
          }}>
          PAYMENT
        </Text>

        <View
          style={{
            marginTop: moderateScale(4),
            borderWidth: 2,
            borderColor: constants.colors.separatorColor,
            paddingBottom: moderateScale(8),
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: moderateScale(8),
              paddingTop: moderateScale(8),
            }}>
            <Text style={{textAlign: 'left', fontFamily: fonts.regular}}>
              {this.state.selectedPaymentMode}
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.handlePaymentModeBottomSheet();
              }}>
              <Text
                style={{
                  textAlign: 'left',
                  fontFamily: fonts.bold,
                  color: constants.colors.pink,
                }}>
                Change
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <Text
          style={{
            color: constants.colors.btnGray,
            fontSize: moderateScale(12),
            marginTop: moderateScale(16),
            fontFamily: fonts.bold,
          }}>
          GST INFORMATION
        </Text>

        <View
          style={{
            marginTop: moderateScale(4),
            borderWidth: 2,
            borderColor: constants.colors.separatorColor,
            paddingBottom: moderateScale(8),
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: moderateScale(8),
              paddingTop: moderateScale(8),
            }}>
            <Text style={{textAlign: 'left', fontFamily: fonts.regular}}>
              Do you want a GST Invoice?
            </Text>
            <Switch
              trackColor={{false: '#767577', true: '#C9C9C9'}}
              thumbColor={
                this.state.isEnabled ? constants.colors.pink : '#C9C9C9'
              }
              ios_backgroundColor={constants.colors.pink}
              onValueChange={(value) => this.onSwitchChanged(value)}
              value={this.state.isEnabled}
            />
          </View>
          {this.state.isEnabled && (
            <View style={styles.mobileEditTextView}>
              {/* <View style={styles.smallMobileImageView}>
              <Image
                source={constants.images.user}
                style={{
                  height: 20,
                  width: 20,
                  tintColor: constants.colors.pink,
                }}></Image>
            </View> */}
              <TextInput
                style={styles.mobileEditText}
                value={this.state.buyer_gst}
                placeholder="GST Number"
                autoCapitalize={'characters'}
                placeholderTextColor={constants.colors.lightBlack}
                onChangeText={(text) =>
                  this.handleChange(text, 'buyer_gst')
                }></TextInput>
            </View>
          )}
        </View>

        <Text
          style={{
            color: constants.colors.btnGray,
            fontSize: moderateScale(12),
            marginTop: moderateScale(16),
            fontFamily: fonts.bold,
          }}>
          COST SUMMARY
        </Text>

        <View
          style={{
            marginTop: moderateScale(4),
            borderWidth: 2,
            borderColor: constants.colors.separatorColor,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: moderateScale(8),
              paddingTop: moderateScale(8),
            }}>
            <Text style={{textAlign: 'left', fontFamily: fonts.regular}}>
              Sub Total
            </Text>
            <Text style={{fontFamily: fonts.regular}}>{`${
              constants.strings.rupee_symbol
            }${parseFloat(this.beforeGstTotal).toFixed(2)}`}</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: moderateScale(8),
              paddingTop: moderateScale(4),
            }}>
            <Text style={{textAlign: 'left', fontFamily: fonts.bold}}>GST</Text>
            <Text style={{fontFamily: fonts.bold}}>{`${
              constants.strings.rupee_symbol
            }${parseFloat(this.totalGstPrice).toFixed(2)}`}</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: moderateScale(8),
              paddingTop: moderateScale(4),
            }}>
            <Text style={{textAlign: 'left', fontFamily: fonts.regular}}>
              Delivery Charges
            </Text>
            <Text style={{fontFamily: fonts.regular}}>{`${
              constants.strings.rupee_symbol
            }${parseFloat(this.totalDeliveryCharges).toFixed(2)}`}</Text>
          </View>
          {this.itemSeprator()}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: moderateScale(8),
              paddingBottom: moderateScale(8),
            }}>
            <Text style={{textAlign: 'left', fontFamily: fonts.bold}}>
              Total
            </Text>
            <Text style={{fontFamily: fonts.bold}}>{`${
              constants.strings.rupee_symbol
            }${parseFloat(
              this.beforeGstTotal +
                this.totalGstPrice +
                this.totalDeliveryCharges,
            ).toFixed(2)}`}</Text>
          </View>
        </View>
      </View>
    );
  };

  customAlert = (title, message, item) => {
    Alert.alert(
      title,
      message,
      [
        item !== undefined ? {text: 'Cancel'} : null,

        {
          text: 'OK',
          onPress: () => {
            if (item !== undefined && item !== null) {
              this.deleteItemFromCart(item);
            } else {
              if (item === undefined) {
                this.props.navigation.reset({
                  index: 0,
                  routes: [{name: 'HomeTabFlow'}],
                });
              } else {
                this.placeOrder();
              }
            }
          },
        },
      ],
      {
        cancelable: true,
      },
    );
  };

  handlePaymentModeBottomSheet = () => {
    if (this.state.isVisible) {
      this.appDefaultSheet.close();
      this.setState({isVisible: false});
    } else {
      this.setState({isVisible: true});
      this.appDefaultSheet.open();
    }
  };



  render() {
    const {cartData, totalAmount} = this.props.route.params;

    return (
      <View style={{flex: 1}}>
        <StatusBar barStyle="dark-content" hidden={false} />
        <HeaderComp
          leftText={'Place Your Order'}
          leftTextStyle={{color: constants.colors.white}}
          imageLeft={constants.images.backArrowBlack}
          leftImageTint={constants.colors.white}
          headerContainerStyle={{backgroundColor: constants.colors.pink}}
          leftImgHandler={() => this.props.navigation.goBack()}
        />
        <View style={{flex: 0.9}}>
          <FlatList
            data={cartData}
            renderItem={({item, index}) => {
              return (
                <View
                  style={{
                    width: width,
                    flexDirection: 'row',
                    marginHorizontal: moderateScale(16),
                    paddingVertical: moderateScale(16),
                  }}>
                  <Image
                    style={{
                      height: moderateScale(60),
                      width: moderateScale(60),
                    }}
                    source={{
                      uri: item.product_detail.Product_Images[0].ImagePath,
                    }}
                    resizeMode="contain"
                  />
                  <View style={{width: '75%', marginStart: moderateScale(16)}}>
                    <Text
                      style={{
                        width: '100%',
                        fontFamily: fonts.bold,
                        fontSize: textScale(16),
                      }}>{`${item.Product_Name}`}</Text>
                    <Text
                      style={{
                        width: '95%',
                        fontFamily: fonts.regular,
                      }}>{`${
                      item.Product_Description &&
                      item.Product_Description.length > 60
                        ? `${item.Product_Description.substring(0, 60)}...`
                        : item.Product_Description
                    }`}</Text>
                    <Text
                      style={{
                        fontFamily: fonts.regular,
                      }}>{`Quantity : ${item.Qty}`}</Text>
                    <Text
                      style={{
                        fontSize: textScale(16),
                        fontFamily: fonts.bold,
                        marginTop: moderateScale(8),
                      }}>{`${constants.strings.rupee_symbol}${parseFloat(
                      item.totalPrice,
                    ).toFixed(2)}`}</Text>
                  </View>
                </View>
              );
            }}
            ListEmptyComponent={this.renderEmptyListComponent()}
            ItemSeparatorComponent={this.itemSeprator}
            ListFooterComponent={this.renderFooterComponent}
          />
        </View>

        <View style={{flex: 0.1, marginHorizontal: moderateScale(16)}}>
          {/* <View
            style={{
              position: 'absolute',
              bottom: 0,
              marginBottom: moderateScale(56),
            }}>
            <View
              style={{
                backgroundColor: constants.colors.separatorColor,
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderColor: constants.colors.separatorColor,
                borderWidth: moderateScale(1),

              }}>
              <Text
                style={{
                  padding: moderateScale(8),
                  backgroundColor: constants.colors.white,
                  width: width / 2 - 20,
                  fontWeight: 'bold',
                }}>
                {'Delivery Charges'}
              </Text>
              <Text
                style={{
                  padding: moderateScale(8),
                  backgroundColor: constants.colors.white,
                  width: width / 2 - 20,
                  fontWeight: 'bold',
                  fontSize: textScale(18),
                  textAlign: 'right',
                }}>
                {`${constants.strings.rupee_symbol}${0}`}
              </Text>
            </View>
            <View
              style={{
                backgroundColor: constants.colors.separatorColor,
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderColor: constants.colors.separatorColor,
                borderWidth: moderateScale(1),

                // marginTop: moderateScale(2),
              }}>
              <Text
                style={{
                  padding: moderateScale(8),
                  backgroundColor: constants.colors.white,
                  width: width / 2 - 20,
                  fontWeight: 'bold',
                }}>
                {'Total Amount'}
              </Text>
              <Text
                style={{
                  padding: moderateScale(8),
                  backgroundColor: constants.colors.white,
                  width: width / 2 - 20,
                  fontWeight: 'bold',
                  fontSize: textScale(18),
                  textAlign: 'right',
                }}>
                {`${constants.strings.rupee_symbol}${totalAmount}`}
              </Text>
            </View>
          </View> */}

          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              position: 'absolute',
              bottom: 0,
              marginBottom: moderateScale(12),
            }}>
            {/* <TouchableOpacity
              style={{width:'50%',alignSelf: 'center'}}
              onPress={() =>{
                const{cartData} = this.state
                let data = {}
                data.type = 4;
                data.seller_id = cartData&&cartData[0].product_detail.Seller_Id ;
                data.Product_Name = cartData&&cartData[0].product_detail.Seller_Name
                this.props.navigation.navigate('SearchResultsScreen', {
                  data: data,
                })

              }}>
              <Text
                style={{
                  // width: '68%',
                  padding: moderateScale(12),
                  // marginStart: moderateScale(8),
                  borderRadius: moderateScale(4),
                  fontWeight: 'bold',
                  textAlign: 'center',
                  fontSize: textScale(16),
                  color: constants.colors.white,
                  backgroundColor: constants.colors.pink,
                }}>
                Add New Item
              </Text>
            </TouchableOpacity> */}
            <TouchableOpacity
              style={{width: '100%', alignSelf: 'center'}}
              onPress={() => {
                if (
                  this.state.isEnabled &&
                  this.state.buyer_gst.trim() !== ''
                ) {
                  if (this.state.selectedPaymentMode === 'COD') {
                    this.customAlert(
                      'Confirmation!',
                      'Are you sure you want to place your order?',
                      null,
                    );
                  } else {
                    this.placeOrder();
                  }
                } else {
                  if (!this.state.isEnabled) {
                    if (this.state.selectedPaymentMode === 'COD') {
                      this.customAlert(
                        'Confirmation!',
                        'Are you sure you want to place your order?',
                        null,
                      );
                    } else {
                      this.placeOrder();
                    }
                  } else {
                    alert('Please enter GST Number');
                  }
                }
              }}>
              <LinearGradient
                start={{x: 0, y: 0}}
                end={{x: 1, y: 7}}
                colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  right: 0,
                  height: 48,
                  borderRadius: moderateScale(4),
                }}
              />
              <Text
                style={{
                  // width: '68%',
                  padding: moderateScale(12),
                  // marginStart: moderateScale(8),
                  borderRadius: moderateScale(4),
                  fontFamily: fonts.bold,
                  textAlign: 'center',
                  fontSize: textScale(16),
                  color: constants.colors.white,
                  // backgroundColor: constants.colors.pink,
                }}>
                {this.state.selectedPaymentMode === 'COD'
                  ? 'Place Your Order'
                  : 'Continue'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <RBSheet
          ref={(refs) => (this.appDefaultSheet = refs)}
          animationType="fade"
          closeOnDragDown={true}
          // height={450}
          openDuration={250}
          customStyles={{
            wrapper: {
              backgroundColor: 'rgba(60,60,60,0.9)',
              backfaceVisibility: 'hidden',
            },
            container: {
              backgroundColor: 'transparent',
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              height: 308,
            },
            draggableIcon: {
              backgroundColor: 'transparent',
            },
          }}
          // onClose={() => resetAll()}
        >
          {this.state.isVisible && (
            <View
              style={{
                paddingTop: 20,
                paddingBottom: 40,
                flex: 1,
                // position: 'absolute',
                // bottom: 0,
                // left: 0,
                // right: 0,
                backgroundColor: 'white',
              }}>
              <TouchableOpacity
                style={{position: 'absolute', top: 0, right: 0}}
                onPress={() => {
                  this.handlePaymentModeBottomSheet();
                }}>
                <Text
                  style={{
                    alignSelf: 'flex-end',
                    textAlign: 'center',
                    fontFamily: fonts.bold,
                    paddingHorizontal: 24,
                    paddingVertical: 16,
                  }}>
                  X
                </Text>
              </TouchableOpacity>

              <Text
                style={{
                  alignSelf: 'center',
                  textAlign: 'center',
                  fontFamily: fonts.bold,
                }}>
                select Payment Mode
              </Text>

              <FlatList
                data={cartData[0].product_detail.paymentoptions}
                renderItem={({item, index}) => (
                  <>
                    <TouchableOpacity
                      style={{
                        alignItems: 'center',
                        marginTop: 16,
                        marginBottom: 16,
                      }}
                      onPress={() => {
                        this.setState({
                          selectedPaymentMode: item.value,
                          selectedPaymentModeId: item.id,
                        });
                        this.handlePaymentModeBottomSheet();
                      }}>
                      <Text
                        style={{
                          fontFamily:
                            Platform.OS === 'ios'
                              ? 'SF Pro Display'
                              : 'SF-Pro-Display-Regular',
                          fontWeight: 'normal',
                          fontStyle: 'normal',
                          fontSize: 14,
                          color: '#3C3C3C',
                          lineHeight: 18,
                        }}>
                        {item.value}
                      </Text>
                    </TouchableOpacity>
                    <View
                      style={{
                        height: 1,
                        width: '100%',
                        backgroundColor: '#3C3C3C',
                        opacity: 0.2,
                      }}></View>
                  </>
                )}
              />
            </View>
          )}
        </RBSheet>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const mapStateToProps = ({auth, category}) => ({
  user: auth.userData,
  categoryClicked: category.setCategoryShown,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DeliveryAddressScreen);
