import React from 'react';
import {Image} from 'react-native';
import {useDispatch, shallowEqual, useSelector} from 'react-redux';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Indicator from '../components/Indicator';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import LoginScreen from '../screens/LoginScreen/LoginScreen';
import OtpVerificationScreen from '../screens/OtpVerificationScreen/OtpVerificationScreen';
import SignupScreen from '../screens/SignupScreen/SignupScreen';
import HomeScreen from '../screens/HomeScreen/HomeScreen';
import SearchScreen from '../screens/SearchScreen/SearchScreen';
import DeliveryAddressScreen from '../screens/DeliveryAddressScreen/DeliveryAddressScreen';
import MyAccountScreen from '../screens/MyAccountScreen/MyAccountScreen';
import YourCartScreen from '../screens/YourCartScreen/YourCartScreen';
import YourOrdersScreen from '../screens/YourOrdersScreen/YourOrdersScreen';
import YourReturnsScreen from '../screens/YourReturnsScreen/YourReturnsScreens';
import KYCStepOneScreen from '../screens/KYCScreens/KYCStepOneScreen';
import KYCStepTwoScreen from '../screens/KYCScreens/KYCStepTwoScreen';
import WebViewScreen from '../screens/WebViewScreen/WebViewScreen';
import SearchResultsScreen from '../screens/SearchResultsScreen/SearchResultsScreen';
import ProductDetailsScreen from '../screens/ProductDetailsScreen/ProductDetailsScreen';
import CategoriesScreen from '../screens/CategoriesScreen/CategoriesScreen';
import ZoomImagesScreen from '../screens/ZoomImagesScreen/ZoomImagesScreen';
import EditProfileScreen from '../screens/EditProfileScreen/EditProfileScreen';
import CategoryExpandScreen from '../screens/CategoryExpandScreen/CategoryExpandScreen';
import CategoryNestedExpandScreen from '../screens/CategoryNestedExpandScreen/CategoryNestedExpandScreen';
import OrderDetailsScreen from '../screens/OrderDetailsScreen/OrderDetailsScreen';
import CancelOrderScreen from '../screens/CancelOrderScreen/CancelOrderScreen';

import SideMenu from '../screens/SideMenu/SideMenu';
import constants, {images} from '../utilities/index';
import {moderateScale} from 'react-native-size-matters';

// configuring navigation screens
const Stack = createStackNavigator();

// // configuring bottom tabs
const Tab = createBottomTabNavigator();

// creating sidemenu
const Drawer = createDrawerNavigator();

const UserAuthFlow = () => {
  const {user = null, t} = useSelector((state) => ({
    user: state.auth.userData,
  }));
  console.log('ttttt valueeeeee : ', t);
  console.log('current userrr:', user);

  if (user === null || user.Buyer_Id === 0) {
    return (
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen
          name="OtpVerificationScreen"
          component={OtpVerificationScreen}
        />
        <Stack.Screen name="SignupScreen" component={SignupScreen} />
      </Stack.Navigator>
    );
  } else {
    console.log('current user:', user.Buyer_id);
    if (
      (user.Buyer_Id !== undefined && user.Buyer_Id !== 0) ||
      (user.BuyerId !== undefined && user.BuyerId !== 0)
    ) {
      return (
        <Stack.Navigator>
          <Stack.Screen
            options={{headerShown: false}}
            name="HomeTabFlow"
            component={HomeTabFlow}
          />

          <Stack.Screen
            options={{headerShown: false}}
            name="UserAuthFlow"
            component={UserAuthFlow}
          />

          <Stack.Screen
            options={{headerShown: false}}
            name="KYCStepOneScreen"
            component={KYCStepOneScreen}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="KYCStepTwoScreen"
            component={KYCStepTwoScreen}
          />

          <Stack.Screen
            options={{headerShown: false}}
            name="WebViewScreen"
            component={WebViewScreen}
          />

          <Stack.Screen
            options={{headerShown: false}}
            name="SearchResultsScreen"
            component={SearchResultsScreen}
          />

          <Stack.Screen
            options={{headerShown: false}}
            name="ProductDetailsScreen"
            component={ProductDetailsScreen}
          />

          <Stack.Screen
            options={{headerShown: false}}
            name="ZoomImagesScreen"
            component={ZoomImagesScreen}
          />

          <Stack.Screen
            options={{headerShown: false}}
            name="EditProfileScreen"
            component={EditProfileScreen}
          />

          <Stack.Screen
            options={{headerShown: false}}
            name="YourCartScreen"
            component={YourCartScreen}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="CategoryExpandScreen"
            component={CategoryExpandScreen}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="CategoryNestedExpandScreen"
            component={CategoryNestedExpandScreen}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="DeliveryAddressScreen"
            component={DeliveryAddressScreen}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="OrderDetailsScreen"
            component={OrderDetailsScreen}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="CancelOrderScreen"
            component={CancelOrderScreen}
          />
        </Stack.Navigator>
      );
    } else {
      return (
        <Stack.Navigator screenOptions={{headerShown: false}}>
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen
            name="OtpVerificationScreen"
            component={OtpVerificationScreen}
          />
          <Stack.Screen name="SignupScreen" component={SignupScreen} />
        </Stack.Navigator>
      );
    }
  }
};

const HomeTabFlow = () => {
  return (
    <Tab.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="HomeScreen"
      tabBarOptions={{
        activeTintColor: constants.colors.pink,
        // inactiveTintColor:'#f87f11'
      }}>
      <Tab.Screen
        name="HomeScreen"
        component={DrawerNavigator}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      {/* <Tab.Screen
        name="SearchScreen"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({color, size}) => (
            <Image
              source={constants.images.search}
              style={{
                height: moderateScale(20),
                width: moderateScale(20),
                tintColor: color,
              }}
            />

            // <MaterialCommunityIcons name="filter" color={color} size={size} />
          ),
        }}
      /> */}
      <Tab.Screen
        name="Enquiry"
        component={WebViewScreen}
        options={{
          tabBarLabel: 'Enquiry',
          tabBarIcon: ({color, size}) => (
            <Image
              source={constants.images.enquiry}
              style={{
                // height: moderateScale(16),
                // width: moderateScale(16),
                tintColor: color,
              }}
            />
            // <MaterialCommunityIcons name="notifications-paused" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Notifications"
        component={WebViewScreen}
        options={{
          tabBarLabel: 'Notifications',
          tabBarIcon: ({color, size}) => (
            <Image
              source={constants.images.notification}
              style={{
                // height: moderateScale(16),
                // width: moderateScale(16),
                tintColor: color,
              }}
            />
            // <MaterialCommunityIcons name="notifications-paused" color={color} size={size} />
          ),
        }}
      />

      <Tab.Screen
        name="MyAccountsScreen"
        component={MyAccountScreen}
        options={{
          tabBarLabel: 'My Account',
          tabBarIcon: ({color, size}) => (
            <Image
              source={constants.images.user}
              style={{
                // height: moderateScale(24),
                // width: moderateScale(24),
                tintColor: color,
              }}
            />
            // <MaterialCommunityIcons name="notifications-paused" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      initialRouteName={'HomeScreen'}
      screenOptions={{headerShown: false}}
      drawerContent={(props) => <SideMenu {...props} />}>
      <Drawer.Screen name="HomeScreen" component={HomeScreen} />
      <Drawer.Screen name="YourOrdersScreen" component={YourOrdersScreen} />
      <Drawer.Screen name="YourReturnsScreen" component={YourReturnsScreen} />
      <Drawer.Screen name="CategoriesScreen" component={CategoriesScreen} />
    </Drawer.Navigator>
  );
};

const AppNavigator = () => {
  const {user, loading, t} = useSelector((state) => ({
    user: state.auth.userData,
    loading: state.loader.loading,
  }));

  return (
    <>
      <Stack.Navigator>
        <Stack.Screen
          options={{headerShown: false}}
          name="UserAuth"
          component={UserAuthFlow}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="HomeTabFlow"
          component={HomeTabFlow}
        />
      </Stack.Navigator>
      {loading && <Indicator />}
    </>
  );
};

export default AppNavigator;
