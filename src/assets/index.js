import {Platform} from 'react-native';

const fonts = Platform.select({
    ios : {
        regular: 'GothamRounded-Book',
        mediumBold: 'GothamRounded-Medium',
        bold: 'GothamRounded-Bold',
        light: 'GothamRounded-Light'
    },
    android: {
        regular: 'GothamRounded-Book',
        mediumBold: 'GothamRounded-Medium',
        bold: 'GothamRounded-Bold',
        light: 'GothamRounded-Light'

    }
});

export {fonts}