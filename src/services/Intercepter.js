import axios from 'axios'
import config from "../config";
import {getStore}  from '../redux/store/store'
const AxiosInstance = axios.create({
    baseURL: config.LIVE_URL,
    timeout: 20000,
    transformRequest: [function (data, headers) {
       let {language} = getStore().getState().language
       let {userData} = getStore().getState().auth 
       debugger
        headers['locale'] =language.code
        if(userData && userData.access_token){
            headers['Authorization'] = `Bearer ${userData.access_token}`
        }
        if(data && data._parts){
            return data

        }else{
            return JSON.stringify(data)
        }
      }],
      headers:{'Content-Type': 'application/json',}

})

AxiosInstance.interceptors.response.use((response) =>{
    return response;
}, (error) => {
    const originalRequest = error.config;
    if (!error.response) {
       return Promise.reject('Network Error')
    }else {
        return error.response
    }

})

export default AxiosInstance