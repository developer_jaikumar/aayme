import AsyncStorage from '@react-native-community/async-storage'
import AxiosInstance from './Intercepter'
import config from '../config/index';

//Post Request
export async function post(api,data){
   return AxiosInstance.post(`${config.LIVE_URL}${api}`,data).then((res)=> res.data).catch(err=> err.response)
}
//Get Request
export async function get(api,data){
   return AxiosInstance.get(`${config.LIVE_URL}${api}`).then((res)=>res.data).catch(err=> err)
}
//Put Request
export async function put(api,data){
   return AxiosInstance.put(`${config.LIVE_URL}${api}`,data).then((res)=> res.data).catch(err=> err.response)
}
//Delete Request
export async function deleteRequest(api,data){
   return AxiosInstance.delete(`${config.LIVE_URL}${api}`,data).then((res)=> res.data).catch(err=> err.response)
}
//Get All Request
export async function getAll(data){
   return Promise.all(data).then((values)=> {
       return values
   }).catch((err) => {
       return err
   })
}
// Get Token
export async function getAccessTokenFromCookies(){
   return new Promise(async (resolve, reject) => {
       let token = await AsyncStorage.getItem('token')
       if (token) {
         resolve(token);
       } else {
         reject(true);
       }
     });
}
// Get Language 
