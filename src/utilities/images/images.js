import backArrowBlack from '../../assets/images/ic_back_arrow_black.png';
import sideMenu from '../../assets/images/ic_menu.png'
import userPlaceHolder from '../../assets/images/img_editprofile.png'
import notification from '../../assets/images/notification.png'
import cart from '../../assets/images/supermarket.png'
import search from '../../assets/images/search.png'
import menu from '../../assets/images/menu.png'
import loginPhone from '../../assets/images/smartphone.png'
import smallPhone from '../../assets/images/phone-2.png'
import user from '../../assets/images/user.png'
import business from '../../assets/images/briefcase.png'
import location from '../../assets/images/place.png'
import return_box from '../../assets/images/return-box.png'
import logout from '../../assets/images/logout-3.png'
import policy from '../../assets/images/policy.png'
import support from '../../assets/images/support.png'
import call from '../../assets/images/call.png'
import filter from '../../assets/images/filter.png'
import order from '../../assets/images/order.png'
import category from '../../assets/images/category.png'
import sort from '../../assets/images/sort.png'
import camera from '../../assets/images/ar-camera.png'
import flag from '../../assets/images/flag.png'
import dropdown from '../../assets/images/play.png'
import new_logo from '../../assets/images/new_logo.png'
import share from '../../assets/images/ic_share.png';
import accept from '../../assets/images/accept.png';
import signout from '../../assets/images/logout.png';
import information from '../../assets/images/information.png'
import contract from '../../assets/images/contract.png'
import terms from '../../assets/images/terms.png'
import parcel from '../../assets/images/parcel.png'
import call_support from '../../assets/images/call-center-operator.png'
import edit_profile from '../../assets/images/pen.png'
import arrow from '../../assets/images/arrow.png'
import enquiry from '../../assets/images/agenda.png'
import cross from '../../assets/images/ic_close_grey.png'



// import tick from '../../assets/images/tick-mark.png';
// import user from '../../assets/images/user.png';
// import business from '../../assets/images/business.png';
// import advertiser from '../../assets/images/advertisor.png';
// import passwordActive from '../../assets/images/ic_show-password_active.png';
// import showPassword from '../../assets/images/ic_show-password.png';
// import instagram from '../../assets/images/instagram.png';
// import instagramLink from '../../assets/images/link_instagram.png';
// import google from '../../assets/images/ic_google.png';
// import hidePassword from '../../assets/images/ic_show-password.png';
// import dropdown from '../../assets/images/dropdown.png';
import mobileVerification from '../../assets/images/mobile_verification.png';
// import icEdit from '../../assets/images/ic_edit_number.png';
// import changeProfilePicture from '../../assets/images/change_profile_picture.png';
// import uploadProfilePicture from '../../assets/images/upload_profile_pic.png';
// import homeInActive from '../../assets/images/ic_homeInActive.png';
// import homeActive from '../../assets/images/ic_home.png';
// import dateNightPlus from '../../assets/images/ic_create_dn.png';
// import dateNightActive from '../../assets/images/ic_date_night.png';
// import dateNightInActive from '../../assets/images/ic_date_night_active.png';
// import createDateNight from '../../assets/images/ic_create_dn.png';
// import notificationInActive from '../../assets/images/ic_notification.png';
// import notificationActive from '../../assets/images/ic_notification_active.png';
// import profileInactive from '../../assets/images/ic_profile.png';
// import profileActive from '../../assets/images/ic_profile_active.png';
// import search from '../../assets/images/ic_search_contacts.png';
// import like from '../../assets/images/like.png';
// import likeActive from '../../assets/images/like_active.png';
// import share from '../../assets/images/share.png';
// import changeProfilePic from '../../assets/images/change_profile_picture.png';
// import cancelBtn from '../../assets/images/ic_cancel.png';
// import add_pic from '../../assets/images/add_pic.png';
// import pic_cross from '../../assets/images/pic-cross.png';
// import reqSent from '../../assets/images/request_sent.png';
// import emailPink from '../../assets/images/ic_email.png';
import edit from '../../assets/images/ic_edit.png';
// import editPencilPink from '../../assets/images/ic_edit.png';
// import businessProfile from '../../assets/images/ic_business_profile.png';
// import myEvent from '../../assets/images/ic_events_business.png';
// import myPlans from '../../assets/images/ic_plans.png';
// import purchasePlan from '../../assets/images/ic_plan_purchase_.png';
// import saveCards from '../../assets/images/ic_card.png';
// import settings from '../../assets/images/ic_profile_settings.png';
// import terms from '../../assets/images/ic_terms_&_conditions.png';
// import about from '../../assets/images/ic_about-us.png';
// import contact from '../../assets/images/ic_contact-us.png';
// import forwardArrowGray from '../../assets/images/ic_arrow.png';
// import emailGray from '../../assets/images/ic_email_small.png';
// import phoneHandle from '../../assets/images/ic_phone_small.png';
// import previousdateNight from '../../assets/images/ic_previous_date-nights.png';
// import myFavRestaurant from '../../assets/images/ic_favourites_profile.png';
// import endtimeLight from '../../assets/images/ic_end_time.png';
// import calenderLight from '../../assets/images/ic_date.png';
// import locationLight from '../../assets/images/ic_location.png';
// import select from '../../assets/images/ic_select_active.png';
// import unselect from '../../assets/images/ic_select_inactive.png';
// import ic_like from '../../assets/images/ic_like_active.png';
// import ic_menu from '../../assets/images/ic_menu.png';
// import ic_post from '../../assets/images/ic_post_location.png';
// import ic_website from '../../assets/images/ic_website.png';
// import ic_menuGray from '../../assets/images/ic_menu_grey.png';
// import default_user from '../../assets/images/defaultuser.png';
// import ic_eventType from '../../assets/images/ic_type_of_event.png';
// import ic_desc from '../../assets/images/ic_description.png';
// import ic_time from '../../assets/images/ic_time.png';
// import ic_datePink from '../../assets/images/ic_dateActive.png';
// import ic_fabBtn from '../../assets/images/create_event_fab.png';
// import ic_eventCreated from '../../assets/images/event_created.png';
// import ic_contactUs from '../../assets/images/contact-us-illustration.png';
// import ic_insta_handle from '../../assets/images/ic_instagram_handle.png';
// import ic_callPink from '../../assets/images/ic_call.png';
// import ic_backArrowWhite from '../../assets/images/ic_back_arrow_white.png';
// import ic_menuWhite from '../../assets/images/ic_menuWhite.png';
// import ic_calenderGray from '../../assets/images/ic_invite_date.png';
// import ic_timeGray from '../../assets/images/ic_invite_time.png';
// import ic_delete from '../../assets/images/ic_delete.png';
// import visacard from '../../assets/images/visaCard.jpg';
// import mastercard from '../../assets/images/masterCard.png';
// import ic_addCard from '../../assets/images/ic_add_new_card.png';
// import ic_select_active from '../../assets/images/ic_select_active.png';
// import ic_checkbox_active from '../../assets/images/ic_checkbox_active.png';
// import ic_checkbox from '../../assets/images/ic_checkbox.png';
// import congratDateNight from '../../assets/images/congratulations.png';
// import sharePink from '../../assets/images/share_ic_pink.png';
// import logoOnscreen from '../../assets/images/logo_on_screens.png';
// import ic_hash_tag from '../../assets/images/play_.png';
// import ic_share from '../../assets/images/ic_share.png';
// import ic_globe from '../../assets/images/ic_globe.png';
// import ic_adv_event from '../../assets/images/ic_advertiserevent.png';
// import ic_pink_web from '../../assets/images/ic_weburl.png'



const res='https://raw.githubusercontent.com/instamobile/tinder-react-native/master/assets/5.jpg';
const res1='https://raw.githubusercontent.com/instamobile/tinder-react-native/master/assets/1.jpg';
const res2='https://raw.githubusercontent.com/instamobile/tinder-react-native/master/assets/2.jpg';
const res3='https://raw.githubusercontent.com/instamobile/tinder-react-native/master/assets/3.jpg';
const res4='https://raw.githubusercontent.com/instamobile/tinder-react-native/master/assets/4.jpg';
const res5='http://gorestaurants.net/wp-content/uploads/2012/06/Portrait-Restaurant.jpg'
const images={
    backArrowBlack,
    sideMenu,
    userPlaceHolder,
    notification,
    cart,
    search,
    menu,
    loginPhone,
    smallPhone,
    user,
    business,
    location,
    return_box,
    logout,
    support,
    policy,
    call,
    filter,
    order,
    category,
    sort,
    camera,
    new_logo,
    flag,
    dropdown,
    share,
    accept,
    signout,
    information,
    contract,
    signout,
    terms,
    parcel,
    call_support,
    edit_profile,
    arrow,
    enquiry,
    cross,
    // tick,
    // user,
    // business,
    // advertiser,
    // passwordActive,
    // showPassword,
    // instagram,
    // instagramLink,
    // google,
    // hidePassword,
    // dropdown,
    mobileVerification,
    // icEdit,
    // changeProfilePicture,
    // uploadProfilePicture,
    // homeInActive,
    // homeActive,
    // dateNightPlus,
    // dateNightActive,
    // dateNightInActive,
    // createDateNight,
    // notificationInActive,
    // notificationActive,
    // profileInactive,
    // profileActive,
    // search,
    res,
    res1,
    res2,
    res3,
    res4,
    res5,
    // like,
    // share,
    // likeActive,
    // changeProfilePic,
    // add_pic,
    // pic_cross,
    // reqSent,
    // emailPink,
    edit,
    // editPencilPink,
    // businessProfile,
    // myEvent,
    // myPlans,
    // purchasePlan,
    // saveCards,
    // settings,
    // terms,
    // about,
    // contact,
    // forwardArrowGray,
    // emailGray,
    // phoneHandle,
    // previousdateNight,
    // myFavRestaurant,
    // endtimeLight,
    // calenderLight,
    // locationLight,
    // select,
    // unselect,
    // ic_like,
    // ic_menu,
    // ic_post,
    // ic_website,
    // ic_menuGray,
    // default_user,
    // ic_eventType,
    // ic_desc,
    // ic_time,
    // ic_datePink,
    // ic_fabBtn,
    // ic_eventCreated,
    // ic_contactUs,
    // ic_insta_handle,
    // ic_callPink,
    // ic_backArrowWhite,
    // ic_menuWhite,
    // ic_calenderGray,
    // ic_timeGray,
    // ic_delete,
    // visacard,
    // mastercard,
    // ic_addCard,
    // ic_select_active,
    // ic_checkbox_active,
    // ic_checkbox,
    // cancelBtn,
    // congratDateNight,
    // sharePink,
    // logoOnscreen,
    // ic_hash_tag,
    // ic_share,
    // ic_globe,
    // ic_adv_event,
    // ic_pink_web
};


module.exports = images;
