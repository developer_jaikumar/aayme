const constants={
    colors:require('./colors/colors'),
    strings:require('./localization/strings'),
    images:require('./images/images'),
    // validations:require('./validations/index'),
};

module.exports = constants;
