const colors = {
  white: '#FFFFFF',
  black: 'rgba(0,0,0,0.87)',
  btnGray: '#CCCCCC',
  pink:'#E03C3C',
  skyBlue:'#01BFD9',
  // pink: '#007bba',
  lightBlack: 'rgba(0,0,0,0.4)',
  fbBtbColor: '#4285F4',
  lightGray: 'rgba(0,0,0,0.2)',
  green_light: '#4A90E2',
  gray: '#999999',
  yellow:'#FFAC23',
  grayLight: 'rgba(0,0,0,0.6)',
  grayDark: 'rgba(0,0,0,0.7)',
  blackExtra: 'rgba(0,0,0,0.87)',
  blackligt: 'rgba(0,0,0,0.65)',
  semiBlack:'rgba(0,0,0,0.75)',
  green: '#30C79E',
  red: '#CF2C45',
  shadowColor: '#E5E5E5',
  grayishBlue: '#3B6687',
  placeholderTextColor:'#A6A6A6',
  separatorColor: 'rgba(24,113,175,0.04)',
  defaultColor:'#F6F6F6',
  screen_bg:'#F6F6F6',
  link_color:'#0000EE',
  light_blue: '#1A007bba',
  red_bg:'#BFCF2C45',
  kyc_blue:'#FFCF2C45',
  red_light:'#FBB9C5',
  blue_light:'#cceeff',
  defaultColor: "#F0F0F0"

};

module.exports = colors;

/**color opacity chart
100% — FF
95% — F2
90% — E6
85% — D9
80% — CC
75% — BF
70% — B3
65% — A6
60% — 99
55% — 8C
50% — 80
45% — 73
40% — 66
35% — 59
30% — 4D
25% — 40
20% — 33
15% — 26
10% — 1A
5% — 0D
0% — 00*/

