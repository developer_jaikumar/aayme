import {Platform, PermissionsAndroid} from 'react-native';
import {check, PERMISSIONS, RESULTS, request} from 'react-native-permissions';
export const androidCameraPermission = () =>
  new Promise(async (resolve, reject) => {
    try {
      if (Platform.OS === 'android' && Platform.Version > 22) {
        const granted = await PermissionsAndroid.requestMultiple([
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        ]);

        if (
          granted['android.permission.CAMERA'] !== 'granted' ||
          granted['android.permission.WRITE_EXTERNAL_STORAGE'] !== 'granted' ||
          granted['android.permission.READ_EXTERNAL_STORAGE'] !== 'granted'
        ) {
          Alert.alert(
            strings.ALERT,
            strings.DO_NOT_HAVE_PERMISSIONS_TO_SELECT_IMAGE,
            [{text: strings.OKAY}],
            {cancelable: true},
          );
          return resolve(false);
          // alert(strings.DO_NOT_HAVE_PERMISSIONS_TO_SELECT_IMAGE);
        }
        return resolve(true);
      }

      return resolve(true);
    } catch (error) {
      return resolve(false);
    }
  });

  export const checkPermissionsAndroid = () => {
    return new Promise(function(resolve, reject) {
      Promise.all([
        check(PERMISSIONS.ANDROID.CAMERA),
        check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE),
        check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
      ]).then(([cameraStatus, readStatus, writeStatus]) => {
        resolve({cameraStatus, readStatus, writeStatus});
      });
    });
    
  
  }

  export const checkPermissionsLocationAndroid = () => {
    return new Promise(function(resolve, reject) {
      Promise.all([
        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
      ]).then(([locationStatus]) => {
        resolve({locationStatus});
      });
    });
    
  
  }

  export const checkContactsPermissionsAndroid = () => {
    return new Promise(function(resolve, reject) {
      Promise.all([
        check(PERMISSIONS.ANDROID.READ_CONTACTS),
        check(PERMISSIONS.ANDROID.WRITE_CONTACTS)
      ]).then(([readStatus, writeStatus]) => {
        resolve({readStatus, writeStatus});
      });
    });
    
  
  }

  export const checkContactsPermissionsIOS = () => {
    return new Promise(function(resolve, reject) {
      Promise.all([
        check(PERMISSIONS.IOS.CONTACTS)
      ]).then(([readStatus]) => {
        resolve({readStatus});
      });
    });
    
  
  }
  export const  checkPermissionsIOS = () => {
    return new Promise(function(resolve, reject) {
     Promise.all([
      check(PERMISSIONS.IOS.CAMERA),
      check(PERMISSIONS.IOS.PHOTO_LIBRARY)
    ]).then(([cameraStatus, photoStatus]) => {
      resolve({cameraStatus, photoStatus});
    });
  });
  }
