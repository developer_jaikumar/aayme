import { Platform } from 'react-native';

export default {
  // API_URL:'http://aayme.alakintechnologies.com/api/',
  API_URL:'https://www.aayme.com/api/',
  LIVE_URL:'http://dev.aayme.com/api/'

  // LIVE_URL:'https://www.aayme.com/api/',
};
