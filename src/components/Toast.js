import React,{Component} from 'react'
import Toast from 'react-native-root-toast'
export const showToast=(status_message)=>{
    Toast.show(status_message, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
      });
};
