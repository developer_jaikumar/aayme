import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
} from 'react-native';
import constants from '../utilities/index';
import {RFValue} from 'react-native-responsive-fontsize';
import {fonts} from '../assets';
import {moderateScaleVertical} from '../utilities/responsiveSize';
import {moderateScale} from 'react-native-size-matters';
import LinearGradient from 'react-native-linear-gradient'

const {width, height} = Dimensions.get('window');

const HeaderComp = (props) => (
  <View style={[styles.container, props.headerContainerStyle]}>
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 7}}
      colors={['#E03C3C', '#E03C3C', '#FF9403', '#FF9403']}
      style={{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        height: 56,
      }}
    />

    <View
      style={{
        width: null,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: moderateScale(16),
      }}>
      {props.imageLeft && (
        <TouchableOpacity
          onPress={props.leftImgHandler}
          hitSlop={styles.hitSlop}>
          <Image
            source={props.imageLeft}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              tintColor: props.leftImageTint,
              height: moderateScale(24),
              width: moderateScale(24),
            }}
          />
        </TouchableOpacity>
      )}

      <Text
        style={[
          styles.leftText,
          props.leftTextStyle,
          {
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 2,
            fontFamily: fonts.bold,
          },
        ]}>
        {props.leftText}
      </Text>
    </View>
    <View style={{flex: 0.8}}>
      {props.headerText && <Text style={styles.text}>{props.headerText}</Text>}
    </View>

    <View
      style={{
        width: null,
        alignItems: 'center',
        flexDirection: 'row',
        marginRight: moderateScale(16),
      }}>
      {props.imageRight && (
        <TouchableOpacity
          onPress={props.rightImgHandler}
          hitSlop={styles.hitSlop}>
          <Image
            source={props.imageRight}
            style={{
              alignSelf: 'flex-end',
              height: moderateScale(24),
              width: moderateScale(24),
              tintColor: constants.colors.white,
            }}
          />
        </TouchableOpacity>
      )}
      {props.textRight && (
        <TouchableOpacity
          onPress={props.textRightHandler}
          hitSlop={styles.hitSlop}>
          {console.log(props.textRight, '===========================')}
          <Text style={styles.rightText}>{props.textRight}</Text>
        </TouchableOpacity>
      )}
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    height: moderateScaleVertical(56),
    flexDirection: 'row',
    justifyContent: 'space-between',
    shadowOpacity: 0.2,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 8,
    shadowColor: '#DFDFDF',
  },
  text: {
    textAlign: 'center',
    fontSize: RFValue(16),
    color: constants.colors.black,
  },
  hitSlop: {
    left: 15,
    right: 15,
    top: 15,
    bottom: 15,
  },
  //     leftText:{
  //         textAlign:'center',
  // fontSize:RFValue(16),
  //         color:constants.colors.black,
  //         fontFamily:fonts.bold
  //     },

  rightText: {
    fontSize: RFValue(14),
    fontFamily: fonts.bold,
    color: constants.colors.pink,
    marginLeft: moderateScale(34),
    textAlign: 'right',
  },
  leftText: {
    fontFamily: fonts.mediumBold,
    fontSize: RFValue(16),
    marginLeft: moderateScale(10),
    color: constants.colors.black,
    marginTop: 8,
  },
});

export default HeaderComp;
