import React, { useEffect } from "react";
import { StyleSheet, View } from 'react-native';
import Animation from 'lottie-react-native';
const Indicator = (props) => {
    let lottiRef;
    useEffect(() => {
        lottiRef.play();
    }, []);

    return <View style={styles.loading}>
        <Animation
            ref={animation => {
                lottiRef = animation;
            }}
            style={styles.lottie}
            resizeMode={'cover'}
            loop={true}
            source={require('../assets/lottie/loader.json')}
        />
    </View>
};
export default Indicator


const styles = StyleSheet.create({
    lottie: {
     height:100
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        display: 'flex',
        top: 0,
        justifyContent: 'space-around',
        bottom: 0,
        alignItems: 'center',
        zIndex: 100000,
        backgroundColor: 'rgba(0,0,0,0.2)'
    }
});
