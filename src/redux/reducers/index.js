import {combineReducers} from 'redux';
import auth from './auth'
import netInfo from './netInfo';
import loader from './loader';
import language from './languages';
import category from './category'
import getCurrentScreen from './getScreenName'

// Combine all the reducers
const rootReducer = combineReducers({
  auth,
  netInfo,
  loader,
  language,
  category,
  getCurrentScreen
});


export default rootReducer;
