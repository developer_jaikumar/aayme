import {SWITCH_LANGUAGE_DATA} from '../actionTypes';

/*eslint-disable */
const defaultState = {
    language: {code:'en',name:'English'}
};
const switchLanguageVal = (state = defaultState, action) => {
    switch (action.type) {
        case SWITCH_LANGUAGE_DATA: {
         const language = {
                ...state,
                language:action.payload
            };
            return language
        }
        default:
            return state;
    }
}

export default switchLanguageVal;
