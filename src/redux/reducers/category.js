import {SHOW_CATEGORY_DATA,HIDE_CATEGORT_DATA} from '../actionTypes';
/*eslint-disable */
const defaultState = {
    setCategoryShown : false
};
const categoryContainer = (state = defaultState, action) => {
    switch (action.type) {
        case SHOW_CATEGORY_DATA: {
         return {setCategoryShown :true}
        }
        case HIDE_CATEGORT_DATA: {
            return {setCategoryShown :false}
         }
        default:
            return state;
    }
}
export default categoryContainer;


