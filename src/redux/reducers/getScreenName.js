import {GET_CURRENT_SCREEN_NAME} from '../actionTypes';
/*eslint-disable */
const defaultState = {
    currentScreen : ''
};
const screenContainer = (state = defaultState, action) => {
    switch (action.type) {
        case GET_CURRENT_SCREEN_NAME: {
         const currentScreen = {
                ...state,
                currentScreen:action.payload
            };
            return currentScreen
        }
        default:
            return state;
    }
}

export default screenContainer;


