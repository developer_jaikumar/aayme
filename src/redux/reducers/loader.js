import {SHOW_LOADER,HIDE_LOADER} from '../actionTypes';

/*eslint-disable */
const defaultState = {
    loading : false
};
const loader = (state = defaultState, action) => {
    switch (action.type) {
        case SHOW_LOADER: {
         return {loading :true}
        }
        case HIDE_LOADER: {
            return {loading :false}
         }
        default:
            return state;
    }
}
export default loader;
