import {SAVE_USER_DATA} from '../actionTypes';

const initialState = {
  userData: null,
  online : false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SAVE_USER_DATA:
      return {...state, 
        userData: {...action.payload}, 
       };
    default:
      return state;
  }
};
