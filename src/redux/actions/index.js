import * as auth from './auth';
import * as netInfo from './netInfo';
import * as loaderAction from './loader';
import * as categoriesAction from './category';
import * as getScreenAction from './getScreenName'

export default {
  ...auth,
  ...netInfo,
  ...loaderAction,
  ...categoriesAction,
  ...getScreenAction
};
