import {OFFLINE, ONLINE} from '../actionTypes';
import store from '../store/store';
const {dispatch} = store;
export const online = () => {
  dispatch({
    type: ONLINE,
  });
};

export const offline = () => {
  dispatch({
    type: OFFLINE,
  });
};
