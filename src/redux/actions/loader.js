
import {SHOW_LOADER,HIDE_LOADER} from '../actionTypes';


export const showLoader = payload => ({
    type: SHOW_LOADER,
  });
  

export const hideLoader = payload => ({
    type: HIDE_LOADER,
  });
  