
import {SAVE_USER_DATA} from '../actionTypes';
import store from '../store/store';

export const saveUserDataType = payload => ({
  type: SAVE_USER_DATA,
  payload
});


export const saveUserData = payload => {
    console.log("payload data",payload)
    // if(payload!==undefined) {
    //     if (Object.keys(payload).length > 0) {
    //         if (payload.id !== undefined) {
                return (dispatch, getState) => {
                    dispatch(saveUserDataType(payload))
                }
    //         }
    //     }
    // }
}



export function login(data) {
  console.log('the data dispatched is, ', data);
  return new Promise((resolve, reject) => {
    apiPost(LOGIN_API, data)
      .then(res => {
        console.log(res,'the res the aoijfdiofjioajfiojajfiojsdfiojio');
        console.log(res.data,'htaufuao9sufo')
        console.log(res.data.accessToken,'the acces toiojioj')
        if(res.status == 200)
        {
          saveUserData(res.data);
          resolve(res);
          setUserData(res.data)
            .then(data => {
              resolve(res);
            })
            .catch(err => console.log('error setting user data', err));
        }
       else{
        resolve(res);
       }
      })
      .catch(error => {
        console.log("error catch")
        console.log(error)
        reject(error);
      });
  });
}

export function socialLogin(data) {
  return new Promise((resolve, reject) => {
    apiPost(SOCIAL_LOGIN_API, data)
      .then(res => {
        saveUserData(res.data);
        // resolve(res);
        setUserData(res.data)
          .then(resolve(res))
          .catch(err => console.log(err));
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function signup(data) {
  console.log('the data dispatched is, ', data);
  return new Promise((resolve, reject) => {
    apiPost(SIGNUP_API, data)
      .then(res => {
        resolve(res);

        // setUserData(res.data)
        //   .then(resolve(res.data))
        //   .catch(err => console.log(err));
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function emailVerification(data) {
  console.log('the data dispatched is, ', data);
  return new Promise((resolve, reject) => {
    apiPost(EMAIL_VERIFICATION_API, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function resendEmail(data) {
  console.log('the data dispatched to resendEmail is, ', data);
  return new Promise((resolve, reject) => {
    apiPost(RESEND_EMAIL_API, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function forgotPassword(data) {
  console.log('the data dispatched to resendEmail is, ', data);
  return new Promise((resolve, reject) => {
    apiPost(FORGOT_PASSWORD_API, data)
      .then(res => {
        console.log(res,  'myres')
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function updateForgotPassword(data) {
  console.log('the data dispatched to updateForgotPassword is, ', data);
  return new Promise((resolve, reject) => {
    apiPost(UPDATE_FORGOT_PASSWORD_API, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function logOut(data) {
  console.log('the data dispatched to logout api  is, ', data);
  return new Promise((resolve, reject) => {
    apiPost(USER_LOGOUT_API, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}
