/* eslint-disable import/no-mutable-exports */
import { createStore, applyMiddleware } from "redux";
import AsyncStorage from '@react-native-community/async-storage';
import reduxThunk from "redux-thunk";
import logger from "redux-logger";

import axios from "axios";
import axiosMiddleware from "redux-axios-middleware";
import { persistStore, persistReducer } from "redux-persist";

import config from "../../config";
import reducers from "../reducers";

/**
 * Create Axios Client to communicate
 * with JustplayTV API
 */
const axiosClient = axios.create({
  baseURL: config.LIVE_URL,
  responseType: "json",
});

// Store instance
 let store = null;
 let persistor = null;

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  timeout: null,
};

const persistedReducer = persistReducer(persistConfig, reducers);

/**
 * Create the Redux store
 */

export const configureStore = () => {
  store = createStore(persistedReducer, applyMiddleware(reduxThunk, axiosMiddleware(axiosClient),logger));
  persistor = persistStore(store);
  return { store, persistor };
};



/**
 * Get store
 */
export const getStore = () => store;

/**
 * Get persistor
 */
export const getPersistor = () => persistor;
/**
 * Dispatch an action
 */
export const dispatch = (...args) => store.dispatch(...args);
export default {
  dispatch,
  getStore,
  configureStore,
};
