export const SAVE_USER_DATA = 'app/auth/SAVE_USER_DATA';
export const SAVE_POST_ITEM = 'app/profile/SAVE_POST_ITEM';
export const CLEAR_POST_DATA = 'app/profile/CLEAR_POST_DATA';
export const CLEAR_REDUX_STATE = 'CLEAR_REDUCER_STATE';
export const UPDATE_POST_ITEM = 'app/profile/UPDATE_POST_ITEM';
export const UPDATE_API_POST_ITEM = 'app/profile/UPDATE_API_POST_ITEM';
export const DELETE_POST_ITEM = 'app/profile/DELETE_POST_ITEM';
export const OFFLINE = 'OFFLINE';
export const ONLINE = 'ONLINE';

export const SHOW_LOADER = 'SHOW_LOADER';
export const HIDE_LOADER = 'HIDE_LOADER';
export const SWITCH_LANGUAGE_DATA = 'SWITCH_LANGUAGE_DATA';

export const SHOW_CATEGORY_DATA = 'SHOW_CATEGORY_DATA'
export const HIDE_CATEGORT_DATA = 'HIDE_CATEGORY_DATA'

export const GET_CURRENT_SCREEN_NAME = 'GET_CURRENT_SCREEN_NAME'

