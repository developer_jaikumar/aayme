/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {configureStore} from './src/redux/store/store';
import AppNavigator from './src/navigation/AppNavigator';
import ProductDetailsScreen from './src/screens/ProductDetailsScreen/ProductDetailsScreen';
import HomeScreen from './src/screens/HomeScreen/HomeScreen';



console.disableYellowBox = true;

const {store, persistor} = configureStore();

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const linking = {
  prefixes: ['https://www.aayme.com/ProductDetail', 'aayme://'],
  config: {
    HomeScreen: {
      path: 'progress-detail/:id',
      parse: {
        id: String,
      }
    },
  }
};

export const rootNavigationRef = React.createRef();


class App extends Component {
  constructor(props) {
    super(props);
    this.currentRouteName = '';
    // this.state = {
    //   currentLanguage: 'es'
    // };
  }

  componentDidMount(){
    SplashScreen.hide()
  }



  render() {
    let{actions} = this.props
    console.log('deep link params',this.props)
    return (
      <>
        <Provider store={store}>
          <PersistGate loading={true} persistor={persistor}>
            {/* <LocalizationProvider> */}
            <NavigationContainer linking={linking} ref={rootNavigationRef}
                onStateChange={(state) => {
                  const currentRouteName = getActiveRouteName(state);
                  this.currentRouteName = currentRouteName;
                  // actions.setCurrentScreenName(currentRouteName);
                  console.log('Route Name : ', currentRouteName);
                  AsyncStorage.setItem('routeName',currentRouteName)
                }}>
                <AppNavigator />
              </NavigationContainer>
            {/* </LocalizationProvider> */}
          </PersistGate>
        </Provider>
      </>
    );
  }
}

export default App


const getActiveRouteName = state => {
  const route = state.routes[state.index];

  if (route.state) {
    // Dive into nested navigators
    return getActiveRouteName(route.state);
  }

  return route.name;
};



